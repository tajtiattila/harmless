package packed

import (
	"math"
	"sort"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

// Source is a data source for route.Space
type Source interface {
	// PackSystems returns all systems to be used for routing.
	PackSystems() ([]gmap.PackSys, error)

	// At returns a list of systems for the positions provided.
	At(positions []gmap.Vec3i) []*gmap.System
}

const MaxNodeLen = 8

// Space represents the problem space for the A* package
type Space struct {
	Source  Source
	Systems []gmap.PackSys

	root snode
}

// NewSpace creates a new Space from src. It
// panics if the absolute value of any coordinate is
// larger than or equal to 1<<31.
func NewSpace(src Source) (*Space, error) {
	v, err := src.PackSystems()
	if err != nil {
		return nil, err
	}
	return newSpace(src, v), nil
}

// Idx returns the index of system at pos.
// It returns -1 if there is no system at pos.
func (s *Space) Idx(pos gmap.Vec3i) int32 {
	return s.root.idx(s.Systems, pos)
}

// Sphere calls f with each PackSys within the sphere defined
// by centre and radius.
func (s *Space) Sphere(centre gmap.Vec3i, radius float64, f func(ps gmap.PackSys, dist float64)) {
	s.SphereIdx(centre, radius, func(i int32, dist float64) {
		f(s.Systems[i], dist)
	})
}

// Sphere calls f with each PackSys index inside Systems
// within the sphere defined by centre and radius.
func (s *Space) SphereIdx(centre gmap.Vec3i, radius float64, f func(i int32, dist float64)) {
	r := int32(math.Floor(radius * gmap.Vec3iUnit))
	s.root.query(&squery{
		v: s.Systems,
		c: centre,
		r: radius,
		f: f,
		min: gmap.Vec3i{
			centre[0] - r,
			centre[1] - r,
			centre[2] - r,
		},
		max: gmap.Vec3i{
			centre[0] + r + 1,
			centre[1] + r + 1,
			centre[2] + r + 1,
		},
	})
}

func newSpace(src Source, v []gmap.PackSys) *Space {
	sz := int64(1)
	for _, n := range v {
		for i := 0; i < 3; i++ {
			for int64(n.P[i]) < -sz {
				sz *= 2
			}
			for sz <= int64(n.P[i]) {
				sz *= 2
			}
		}
	}
	if sz >= 1<<31 {
		panic("nodes don't fit into int32 based tree")
	}
	isz := int32(sz)
	root := snode{
		min: gmap.Vec3i{-isz, -isz, -isz},
		max: gmap.Vec3i{isz, isz, isz},
		s:   0,
		e:   int32(len(v)),
	}
	subdivide(v, &root)
	return &Space{src, v, root}
}

type snode struct {
	min, max gmap.Vec3i
	children []snode // 0 to 8 subtrees (empty ones omitted)
	s, e     int32   // indices into Space.v
}

func (n *snode) idx(v []gmap.PackSys, p gmap.Vec3i) int32 {
	if p[0] < n.min[0] || p[1] < n.min[1] || p[2] < n.min[2] ||
		n.max[0] < p[0] || n.max[1] < p[1] || n.max[2] < p[2] {
		return -1
	}
	if n.children != nil {
		for _, child := range n.children {
			if i := child.idx(v, p); i >= 0 {
				return i
			}
		}
	} else {
		for i := n.s; i < n.e; i++ {
			if v[i].P == p {
				return i
			}
		}
	}
	return -1
}

func (n *snode) query(q *squery) {
	if q.max[0] < n.min[0] || q.max[1] < n.min[1] || q.max[2] < n.min[2] ||
		n.max[0] < q.min[0] || n.max[1] < q.min[1] || n.max[2] < q.min[2] {
		return
	}
	if n.children != nil {
		for _, child := range n.children {
			child.query(q)
		}
	} else {
		for i := n.s; i < n.e; i++ {
			if dist := q.v[i].P.Sub(q.c).Abs(); dist <= q.r {
				q.f(i, dist)
			}
		}
	}
}

type squery struct {
	v []gmap.PackSys
	c gmap.Vec3i
	r float64
	f func(i int32, dist float64)

	min, max gmap.Vec3i
}

func subdivide(v []gmap.PackSys, n *snode) {
	leaves := v[n.s:n.e]
	if len(leaves) < MaxNodeLen || n.min[0]+1 == n.max[0] {
		return // no need to or cannot subdivide
	}

	var c gmap.Vec3i
	for k := 0; k < 3; k++ {
		c[k] = (n.min[k] + n.max[k]) / 2
	}

	// sort leaves so every leaf in the same octant is in a continuous range
	sort.Sort(&nodeSort{c: c, v: leaves})

	i := 0
	for octant := 0; octant < 8; octant++ {
		var child snode
		for k := 0; k < 3; k++ {
			if (octant & (1 << uint(2-k))) == 0 {
				child.min[k] = n.min[k]
				child.max[k] = c[k]
			} else {
				child.min[k] = c[k]
				child.max[k] = n.max[k]
			}
		}
		s := i
	LeavesLoop:
		for ; i < len(leaves); i++ {
			p := leaves[i].P
			for k := 0; k < 3; k++ {
				if p[k] < child.min[k] || child.max[k] <= p[k] {
					break LeavesLoop
				}
			}
		}
		// add child if there it has at least one element
		if s != i {
			child.s, child.e = n.s+int32(s), n.s+int32(i)
			subdivide(v, &child)
			n.children = append(n.children, child)
		}
	}

	n.s, n.e = 0, 0

	// if n has only once child, replace
	// it with its child
	if len(n.children) == 1 {
		*n = n.children[0]
	}
}

// nodeSort sorts v according to which octant they are with
// relation to center c. The order of coordinates is X, Y, Z:
// Nodes left of c (smaller X) comes first, then Nodes below c
// (smaller Y) within those blocks comes first, finally nodes
// behind c (smaller Z) within comes first.
type nodeSort struct {
	c gmap.Vec3i
	v []gmap.PackSys
}

func (s *nodeSort) Len() int      { return len(s.v) }
func (s *nodeSort) Swap(i, j int) { s.v[i], s.v[j] = s.v[j], s.v[i] }

func (s *nodeSort) Less(i, j int) bool {
	u, v := s.v[i].P, s.v[j].P
	for k := 0; k < 3; k++ {
		if ok := u[k] < s.c[k]; ok != (v[k] < s.c[k]) {
			return ok
		}
	}
	return false
}
