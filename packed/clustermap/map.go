// Package clustermap implements a cluster map
// and a cluster map cache.
//
// A cluster represents a bubble in space that
// contains all systems reachable with a certain jump range.
package clustermap

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/packed"
)

type Map struct {
	*packed.Space

	vclsidx []int32
}

// NewMap creates a new cluster map for the space
// with jumprange.
func NewMap(space *packed.Space, jumprange float64) *Map {
	v := space.Systems
	set := make(map[int32]bool)
	for i := range v {
		set[int32(i)] = true
	}

	imap := make([]int32, len(v))
	cluster := int32(1)
	work := make([]int32, 0, len(set))
	for len(set) != 0 {
		var i int32
		for i, _ = range set {
			break
		}
		delete(set, i)
		work = append(work[:0], i)
		for len(work) != 0 {
			n := len(work) - 1
			work, i = work[:n], work[n]
			imap[i] = cluster

			space.SphereIdx(v[i].P, jumprange, func(j int32, dist float64) {
				if set[j] {
					delete(set, j)
					work = append(work, j)
				}
			})
		}
		cluster++
	}

	return &Map{space, imap}
}

// At returns the cluster index for the system at pos,
// or zero if pos is invalid. Two systems are reachable
// from each other only if they share the same cluster index.
func (m *Map) At(pos gmap.Vec3i) int32 {
	idx := m.Space.Idx(pos)
	if idx >= 0 {
		return m.vclsidx[idx]
	}
	return 0
}
