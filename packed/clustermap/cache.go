package clustermap

import (
	"sync"

	"bitbucket.org/tajtiattila/harmless/packed"
)

// ClusterCache caches cluster maps for each jump range.
//
// The cluster map assigns an integer cluster id to each system.
// Each system having the same cluster id can be reached from all other
// systems sharing the same cluster id, with jumps smaller than or equal
// to the jump range.
//
// The zero value of Cache is usable after a call to SetSpace.
type Cache struct {
	*packed.Space

	mtx  sync.Mutex
	data map[int]*clusterNode
}

func NewCache(space *packed.Space) *Cache {
	return &Cache{
		Space: space,
		data:  make(map[int]*clusterNode),
	}
}

// SetSpace sets a new space to use with this Cache
// and removes any cached data.
func (c *Cache) SetSpace(space *packed.Space) {
	c.mtx.Lock()
	c.Space = space
	c.data = make(map[int]*clusterNode)
	c.mtx.Unlock()
}

// Map creates a Map for jumprange. Map will be
// generated only once for any Space, even if multiple
// maps are requested from different threads.
func (c *Cache) Map(jumprange float64) *Map {
	jri := int(jumprange * 100) // 2 dp

	c.mtx.Lock()
	node := c.data[jri]
	if node == nil {
		node = new(clusterNode)
		c.data[jri] = node
	}
	space := c.Space
	c.mtx.Unlock()

	node.once.Do(func() {
		node.data = NewMap(space, float64(jri)/100)
	})
	return node.data
}

type clusterNode struct {
	once sync.Once
	data *Map
}
