package main

import (
	. "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/planner"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/tajtiattila/basedir"
	"github.com/tajtiattila/jsonmin"
	"os"
	"time"
)

type Config struct {
	DB   string `json:"db"`
	Html bool
	planner.Query
	planner.Config
}

func NewConfig() *Config {
	c := new(Config)
	*c = defc

	// specify command line argument overrides
	fs := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	fs.StringVar(&c.DB, "db", c.DB, "sqlite database filename")
	fs.Var(&c.Ship, "ship", "ship type")
	fs.Float64Var(&c.Loadout, "loadout", c.Loadout, "equipment loadout [t]")
	fs.IntVar(&c.Cash, "cash", c.Cash, "amount of cash to use for trading")
	fs.Float64Var(&c.Dock, "dock", c.Dock, "cost of docking compared to jumping")
	fs.IntVar(&c.MinStock, "stock", c.MinStock, "minimum station stock to conisder for routes")
	fs.Var(&c.From, "from", "show only routes starting at this station")
	fs.Var(&c.To, "to", "show only routes ending at this station")
	fs.BoolVar(&c.Circle, "circle", c.Circle, "show only circular routes")
	fs.IntVar(&c.MaxLen, "maxlen", c.MaxLen, "max route length")
	fs.IntVar(&c.Show, "show", c.Show, "show this many routes best")
	fs.DurationVar(&c.Timeout.V, "timeout", c.Timeout.V, "timeout for trade route generator")
	fs.IntVar(&c.WorkingSetSize, "workset", c.WorkingSetSize, "max working set size when generating routes")
	fs.DurationVar(&c.Age.V, "age", c.Age.V, "max. age of market entries to use in calculateion")
	savep := fs.String("defcfg", "", "save default config to the file specified")
	fs.BoolVar(&c.Html, "html", false, "html output")

	// first load the config, if any
	f, err := basedir.Config.Open("eliteprofit.json")
	var cfgname string
	if err == nil {
		defer f.Close()
		cfgname = f.Name()
		if err = jsonmin.NewDecoder(f).Decode(&c); err != nil {
			fmt.Fprintln(os.Stderr, "Error reading config:", cfgname, "\n", err)
			os.Exit(1)
		}
	}

	fs.Parse(os.Args[1:])
	if fs.NArg() != 0 {
		fmt.Fprintln(os.Stderr, "Unexpected positional arguments:", fs.Args())
		os.Exit(1)
	}

	if *savep != "" {
		SaveDefaultConfig(*savep)
	}

	return c
}

func SaveDefaultConfig(cfgname string) error {
	if cfgname != "" {
		c := new(Config)
		*c = defc
		f, err := os.Create(cfgname)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Can't open config for writing:", cfgname, "\n", err)
			return err
		}
		defer f.Close()
		if err = json.NewEncoder(f).Encode(c); err != nil {
			fmt.Fprintln(os.Stderr, "Error writing config:", cfgname, "\n", err)
			return err
		}
	}
	return nil
}

var defc = Config{
	DB: "market.db",
	Query: planner.Query{
		Ship:     planner.ShipSpec{MustFindShip("Sidewinder")},
		Loadout:  8,
		Cash:     1000,
		Dock:     30,
		MinStock: 10000,
		MaxLen:   6,
		Age:      planner.Duration{168 * 24 * time.Hour},
	},
	Config: planner.Config{
		Show:           100,
		Timeout:        planner.Duration{10 * time.Second},
		WorkingSetSize: 10000,
	},
}
