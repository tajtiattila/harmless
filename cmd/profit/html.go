// vim: set expandtab :
// © 2014 Attila Tajti
package main

import (
	"bitbucket.org/tajtiattila/harmless/planner"
	"fmt"
	"github.com/tajtiattila/basedir"
	"html/template"
	"io"
	"time"
)

/*
### 90+
##O 80+
##_ 70+
#O_ 60+
#__ 50+
O__ 40+
*/

func faicon(s string) template.HTML {
	return template.HTML(`<i class="fa fa-` + s + `"></i>`)
}

func faicon3(a, b, c string) template.HTML {
	return faicon(a) + faicon(b) + faicon(c)
}

func ageicon(t time.Time) template.HTML {
	nmin := int(time.Now().Sub(t).Nanoseconds()/1e9) / 60
	var icon template.HTML
	var styl string
	switch {
	case nmin < 2:
		icon, styl = faicon("dot-circle-o"), "ok"
	case nmin < 15:
		icon, styl = faicon("check-circle-o"), "ok"
	case nmin < 180:
		icon, styl = faicon("clock-o"), "ok"
	case nmin < 2880:
		icon, styl = faicon("question-circle"), "old"
	default:
		icon, styl = faicon("warning"), "old"
	}
	return template.HTML(`<span class="ageicon `+styl+`">`) + icon + template.HTML(`</span>`)
}

type Template interface {
	Execute(w io.Writer, v interface{}) error
}

type FileTemplate struct {
	tmpl *template.Template
}

func (t *FileTemplate) Execute(w io.Writer, data interface{}) error {
	return t.tmpl.Execute(w, &tdata{"http:", data})
}

type tdata struct {
	Proto string
	Data  interface{}
}

var htmlTmpl Template

func init() {
	dir, err := basedir.Gopath.Dir("src/bitbucket.org/tajtiattila/elitetrade/cmd/profit/data")
	if err != nil {
		panic(err)
	}
	t := template.Must(template.New("hrt").Funcs(template.FuncMap{
		"add1": func(x int) int { return x + 1 },
		"relhopn": func(cur, best planner.TradeItem) int {
			return (cur.TotalProfit * 10 / best.TotalProfit) * 10
		},
		"relhopfaicon": func(cur, best planner.TradeItem, a, b, c string) template.HTML {
			p := cur.TotalProfit * 100 / best.TotalProfit
			switch {
			case p > 90:
				return faicon3(a, a, a)
			case p > 80:
				return faicon3(a, a, b)
			case p > 70:
				return faicon3(a, a, c)
			case p > 60:
				return faicon3(a, b, c)
			case p > 50:
				return faicon3(a, c, c)
			case p > 40:
				return faicon3(b, c, c)
			}
			return faicon3(c, c, c)
		},
		"agestr": func(t time.Time) string {
			nsec := int(time.Now().Sub(t).Nanoseconds() / 1e9)
			switch {
			case nsec < 60:
				return "now"
			case nsec < 60*100:
				return fmt.Sprint(nsec/60, "m")
			case nsec < 48*60*60:
				return fmt.Sprint(nsec/(60*60), "h")
			}
			return fmt.Sprint(nsec/(24*60*60), "d")
		},
		"worstageicon": func(ti planner.TradeItem) template.HTML {
			if ti.T.BuyT.Before(ti.T.SellT) {
				return ageicon(ti.T.BuyT)
			}
			return ageicon(ti.T.SellT)
		},
		"ageicon": ageicon,
	}).ParseGlob(dir + "/*.html")).Lookup("result")
	//template.Must(template.ParseGlob(dir + "/*.html"))

	htmlTmpl = &FileTemplate{t}
}
