package main

import (
	"bitbucket.org/tajtiattila/harmless/planner"
	"fmt"
	t "text/template"
	"time"
)

/*
var routeTemplate = t.Must(t.New("rt").Parse(`Found {{.NumFound}} entries in {{.Time}}
{{range .Routes}}{{.Index}}: {{.ProfitPerJump}}¤ = {{.Profit}}¤ in {{.Len}} stops
{{range .Hops}}  {{.From}} {{range .Route}}` +
`→{{if .Refuel}}REFUEL@{{end}}{{.System.Name}} ` +
`{{end}}» {{.To}}{{if lt .FuelRemaining 35}} (tank={{.FuelRemaining}}%}}
{{range (.Best 2 0.6)}}    {{print "%7.0f" .TotalProfit}}¤ {{print "%.20s" .T.Commodity.Name}}{{print "%4d" .Qty}}¤ ` +
`({{print "%4d" .Buy}}-{{print "%5d" .Sell}}+{{print "%7d" .Stock}})  {{.BuyAge}} » {{.SellAge}}
{{end}}
{{end}}{{end}}
`))
*/

var textFmt = t.New("rt")

func init() {

	textFmt.Funcs(t.FuncMap{
		"add1": func(x int) int { return x + 1 },
		"agestr": func(t time.Time) string {
			nsec := int(time.Now().Sub(t).Nanoseconds() / 1e9)
			switch {
			case nsec < 60:
				return "now"
			case nsec < 60*100:
				return fmt.Sprint(nsec/60, "m")
			case nsec < 48*60*60:
				return fmt.Sprint(nsec/(60*60), "h")
			}
			return fmt.Sprint(nsec/(24*60*60), "d")
		},
		"agemin": func(ti *planner.TradeItem) int {
			t, tx := ti.T.BuyT, ti.T.SellT
			if tx.Before(t) {
				t = tx
			}
			return int(time.Now().Sub(t) / time.Minute)
		},
	})
	t.Must(textFmt.Parse(`Found {{.NumFound}} entries in {{.Duration}}` + "\n{{.Err}}\n\n" +
		`{{range $i, $r := .Routes}}{{add1 $i}}: {{template "R" $r}}{{end}}`))
	t.Must(textFmt.Parse(`{{define "R"}}{{template "RS" .}}{{range .Hops}}` +
		`{{template "HJ" .}}{{template "HT" (.Best 2 0.6)}}{{end}}` + "\n{{end}}"))

	t.Must(textFmt.Parse(`{{define "RS"}}{{.AvgProfit}}¤ = {{.Profit}}¤ in {{len .Hops}} stops` + "\n{{end}}"))
	t.Must(textFmt.Parse(`{{define "HJ"}}    {{.From.Name}} ` +
		`{{range .Route}}→{{if .Refuel}}REFUEL@{{end}}{{.System.Name}} {{end}}` +
		`» {{.To.Name}}` +
		`{{if lt .FuelRemaining 35}} (tank={{.FuelRemaining}}%){{end}}` + "\n{{end}}"))
	t.Must(textFmt.Parse(`{{define "HT"}}{{range .}}        ` +
		`{{printf "%7d" .TotalProfit}}¤ {{printf "%-20.20s" .T.Commodity.Name}}{{printf "%4d" .Qty}}t ` +
		`({{printf "%4d" .Buy}}-{{printf "%5d" .Sell}}+{{printf "%7d" .Stock}})` +
		`{{if lt 2 (agemin .)}}  {{agestr .T.BuyT}} » {{agestr .T.SellT}}{{end}}` +
		"\n{{end}}{{end}}"))
}
