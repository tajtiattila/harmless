package main

import (
	ed "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/route"
	"flag"
	"fmt"
	"os"
)

func main() {
	db := flag.String("db", "./data/elite.sqlite", "database to use")
	sysn := flag.String("c", "Eranin", "system at center")
	r := flag.Float64("r", 25, "radius")
	flag.Parse()

	verify(ed.LoadStarData(*db))

	sys, err := ed.FindSystem(*sysn)
	verify(err)

	chart := route.NewOctreeChart(ed.Systems)

	var v []*ed.System
	chart.Sphere(sys.P, *r, func(s *ed.System) {
		v = append(v, s)
	})

	goexport(v)
}

func goexport(v []*ed.System) {
	for _, s := range v {
		fmt.Printf(`d.sys("%s", %.6f, %.6f, %.6f)`, s.Name, s.P[0], s.P[1], s.P[2])
		sep, add := `.st(`, ""
		for _, st := range s.Stations {
			fmt.Printf(`%s"%s"`, sep, st.Name)
			sep, add = ", ", ")"
		}
		fmt.Println(add)
	}
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
