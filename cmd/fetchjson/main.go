package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	var (
		src           string
		local, remote bool
	)

	flag.StringVar(&src, "url", "", "source url")
	flag.BoolVar(&remote, "remote", false, "use starchart.club url")
	flag.BoolVar(&local, "local", false, "use default local url")
	flag.Parse()

	if src == "" && !local {
		remote = true
	}
	switch {
	case remote:
		src = "http://starchart.club/elite.json"
	case local:
		src = "http://localhost:6968/elite.json"
	}
	data, err := fetchJson(src)
	verify(err)

	verify(ioutil.WriteFile("data/elite.json", data, 0644))
}

func fetchJson(url string) ([]byte, error) {
	r, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	data, err := ioutil.ReadAll(r.Body)

	var buf bytes.Buffer
	if err := json.Indent(&buf, data, "", " "); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
