package main

import (
	marketjson "bitbucket.org/tajtiattila/harmless/market/json"
	"bitbucket.org/tajtiattila/harmless/market/quote"
	_ "bitbucket.org/tajtiattila/harmless/market/quote/emdn"
	etdb "bitbucket.org/tajtiattila/harmless/market/sqlitedb"
	"bitbucket.org/tajtiattila/harmless/util"
	"flag"
	"fmt"
	"io"
	"net/http"
	"time"
)

func main() {
	imp := flag.Bool("import", false, "import log before starting up")
	logfp := flag.String("log", "market.log", "log file to write json input (and optional import)")
	dbfp := flag.String("db", "market.db", "sqlite database filename")
	jsonp := flag.String("serve", "", "serve json on specified address")
	flag.Parse()

	logf := util.PreparePath(*logfp)
	dbf := util.PreparePath(*dbfp)

	mdb, err := etdb.OpenMarketDb(dbf)
	if err != nil {
		fatal(err)
	}
	defer mdb.Close()

	chsrv := make(chan error)
	if *jsonp != "" {
		srvjson, err := marketjson.NewServer(mdb)
		if err != nil {
			fatal(err)
		}
		http.Handle("/", srvjson)
		go func() {
			chsrv <- http.ListenAndServe(*jsonp, nil)
			close(chsrv)
		}()
	}

	if *imp {
		importlog(mdb, logf)
	}

	req := "tcp://firehose.elite-market-data.net:9500"
	if logf != "" {
		req += " log=" + logf
	}
	s, err := quote.Open("firehose", req)
	if err != nil {
		fatal(err)
	}
	defer s.Close()

	fmt.Println("Fetching updates from firehose")

	chdb := mdb.InsertChan(1000, 10*time.Second)
	defer close(chdb)

	counter := 0
	var q quote.Quote
	for {
		select {
		case err = <-chsrv:
			fatal(err)
		}
		if err = s.Read(&q); err != nil {
			fatal(err)
		}
		chdb <- q
		fmt.Printf("\r%8d %s %-30.30s", counter, time.Now().Format("Mon 15:04:05"), q.System)
		counter++
	}
}

func importlog(mdb *etdb.MarketDB, logf string) {
	s, err := quote.Open("firejson", logf)
	if err != nil {
		fatal(err)
	}
	defer s.Close()
	fmt.Println("Importing", logf)
	buf, bufp := make([]quote.Quote, 1e6), 0
	counter := 0
	for {
		var q quote.Quote
		if err = s.Read(&q); err != nil {
			if err == io.EOF {
				break
			} else {
				fatal(err)
			}
		}
		buf[bufp] = q
		bufp++
		counter++
		if counter%1000 == 0 {
			fmt.Printf("%8d\r", counter)
		}
		if bufp == len(buf) {
			if err = mdb.Insert(buf); err != nil {
				fatal(err)
			}
			bufp = 0
		}
	}
	fmt.Printf("%8d\n", counter)
	if err = mdb.Insert(buf[:bufp]); err != nil {
		fatal(err)
	}
	fmt.Println("Syncing database")
	if err = mdb.Checkpoint(); err != nil {
		fmt.Println(err)
	}
	fmt.Println("Import done")
}

func fatal(err error) {
	panic(err)
}
