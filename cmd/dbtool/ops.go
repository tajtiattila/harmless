package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/dbsql"
	"fmt"
	"runtime"
	"time"
)

type Op interface {
	Title() string
	Run(c *Context) error
}

// verboseOp sets verbose mode
type verboseOp struct{}

func (*verboseOp) Title() string { return "· verbose" }
func (*verboseOp) Run(c *Context) error {
	c.Verbose = true
	return nil
}

// numThreadsOp sets max number of worker threads
type numThreadsOp struct {
	n int
}

func (o *numThreadsOp) Title() string { return fmt.Sprint("· number of threads", o.n) }
func (o *numThreadsOp) Run(c *Context) error {
	c.Nthreads = o.n
	runtime.GOMAXPROCS(o.n)
	return nil
}

// pushOp pushes the active data down the stack
type pushOp struct{}

func (*pushOp) Title() string { return "· push" }
func (*pushOp) Run(c *Context) error {
	c.Stack = append(c.Stack, StackElem{})
	return nil
}

// popOp pops the top data off the stack
type popOp struct{}

func (*popOp) Title() string { return "· pop" }
func (*popOp) Run(c *Context) error {
	l := len(c.Stack)
	if l == 1 {
		return fmt.Errorf("stack has only one element")
	}
	c.Stack = c.Stack[:l-1]
	return nil
}

// readOp reads sqlite/json data
type readOp struct {
	kind, fn string
}

func (o *readOp) Title() string {
	return fmt.Sprintf("« read %s ‘%s’", o.kind, o.fn)
}

func (o *readOp) Run(c *Context) error {
	src, err := db.Open(o.kind, o.fn)
	if err != nil {
		return err
	}

	se := c.Top()
	se.Data = db.Data{}
	err = src.Read(&se.Data)
	if err == nil {
		se.Filename = o.fn
	}
	return err
}

// writeOp writes sqlite or json data
type writeOp struct {
	kind, fn string
}

func (o *writeOp) Title() string {
	return fmt.Sprintf("» write %s ‘%s’", o.kind, o.fn)
}

func (o *writeOp) Run(c *Context) error {
	dst, err := db.Open(o.kind, o.fn)
	if err != nil {
		return err
	}

	se := c.Top()

	fixinfer(&se.Data)

	err = dst.Write(&se.Data)
	if err == nil {
		se.Filename = o.fn
	}
	return err
}

// textDataOp reads new systems
type textDataOp struct {
	fn string
}

func (o *textDataOp) Title() string {
	return fmt.Sprintf("« new textdata ‘%s’", o.fn)
}

func (o *textDataOp) Run(c *Context) error {
	_, err := textsysdata(o.fn, &c.Top().Data)
	return err
}

// shiftOp shifts system coordinates
type shiftOp [3]float64

func (o shiftOp) Title() string { return fmt.Sprintf("· shift %v %v %v", o[0], o[1], o[2]) }

func (o shiftOp) Run(c *Context) error {
	data := c.Top()
	for _, sys := range data.Systems {
		for i := 0; i < 3; i++ {
			sys.P[i] += o[i]
		}
	}
	return nil
}

// newSysOp replaces systems at top of stack with the ones down below
type newSysOp struct {
	keepold bool // if true, old systems not found in new set are kept
}

func (o *newSysOp) Title() string {
	if o.keepold {
		return "· update systems"
	}
	return "· apply new systems"
}

func (o *newSysOp) Run(c *Context) error {
	data, upde := &c.Top().Data, c.Index(1)
	if upde == nil {
		return fmt.Errorf("no source on stack")
	}
	upd := &upde.Data

	var v []*db.System
	for _, ab := range matchsys(data, upd) {
		dsys, usys := ab.a, ab.b
		switch {
		case dsys == nil && usys == nil:
			return fmt.Errorf("internal error")
		case dsys == nil:
			c.Printf("  + ‘%s’ %v\n", usys.Name, usys.P)
		case usys == nil:
			var mark string
			if o.keepold {
				mark = "?"
			} else {
				mark = "-"
			}
			c.Printf("  %s ‘%s’ %v\n", mark, dsys.Name, dsys.P)
		default:
			c.Printf("    ‘%s’", dsys.Name)
			if dsys.P != usys.P {
				ofs := usys.P.Sub(dsys.P)
				c.Printf(" shift %v", ofs)
			}
			c.Println()
		}
		if usys != nil {
			if dsys != nil {
				v = append(v, mergesys(dsys, usys))
			} else {
				v = append(v, usys)
			}
		} else if o.keepold {
			v = append(v, dsys)
		}
	}
	sortnames(v)
	data.Systems = v
	return nil
}

// addedOp sets added and modified parameters for all systems
type addedOp struct {
	n string
	t time.Time
}

func (o *addedOp) Title() string { return "· added" }

func (o *addedOp) Run(c *Context) error {
	data := c.Top()
	for _, sys := range data.Systems {
		sys.Added = o.n
		sys.Modified = o.t
	}
	return nil
}

// inhabitOp adds missing stations to the data on top of the stack
// from the one below, assuming all systems in the source have a station
type inhabitOp struct{}

func (o *inhabitOp) Title() string { return "· inhabit" }

func (o *inhabitOp) Run(c *Context) error {
	data, upde := &c.Top().Data, c.Index(1)
	if upde == nil {
		return fmt.Errorf("no source on stack")
	}
	upd := &upde.Data

	for _, ab := range matchsys(data, upd) {
		dsys, usys := ab.a, ab.b
		if dsys != nil && usys != nil && len(dsys.Stations) == 0 {
			dsys.Stations = append(dsys.Stations, db.Station{
				Name:  dsys.Name + "-Station",
				Valid: db.Current,

				StationDetail: db.StationDetail{Inferred: true},
			})
		}
	}
	return nil
}

// mergePosOp removes duplicates by combining a systems at the same position
type mergePosOp struct{}

func (o *mergePosOp) Title() string { return "· merge positions" }

func (o *mergePosOp) Run(c *Context) error {
	data := &c.Top().Data

	m := make(map[db.Vec3][]*db.System)
	hasdup := false
	for _, sys := range data.Systems {
		m[sys.P] = append(m[sys.P], sys)
		if len(m[sys.P]) > 1 {
			hasdup = true
		}
	}
	if !hasdup {
		return nil
	}
	v := data.Systems
	data.Systems = make([]*db.System, 0, len(v))
	for _, sys := range v {
		vdup := m[sys.P]
		switch len(vdup) {
		case 0:
			// already processed
		case 1:
			// no dupe
			data.Systems = append(data.Systems, sys)
		default:
			// duplicates
			delete(m, sys.P)
			combined := vdup[0]
			for _, dup := range vdup[1:] {
				var n = combined.Name
				combined = mergesys(combined, dup)
				const glue = " · "
				combined.Name = n + glue + dup.Name
			}
			c.Println("  >", combined.Name)
			data.Systems = append(data.Systems, combined)
		}
	}
	data.CalcSystemIds()
	data.CalcSysStationIds()
	return nil
}
