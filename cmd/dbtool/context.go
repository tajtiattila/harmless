package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/dbsql"
	"fmt"
)

type Context struct {
	Stack    []StackElem
	Verbose  bool
	Nthreads int
}

type StackElem struct {
	db.Data
	Filename string
}

func (c *Context) Top() *StackElem {
	return &c.Stack[len(c.Stack)-1]
}

func (c *Context) Index(i int) *StackElem {
	i = len(c.Stack) - i - 1
	if i < 0 {
		return nil
	}
	return &c.Stack[i]
}

func (c *Context) Print(v ...interface{}) {
	if c.Verbose {
		fmt.Print(v...)
	}
}

func (c *Context) Println(v ...interface{}) {
	if c.Verbose {
		fmt.Println(v...)
	}
}

func (c *Context) Printf(f string, v ...interface{}) {
	if c.Verbose {
		fmt.Printf(f, v...)
	}
}

func (c *Context) Error(v ...interface{}) {
	fmt.Print(v...)
}

func (c *Context) Errorln(v ...interface{}) {
	fmt.Println(v...)
}

func (c *Context) Errorf(f string, v ...interface{}) {
	fmt.Printf(f, v...)
}
