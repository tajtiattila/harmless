package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

type tdexp struct {
	fn string
}

func (o *tdexp) Title() string {
	return fmt.Sprintf("« read TradeDangerous db %s", o.fn)
}

func (o *tdexp) Run(c *Context) error {
	data := &c.Top().Data
	path, err := tmpCopy(o.fn, "elite.sqlite")
	if err != nil {
		return err
	}
	defer os.Remove(path)

	src, err := db.Open("sqlite", path)
	if err != nil {
		return err
	}
	src.Close()

	xdb, err := sql.Open("sqlite3", path)
	if err != nil {
		return err
	}
	defer xdb.Close()
	if err = runSql(xdb, fixupsql); err != nil {
		return err
	}
	xdb.Close()

	src, err = db.Open("sqlite", path)
	if err != nil {
		return err
	}

	return src.Read(data)
}

func tmpCopy(fn, tmp string) (string, error) {
	forig, err := os.Open(fn)
	if err != nil {
		return "", err
	}
	defer forig.Close()
	ftmp, err := ioutil.TempFile("", tmp)
	if err != nil {
		return "", err
	}
	path := ftmp.Name()
	_, err = io.Copy(ftmp, forig)
	ftmp.Close()
	if err != nil {
		os.Remove(path)
		return "", err
	}
	return path, nil
}

func runSql(db *sql.DB, sql string) error {
	for _, stmt := range strings.SplitAfter(sql, ";\n") {
		_, err := db.Exec(stmt)
		if err != nil {
			fmt.Fprintln(os.Stderr, stmt, err)
		}
	}
	return nil
}

var fixupsql = `

ALTER TABLE Ship ADD COLUMN
   value INTEGER NOT NULL DEFAULT 0;
ALTER TABLE Ship ADD COLUMN
   manufacturer VARCHAR(40) COLLATE nocase;
ALTER TABLE System ADD COLUMN
   valid_id INTEGER NOT NULL DEFAULT 179;
ALTER TABLE Station ADD COLUMN
   shipyard BOOL;
ALTER TABLE Station ADD COLUMN
   outfitting BOOL;
ALTER TABLE Station ADD COLUMN
   commodity_market BOOL;
ALTER TABLE Station ADD COLUMN
   black_market BOOL;
ALTER TABLE Station ADD COLUMN
   valid_id INTEGER NOT NULL DEFAULT 179;
ALTER TABLE Station ADD COLUMN
   inferred BOOL NOT NULL DEFAULT FALSE;

INSERT OR IGNORE INTO Type(name) VALUES('Unknown');
INSERT INTO Component(component_id,type_id,name,mass)
  SELECT u.upgrade_id,t.type_id,u.name,u.weight
    FROM Upgrade u
    INNER JOIN Type t ON t.name='Unknown';
INSERT INTO ComponentVendor(component_id,station_id,cost)
  SELECT upgrade_id,station_id,cost
    FROM UpgradeVendor;

ALTER TABLE SystemDetail RENAME TO SystemDetailOrig;
CREATE TABLE SystemDetail
 (
   system_id INTEGER NOT NULL,
   allegiance_id INTEGER NOT NULL,
   government_id INTEGER NOT NULL,
   economy_id INTEGER NOT NULL,
   population INTEGER NOT NULL,
   valid_id INTEGER NOT NULL DEFAULT 179,

   UNIQUE (system_id),

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (allegiance_id) REFERENCES Allegiance(allegiance_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (government_id) REFERENCES Government(government_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE,
   FOREIGN KEY (economy_id) REFERENCES Economy(economy_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );
INSERT INTO Allegiance(name)
	SELECT allegiance FROM SystemDetailOrig GROUP BY allegiance;
INSERT INTO Economy(name)
	SELECT economy FROM SystemDetailOrig GROUP BY economy;
INSERT INTO Government(name)
	SELECT government FROM SystemDetailOrig GROUP BY government;
INSERT INTO SystemDetail(system_id,allegiance_id,government_id,economy_id,population)
	SELECT sd.system_id,a.allegiance_id,g.government_id,e.economy_id,sd.population
		FROM SystemDetailOrig sd
			INNER JOIN Allegiance a ON sd.allegiance=a.name
			INNER JOIN Economy e ON sd.economy=e.name
			INNER JOIN Government g ON sd.government=g.name;

UPDATE Station SET inferred = (Station.name == ((SELECT sys.name FROM System sys WHERE sys.system_id=Station.system_id) || '-Station'));
`
