package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strings"
)

func textsysdata(fn string, data *db.Data) (nread int, err error) {
	f, err := os.Open(fn)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return 0, err
	}

	t := fi.ModTime().UTC()

	data.Systems = data.Systems[:0]

	scanner, n := bufio.NewScanner(f), 0
	var fparse func(*db.System, string) error
	for scanner.Scan() {
		line := scanner.Text()
		if strings.TrimSpace(line) == "" {
			continue
		}
		sys := new(db.System)
		if n == 0 {
			if line[0] == '#' {
				continue
			}
			err = parsewcoord(sys, line)
			if err == nil {
				fparse = parsewcoord
			} else {
				fparse = parsename
			}
		}
		err = fparse(sys, line)
		if err != nil {
			return n, err
		}
		sys.Modified = t
		sys.Valid = db.Current
		data.Systems = append(data.Systems, sys)
		n++
	}
	return n, nil
}

func parsename(p *db.System, line string) error {
	p.Name = strings.TrimSpace(line)
	p.P = db.Vec3{}
	return nil
}

func parsewcoord(p *db.System, line string) error {
	line = strings.TrimSpace(line)
	i := strings.LastIndex(line, " ")
	if i < 0 {
		return fmt.Errorf("‘%s’: missing Z", line)
	}
	i = strings.LastIndex(line[:i], " ")
	if i < 0 {
		return fmt.Errorf("‘%s’: missing Y", line)
	}
	i = strings.LastIndex(line[:i], " ")
	if i < 0 {
		i = 0
	} else {
		p.Name = strings.TrimSpace(line[:i])
		i++
	}
	var x, y, z float64
	_, err := fmt.Sscan(line[i:], &x, &y, &z)
	if err != nil {
		return err
	}
	p.P = db.Vec3{x, y, z}
	for i := range p.P {
		p.P[i] = math.Floor((p.P[i]*32)+0.5) / 32
	}
	return nil
}

func mergesys(sys, update *db.System) (nsys *db.System) {
	nsys = new(db.System)
	*nsys = *update
	nsys.DbKey = sys.DbKey
	m := make(map[string]bool)
	var infermin, updbest, bestinferred db.Validity
	for _, stn := range update.Stations {
		if !stn.Inferred {
			m[strings.ToLower(stn.Name)] = true
			if stn.Valid > updbest {
				infermin = stn.Valid + 1
				updbest = stn.Valid
			}
		} else {
			if stn.Valid > updbest {
				bestinferred = stn.Valid
			}
		}
	}
	for _, stn := range sys.Stations {
		if !stn.Inferred {
			if stn.Valid+1 > infermin {
				infermin = stn.Valid + 1
			}
		} else {
			if stn.Valid > bestinferred {
				bestinferred = stn.Valid
			}
		}
	}

	nsys.Stations = nsys.Stations[:0]
	for _, stn := range update.Stations {
		if !stn.Inferred {
			nsys.Stations = append(nsys.Stations, stn)
		}
	}
	for _, stn := range sys.Stations {
		// keep old stations
		if !stn.Inferred && stn.Valid >= updbest && !m[strings.ToLower(stn.Name)] {
			nsys.Stations = append(nsys.Stations, stn)
		}
	}
	if bestinferred != db.Unknown && bestinferred >= infermin {
		nsys.Stations = append(nsys.Stations, db.Station{
			Valid: bestinferred,
			Name:  "Unknown Station",

			StationDetail: db.StationDetail{Inferred: true},
		})
	}

	if nsys.SystemDetail == nil {
		nsys.SystemDetail = sys.SystemDetail
	}
	return
}

func fixinfer(data *db.Data) {
	for _, sys := range data.Systems {
		for istn := range sys.Stations {
			stn := &sys.Stations[istn]
			if stn.Inferred {
				stn.Name = sys.Name + "-Station"
			}
		}
	}
}

func sortnames(v []*db.System) {
	sort.Sort(sortname(v))
}

type sortname []*db.System

func (s sortname) Len() int           { return len(s) }
func (s sortname) Less(i, j int) bool { return strings.ToLower(s[i].Name) < strings.ToLower(s[j].Name) }
func (s sortname) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

func dstr(dist float64) string {
	return fmt.Sprintf("%.2fLy", dist)
}
