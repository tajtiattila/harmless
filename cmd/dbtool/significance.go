package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"math"
	"sort"
)

type signifOp struct{}

const (
	// Signif value for insignificant systems
	insignificant = 20

	// minimal Signif for systems with small significance
	minsignificant = 30

	// given a more significant system at range
	// use this multiplier for maximum on range to calculate significance
	abssignifmul = 0.1

	moresignificantnear = 10

	maxsamesignifnear = 20
)

func (o *signifOp) Title() string {
	return "· calculate significance"
}

func (o *signifOp) Run(c *Context) error {
	data := c.Top()

	vsys := make([]*db.System, len(data.Systems))
	copy(vsys, data.Systems)
	sortbasesignif(vsys)

	vcmp := make([]*db.System, len(vsys))
	for i, sys := range vsys {
		maxsignif := float64(1e9)
		if i == 0 {
			sys.Signif = maxsignif
			continue
		}
		isig := basesignif(sys)
		if isig == 0 {
			sys.Signif = insignificant
			continue
		}
		copy(vcmp, vsys)
		sortnear(sys, vcmp)
		nnear := 0
		c.Println("  >", sys.Name, isig)
		for _, cmp := range vcmp[1:] {
			jsig := basesignif(cmp)
			if jsig >= isig {
				dist := cmp.P.Sub(sys.P).Abs()
				c.Println("    |", cmp.Name, jsig, dstr(dist))
				nnear++
				if nnear >= maxsamesignifnear {
					maxsignif = math.Min(maxsignif, dist)
					break
				}
				if jsig > isig {
					// max. possible signif is dist to closest system
					// with larger basesignif
					maxsignif = math.Min(maxsignif, dist*moresignificantnear)
				}
			}
		}
		if maxsignif < minsignificant {
			sys.Signif = minsignificant
		} else {
			sys.Signif = math.Floor(maxsignif)
		}
		c.Println("  <", sys.Name, sys.Signif)
	}
	return nil
}

var abssignif = make(map[string]int)

func init() {
	// init systems with absolute significance
	for i, n := range []string{
		"Sol",
		"Achenar",
		"Lave",
		"Eranin",
	} {
		abssignif[n] = 1000 - i
	}
}

func basesignif(sys *db.System) (v int) {
	if v = abssignif[sys.Name]; v != 0 {
		return
	}
	if sys.SystemDetail != nil {
		// systems having a habitable planet should be significant
		// assume having Agriculture or Tourism to have a habitable planet
		switch sys.Econ {
		case "Tourism", "Agriculture":
			v += 20
		case "None", "", "Extraction":
			// pass
		default:
			v += 10
		}
		pop := sys.Pop
		if pop <= 0 && len(sys.Stations) != 0 {
			pop = 1e5
		}
		if 0 < pop {
			v += int(math.Log10(float64(pop)) + 0.1)
		}
	} else {
		if len(sys.Stations) != 0 {
			v = 8
		}
	}
	return v
}

func sortbasesignif(v []*db.System) {
	sort.Sort(&sortsysf{
		v: v,
		f: func(s *db.System) float64 { return float64(-basesignif(s)) },
	})
}

func sortnear(c *db.System, v []*db.System) {
	sort.Sort(&sortsysf{
		v: v,
		f: func(s *db.System) float64 { return s.P.Sub(c.P).Abssq() },
	})
}

type sortsysf struct {
	v []*db.System
	f func(*db.System) float64
}

func (s *sortsysf) Len() int           { return len(s.v) }
func (s *sortsysf) Swap(i, j int)      { s.v[i], s.v[j] = s.v[j], s.v[i] }
func (s *sortsysf) Less(i, j int) bool { return s.f(s.v[i]) < s.f(s.v[j]) }
