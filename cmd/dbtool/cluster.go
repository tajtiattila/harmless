package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"fmt"
	"math"
	"sort"
)

// mkClusterMap generates the cluster map for all jump ranges
// the result may be stored later in SQL
func mkClusterMap(maxr, rstep float64) map[*db.System][]clusterInfo {
	m := make(map[*db.System][]clusterInfo)
	w := make(map[int][]*db.System)
	for i, sys := range db.Systems {
		m[sys] = []clusterInfo{{i + 1, 0}}
		w[i+1] = []*db.System{sys}
	}

	nsteps := int(maxr/rstep) + 1
	var connect []int
	var dstep int
	for step := 0; step <= nsteps; step += dstep {
		dstep = 100
		if 3*step/2 > dstep {
			dstep = 3 * step / 2
		}
		jumprange := float64(step) * rstep
		testrange := jumprange + float64(dstep)*rstep
		fmt.Printf("%6.2f %d\n", jumprange, len(w))
		// find clusters that can be connected
		for cls, vsys := range w {
			connect = connect[:0]
			for _, sys := range vsys {
				db.DefaultData.Sphere(sys.P, testrange, func(cmp *db.System, dist float64) {
					vm := m[cmp]
					if xcls := vm[len(vm)-1].cluster; xcls != cls {
						xstep := int(math.Ceil((dist - jumprange) / rstep))
						if xstep <= 0 {
							panic("inconsistency")
						}
						switch {
						case xstep < dstep:
							dstep = xstep
							connect = connect[:0]
							fallthrough
						case xstep == dstep:
							connect = append(connect, xcls)
						}
					}
				})
			}
			if len(connect) != 0 {
				nextrange := jumprange + float64(dstep)*rstep
				connect = append(connect, cls)
				sort.Ints(connect)
				cls = connect[0] // use smallest
				for _, xcls := range connect[1:] {
					if xcls == cls {
						continue // may appear multiple times
					}
					vsys := w[xcls]
					if vsys == nil {
						continue // dupe in connect
					}
					delete(w, xcls)
					w[cls] = append(w[cls], vsys...)
					for _, sys := range vsys {
						m[sys] = append(m[sys], clusterInfo{cls, nextrange})
					}
				}
			}
		}
	}
	return m
}

// clusterInfo holds the cluster id to use
// when jump range is equal to or greather than rng
type clusterInfo struct {
	cluster int
	rng     float64
}
