package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"math"
)

type statOp struct{}

func (o *statOp) Title() string {
	return "· calculate statistics"
}

func (o *statOp) Run(c *Context) error {
	data := c.Top()

	c.Printf("  %d systems\n", len(data.Systems))

	chart := db.NewOctreeChart(data.Systems)
	var radii = []float64{5, 7, 10, 15, 25, 50}
	for _, r := range radii {
		mbox, msphere := 0, 0
		for _, p := range data.Systems {
			nbox, nsphere := 0, 0
			mi, ma := p.P.Sub(db.Vec3{r, r, r}), p.P.Add(db.Vec3{r, r, r})
			chart.Box(mi, ma, func(q *db.System) {
				p := q.P
				if mi[0] <= p[0] && mi[1] <= p[1] && mi[2] <= p[2] &&
					p[0] < ma[0] && p[1] < ma[1] && p[2] < ma[2] {
					nbox++
				}
			})
			chart.Sphere(p.P, r, func(q *db.System, dist float64) {
				if dist <= r {
					nsphere++
				}
			})
			if mbox < nbox {
				mbox = nbox
			}
			if msphere < nsphere {
				msphere = nsphere
			}
		}
		ubox := (8 * r * r * r) / float64(mbox)
		usphere := (4 / 3 * math.Pi * r * r * r) / float64(msphere)
		c.Printf("  r=%v box=%d (%.2f Ly³/star) sphere=%d (%.2f Ly³/star)\n", r, mbox, ubox, msphere, usphere)
	}
	return nil
}
