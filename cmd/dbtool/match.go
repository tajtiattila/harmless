package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"fmt"
	"strings"
)

type absys struct {
	a, b *db.System
}

// matchsys finds matching systems in a and b, and returns the mapping in a slice
// each system in a and b may appear only once in the returned slice
func matchsys(a, b *db.Data) []absys {
	nm := make(map[string]*mnamep)
	for _, sys := range a.Systems {
		lsysn := strings.ToLower(sys.Name)
		p := nm[lsysn]
		if p == nil {
			p = new(mnamep)
			nm[lsysn] = p
		}
		p.a = append(p.a, sys)
	}

	for _, sys := range b.Systems {
		lsysn := strings.ToLower(sys.Name)
		p := nm[lsysn]
		if p == nil {
			p = new(mnamep)
			nm[lsysn] = p
		}
		p.b = append(p.b, sys)
	}

	r := makeabmapper()

	// match systems on name
	for _, m := range nm {
		la, lb := len(m.a), len(m.b)
		switch {
		case la == 0:
			// 0 ← n
			for _, sys := range m.b {
				r.add(nil, sys)
			}
		case lb == 0:
			// n ← 0
			for _, sys := range m.a {
				r.add(sys, nil)
			}
		case la == 1 && lb == 1:
			// 1 ← 1
			r.add(m.a[0], m.b[0])
		default:
			// n ← m
			va, vb := m.a, m.b
			swap := false
			if len(va) < len(vb) {
				swap = true
				va, vb = vb, va
			}
			var bestperm []int
			var bestdist2 float64
			vi := startperm(len(va))
			for np := true; np; np = nextperm(vi) {
				var dist2 float64
				for i := range vb {
					asys, bsys := va[vi[i]], vb[i]
					dist2 += asys.P.Sub(bsys.P).Abssq()
				}
				if bestperm == nil || dist2 < bestdist2 {
					bestperm = append(bestperm[:0], vi...)
					bestdist2 = dist2
				}
			}
			for i := range va {
				asys := va[bestperm[i]]
				var bsys *db.System
				if i < len(vb) {
					bsys = vb[i]
				} else {
					bsys = nil
				}
				if swap {
					r.add(bsys, asys)
				} else {
					r.add(asys, bsys)
				}
			}
		}
	}

	// match systems on position
	for p, asys := range r.am {
		if bsys, ok := r.bm[p]; ok {
			delete(r.am, p)
			delete(r.bm, p)
			r.v = append(r.v, absys{asys, bsys})
		}
	}

	// append systems left after position matching
	for _, sys := range r.am {
		r.v = append(r.v, absys{sys, nil})
	}
	for _, sys := range r.bm {
		r.v = append(r.v, absys{nil, sys})
	}
	return r.v
}

type abmapper struct {
	v  []absys
	am map[db.Vec3]*db.System
	bm map[db.Vec3]*db.System
}

func makeabmapper() abmapper {
	return abmapper{
		am: make(map[db.Vec3]*db.System),
		bm: make(map[db.Vec3]*db.System),
	}
}

func (m *abmapper) add(a, b *db.System) {
	if false {
		if a != nil {
			fmt.Printf("%s %v", a.Name, a.P)
		} else {
			fmt.Print("nil")
		}
		fmt.Print(" ←→ ")
		if b != nil {
			fmt.Printf("%s %v", b.Name, b.P)
		} else {
			fmt.Print("nil")
		}
		fmt.Println()
	}
	switch {
	case a == nil:
		if m.bm[b.P] != nil {
			panic(fmt.Errorf("duplicate position: %s @ %v", b.Name, b.P))
		}
		m.bm[b.P] = b
	case b == nil:
		if m.am[a.P] != nil {
			panic(fmt.Errorf("duplicate position: %s @ %v", a.Name, a.P))
		}
		m.am[a.P] = a
	default:
		m.v = append(m.v, absys{a, b})
	}
}

type mnamep struct {
	a []*db.System
	b []*db.System
}

func startperm(n int) (a []int) {
	a = make([]int, n)
	for i := range a {
		a[i] = i
	}
	return a
}

func nextperm(a []int) bool {
	if len(a) < 2 {
		return false
	}
	// en.wikipedia.org/wiki/Permutation
	//
	// 1. Find the largest index k such that a[k] < a[k + 1].
	// If no such index exists, the permutation is the last permutation.
	for k := len(a) - 2; 0 <= k; k-- {
		if a[k] < a[k+1] {
			// 2. Find the largest index l greater than k such that a[k] < a[l].
			for l := len(a) - 1; k < l; l-- {
				if a[k] < a[l] {
					// 3. Swap the value of a[k] with that of a[l].
					a[k], a[l] = a[l], a[k]
					// 4. Reverse the sequence from a[k + 1] up to and including the final element a[n].
					for i, j := k+1, len(a)-1; i < j; i, j = i+1, j-1 {
						a[i], a[j] = a[j], a[i]
					}
					return true
				}
			}
			panic("impossible")
		}
	}
	return false
}
