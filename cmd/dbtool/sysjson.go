package main

import (
	"bitbucket.org/tajtiattila/harmless/db"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

const timeFmt = "2006-01-02T15:04:05.999Z07:00"

type SysJsonSys struct {
	Name string `json:"name"`

	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`

	Contr    string `json:"contributor,omitempty"`
	Region   string `json:"region,omitempty"`
	Modified string `json:"contributed,omitempty"`

	Allegiance string `json:"allegiance,omitempty"`
	Economy    string `json:"economy,omitempty"`
	Government string `json:"government,omitempty"`
	Population int64  `json:"population,omitempty"`
	Security   string `json:"security,omitempty"`

	Stations   []sjStation `json:"stations,omitempty"`
	Calculated bool        `json:"calculated,omitempty"`
	Distances  []sysDist   `json:"distances,omitempty"`
}

type sysDist struct {
	Ref  string      `json:"system"`
	Dist interface{} `json:"distance"`
}

type sjStation struct {
	Name string  `json:"station"`
	Dist float64 `json:"distance,omitempty"`

	Type    string `json:"type,omitempty"`
	Faction string `json:"faction,omitempty"`
	PadSize string `json:"largest_pad,omitempty"`

	Cmarket  *bool `json:"commodities_market,omitempty"`
	Bmarket  *bool `json:"black_market,omitempty"`
	Shipyard *bool `json:"shipyard,omitempty"`
	Outfit   *bool `json:"outfitting,omitempty"`
}

type readSysJson struct {
	fn string
}

func (o *readSysJson) Title() string { return fmt.Sprintf("· read systems-json ‘%s’", o.fn) }

func (o *readSysJson) Run(c *Context) error {
	f, err := os.Open(o.fn)
	if err != nil {
		return err
	}
	defer f.Close()

	var v []SysJsonSys
	if err = json.NewDecoder(f).Decode(&v); err != nil {
		return err
	}

	data := c.Top()
	data.Systems = data.Systems[:0]
	for _, js := range v {
		if strings.HasSuffix(strings.ToLower(js.Name), "(removed)") {
			continue
		}
		const unvertag = "(unverified)"
		if strings.HasSuffix(strings.ToLower(js.Name), unvertag) {
			js.Name = strings.TrimSpace(strings.TrimSuffix(js.Name, unvertag))
		}
		added, valid := o.addedValidValues(js.Contr, js.Region)
		m, err := time.Parse(timeFmt, js.Modified)
		if err != nil {
			return err
		}
		sys := &db.System{
			Name:     js.Name,
			P:        db.Vec3{js.X, js.Y, js.Z},
			Added:    added,
			Valid:    valid,
			Modified: m,
		}
		if js.Allegiance != "" {
			sys.SystemDetail = &db.SystemDetail{
				Alleg: js.Allegiance,
				Econ:  fixecon(js.Economy),
				Govt:  js.Government,
				Pop:   js.Population,
				Sec:   db.ParseSecurity(js.Security),
				Valid: valid,
			}
		}
		for _, jt := range js.Stations {
			stn := db.Station{
				Name: jt.Name,
				Dist: jt.Dist,
				StationDetail: db.StationDetail{
					Type:     jt.Type,
					Faction:  jt.Faction,
					Shipyard: sysjbool(jt.Shipyard),
					Outfit:   sysjbool(jt.Outfit),
					Cmarket:  sysjbool(jt.Cmarket),
					Bmarket:  sysjbool(jt.Bmarket),
				},
				Valid: valid,
			}

			sys.Stations = append(sys.Stations, stn)
		}
		data.Systems = append(data.Systems, sys)
	}
	return nil
}

func (o *readSysJson) addedValidValues(contr, reg string) (added string, valid db.Validity) {
	xadd, remark := reg, ""
	if n := strings.IndexRune(reg, '('); n != -1 {
		xadd, remark = strings.TrimSpace(reg[:n]), strings.TrimSpace(reg[n+1:])
		if len(remark) != 0 && remark[len(remark)-1] == ')' {
			remark = remark[:len(remark)-1]
		}
	}
	added = xadd
	if strings.HasPrefix(xadd, "Gamma") ||
		strings.HasPrefix(xadd, "Beta") ||
		strings.HasPrefix(xadd, "Premium Beta") ||
		strings.HasPrefix(xadd, "Alpha") {
		if remark == "unverified" {
			valid = db.ParseValidity(xadd)
		} else {
			valid = db.Current
		}
	} else {
		valid = db.Invalid
	}
	return
}

type updSysJson struct {
	fn string
}

func (o *updSysJson) Title() string { return fmt.Sprintf("· update systems-json ‘%s’", o.fn) }

func (o *updSysJson) Run(c *Context) error {
	f, err := os.Open(o.fn)
	if err != nil {
		return err
	}

	var src []json.RawMessage
	err = json.NewDecoder(f).Decode(&src)
	f.Close()

	if err != nil {
		return err
	}

	data := c.Top()
	names := make(map[string]*db.System)
	for _, sys := range data.Systems {
		names[sys.Name] = sys
	}

	v := make([]sysJsonPart, 0, len(src))
	for i := range src {
		var js SysJsonSys
		if err = json.Unmarshal([]byte(src[i]), &js); err != nil {
			return err
		}
		const unvertag = "(unverified)"
		js.Name = strings.TrimSpace(strings.TrimSuffix(js.Name, unvertag))

		oldbits, err := json.Marshal(js)
		if err != nil {
			return err
		}
		if sys, ok := names[js.Name]; ok {
			updSysJsonSys(&js, sys)
			delete(names, js.Name)
			newbits, err := json.Marshal(js)
			if err != nil {
				return err
			}
			if same(newbits, oldbits) {
				c.Println("  ●", js.Name)
				v = append(v, sysJsonRaw(src[i]))
			} else {
				c.Println("  ○", js.Name)
				v = append(v, js)
			}
		}
	}

	for _, sys := range names {
		c.Println("  +", sys.Name)
		var js SysJsonSys
		js.Contr = "starchart"
		js.Region = "Gamma"
		updSysJsonSys(&js, sys)
		v = append(v, js)
	}

	f, err = os.Create(o.fn)
	if err != nil {
		return err
	}
	defer f.Close()

	xf := &textWriter{w: f}
	fmt.Fprintln(xf, "[")
	for i, ob := range v {
		data, err := ob.marshal()
		if err != nil {
			return err
		}
		fmt.Fprint(xf, "  ")
		_, err = xf.Write(data)
		if err != nil {
			return err
		}
		if i != len(v)-1 {
			fmt.Fprintln(xf, ",")
		} else {
			fmt.Fprintln(xf)
		}
	}
	fmt.Fprintln(xf, "]")
	return err
}

type sysJsonPart interface {
	marshal() ([]byte, error)
}

type sysJsonRaw []byte

func (r sysJsonRaw) marshal() ([]byte, error) {
	return []byte(r), nil
}

func (js SysJsonSys) marshal() ([]byte, error) {
	return json.MarshalIndent(js, "  ", "  ")
}

func same(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func popsfx(s *string, sfx string) bool {
	if strings.HasSuffix(strings.ToLower(*s), sfx) {
		*s = strings.TrimSpace((*s)[:len(*s)-len(sfx)])
		return true
	}
	return false
}

func fixecon(s string) string {
	if s == "Agricultural" {
		return "Agriculture"
	}
	return s
}

func sysjbool(i interface{}) *bool {
	switch x := i.(type) {
	case bool:
		return &x
	case *bool:
		return x
	}
	return nil
}

func updSysJsonSys(js *SysJsonSys, sys *db.System) {
	if js.Modified == "" {
		js.Modified = sys.Modified.Format(timeFmt)
	} else {
		m, err := time.Parse(timeFmt, js.Modified)
		if err != nil || m != sys.Modified {
			js.Modified = sys.Modified.Format(timeFmt)
		}
	}
	if sys.SystemDetail != nil {
		js.Allegiance = sys.Alleg
		js.Economy = sys.Econ
		js.Government = sys.Govt
		pop := sys.Pop
		if pop < 0 {
			pop = 0
		}
		js.Population = pop
		var sec string
		if sys.Sec != db.SecUnknown {
			sec = sys.Sec.String()
		}
		js.Security = strings.Title(sec)
	}

	m := make(map[string]*db.Station)
	for i := range sys.Stations {
		stn := &sys.Stations[i]
		m[stn.Name] = stn
	}

	var xs []sjStation
	for _, jst := range js.Stations {
		if stn := m[jst.Name]; stn != nil {
			jstn, ok := mkSysJsonStation(*stn)
			if ok {
				xs = append(xs, jstn)
			}
			delete(m, jst.Name)
		}
	}

	js.Stations = xs
	for _, stn := range sys.Stations {
		if m[stn.Name] == nil {
			continue // already added
		}
		jstn, ok := mkSysJsonStation(stn)
		if ok {
			js.Stations = append(js.Stations, jstn)
		}
	}
}

func mkSysJsonStation(stn db.Station) (sjStation, bool) {
	if stn.Inferred {
		return sjStation{}, false
	}
	return sjStation{
		Name:     stn.Name,
		Dist:     stn.Dist,
		Type:     stn.Type,
		Shipyard: stn.Shipyard,
		Outfit:   stn.Outfit,
		Cmarket:  stn.Cmarket,
		Bmarket:  stn.Bmarket,

		Faction: stn.Faction,
		PadSize: calcPadSize(stn),
	}, true
}

func calcPadSize(stn db.Station) string {
	if stn.Type == "" {
		return ""
	}
	if strings.HasSuffix(stn.Type, "Starport") {
		return "large"
	}
	if strings.ToLower(stn.Type) == "unsanctioned outpost" {
		return "small"
	}
	return "medium"
}

type textWriter struct {
	w  io.Writer
	cr bool
}

func (t *textWriter) Write(p []byte) (nn int, err error) {
	n, s := 0, 0
	for i := range p {
		switch p[i] {
		case '\r':
			t.cr = true
		case '\n':
			if !t.cr {
				n, err = t.w.Write(p[s:i])
				nn += n
				if err != nil {
					return
				}
				_, err = t.w.Write([]byte{'\r'})
				if err != nil {
					return
				}
				s = i
			}
			t.cr = false
		default:
			t.cr = false
		}
	}
	n, err = t.w.Write(p[s:])
	nn += n
	return
}

/*
type stnSort []sjStation

func (s stnSort) Len() int      { return len(s) }
func (s stnSort) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s stnSort) Less(i, j int) bool {
	d := s[i].Dist - s[j].Dist
	if d != 0 {
		return d < 0
	}
	return strings.ToLower(s[i].Name) < strings.ToLower(s[j].Name)
}
*/
