package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"

	"bitbucket.org/tajtiattila/harmless/storage"
	"bitbucket.org/tajtiattila/harmless/storage/edit"
	_ "bitbucket.org/tajtiattila/harmless/storage/sqlite"
)

func main() {
	var dataf, jsonf string
	flag.StringVar(&dataf, "data", "./data/elite.sqlite", "path to db")
	flag.StringVar(&jsonf, "json", "./data/elite.json", "db export file name")
	flag.Parse()

	stor, err := storage.Open("sqlite", dataf)
	verify(err)
	defer stor.Close()

	if flag.NArg() == 0 {
		fmt.Fprintln(os.Stderr, "%v needs at least one file argument", os.Args[0])
		os.Exit(1)
	}

	var stat edit.Stat
	for _, elf := range flag.Args() {
		l, err := os.Open(elf)
		verify(err)
		defer l.Close()

		pr, pw := io.Pipe()

		go func() {
			verify(fixposts(l, pw))
			pw.Close()
		}()

		h := edit.NewHandler(stor)
		s, err := h.Load(pr, func(p *edit.Post, err error) error {
			if err != nil {
				fmt.Fprintf(os.Stderr, "%v:\n  %#v\n", err, p)
			}
			return nil
		})
		verify(err)
		stat.Nok += s.Nok
		stat.Ntotal += s.Ntotal
	}

	fmt.Fprintf(os.Stderr, "Loaded %d/%d edits\n", stat.Nok, stat.Ntotal)

	if jsonf != "" {
		if dumpio, ok := stor.(storage.DumpIO); ok {
			f, err := os.Create(jsonf)
			verify(err)
			defer f.Close()

			fmt.Fprintf(os.Stderr, "Exporting %q into %q\n", dataf, jsonf)
			verify(storage.Export(dumpio, f))
		}
	}
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func fixposts(r io.Reader, w io.Writer) error {
	j := json.NewDecoder(r)
	for {
		var m json.RawMessage
		if err := j.Decode(&m); err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
		b := []byte(m)
		if err := checkpost(b); err != nil {
			var x map[string]interface{}
			if err := json.Unmarshal(b, &x); err != nil {
				return err // invalid json
			}
			if f := At(x, "data", "id"); f.IsString() {
				f.Set(0)
			}
			if f := At(x, "data", "valid"); f.IsString() {
				switch f.Value.(string) {
				case "Gamma":
					f.Set(true)
				case "invalid":
					f.Set(false)
				default:
					f.Set(nil)
				}
			}
			if f := At(x, "data", "stations"); f.IsArray() {
				v := f.Value.([]interface{})
				for _, stn := range v {
					if f := At(stn, "id"); f.Value != nil {
						f.Set(0)
					}
				}
			}
			b, err = json.Marshal(x)
			if err != nil {
				return err // can't remarshal object
			}
			if err = checkpost(b); err != nil {
				fmt.Printf("%s\n", b)
				return err // still not ok
			}
		}
		if _, err := w.Write(b); err != nil {
			return err
		}
	}
	panic("not reached")
}

func checkpost(b []byte) error {
	var p edit.Post
	return json.Unmarshal(b, &p)
}

type Field struct {
	Value interface{}
	Set   func(v interface{})
}

func (f Field) IsString() bool {
	if f.Value == nil {
		return false
	}
	_, ok := f.Value.(string)
	return ok
}

func (f Field) IsArray() bool {
	if f.Value == nil {
		return false
	}
	_, ok := f.Value.([]interface{})
	return ok
}

func At(i interface{}, at ...interface{}) (f Field) {
	f.Value = i
	for _, p := range at {
		switch x := p.(type) {
		case int:
			v, ok := f.Value.([]interface{})
			if !ok {
				return Field{}
			}
			f.Set = func(v []interface{}, x int) func(v interface{}) {
				return func(w interface{}) {
					v[x] = w
				}
			}(v, x)
			f.Value = v[x]
		case string:
			m, ok := f.Value.(map[string]interface{})
			if !ok {
				return Field{}
			}
			f.Set = func(m map[string]interface{}, x string) func(v interface{}) {
				return func(w interface{}) {
					m[x] = w
				}
			}(m, x)
			f.Value = m[x]
		}
	}
	return
}
