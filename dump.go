package main

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/storage"
)

type Dumper struct {
	dumpio storage.DumpIO

	Backup Backupper // optional backup handler

	mtx     sync.RWMutex
	modtime time.Time
	p, gzp  []byte
}

type Backupper interface {
	Backup(modtime time.Time, gzdata []byte) error
}

func NewDumper(dio storage.DumpIO, bkup Backupper) *Dumper {
	d := &Dumper{
		dumpio: dio,
		Backup: bkup,
	}
	d.update()

	go func() {
		for _ = range time.Tick(5 * time.Minute) {
			d.update()
		}
	}()

	return d
}

func (d *Dumper) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	log.Println("Dumper.ServeHTTP", len(d.p), len(d.gzp))
	d.mtx.RLock()
	var data []byte
	if strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") {
		w.Header().Set("Content-Encoding", "gzip")
		data = d.gzp
	} else {
		data = d.p
	}
	mt := d.modtime
	d.mtx.RUnlock()

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	http.ServeContent(w, req, "elite.json", mt, bytes.NewReader(data))
}

func (d *Dumper) update() {
	vsys, err := d.dumpio.GetSystems()
	if err != nil {
		log.Printf("Dumper/DumpIO.GetSystems: %v\n", err)
		return
	}
	storage.SortSystems(vsys)
	storage.CleanIds(vsys)

	mt := lastModTime(vsys)

	d.mtx.RLock()
	done := !mt.After(d.modtime)
	d.mtx.RUnlock()

	if done {
		return
	}

	var buf bytes.Buffer

	dump := storage.Dump{Systems: vsys}
	err = json.NewEncoder(&buf).Encode(dump)
	if err != nil {
		log.Printf("Dumper/json.Encode: %v\n", err)
		return
	}

	p := buf.Bytes()

	var gzbuf bytes.Buffer
	gz := gzip.NewWriter(&gzbuf)
	_, err = buf.WriteTo(gz)
	gz.Close()
	if err != nil {
		log.Printf("Dumper/gzip.Write: %v\n", err)
		return
	}

	if d.Backup != nil {
		go func() {
			if err = d.Backup.Backup(mt, gzbuf.Bytes()); err != nil {
				log.Println(err)
			}
		}()
	}

	d.mtx.Lock()
	defer d.mtx.Unlock()

	d.modtime = mt
	d.p = p
	d.gzp = gzbuf.Bytes()
}

func lastModTime(vsys []*gmap.System) (t time.Time) {
	for _, sys := range vsys {
		if sys.Modified.After(t) {
			t = sys.Modified
		}
	}
	return
}
