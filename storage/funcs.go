package storage

import (
	"bytes"
	"encoding/json"
	"fmt"
	"golang.org/x/text/unicode/norm"
	"math"
	"sort"
	"strings"
	"time"

	"bitbucket.org/tajtiattila/harmless/fuzzy"
	"bitbucket.org/tajtiattila/harmless/gmap"
)

func StringToArray(s string) []string {
	v0 := strings.Split(s, ";")
	v := make([]string, 0, len(v0))
	for _, e := range v0 {
		if e != "" {
			v = append(v, e)
		}
	}
	return v
}

func ArrayToString(v []string) string {
	return strings.Join(v, ";")
}

func FindBestMatch(name string, v []*gmap.System) (*gmap.System, error) {
	i, err := fuzzy.Find(name, vsysDict(v))
	if err != nil {
		return nil, err
	}
	return v[i], nil
}

// SystemIdString generates an ID for a system based
// on its rounded coordinates and name. The Id is guaranteed
// to be unique if the ASCII-alphanumeric parts of the name
// is unique or the systems are at least 14 Ly apart.
func SystemIdString(sys *gmap.System) string {
	return coordid(sys.P) + "_" + nameid(sys.Name)
}

const b32bits = 5

func coordid(pos [3]float64) string {

	b32percoord := uint(0)
	for _, c := range pos {
		uc := ucoord(c)
		for 1<<(b32bits*b32percoord) <= uc {
			b32percoord++
		}
	}

	buf := make([]byte, 1+3*b32percoord)
	buf[0] = byte('0' + b32percoord)

	for i, c := range pos {
		base32(buf[1+i*int(b32percoord):], ucoord(c), b32percoord)
	}

	return string(buf)
}

func ucoord(c float64) uint {
	ic := int32(math.Floor(c/8 + 0.5))
	return uint((ic << 1) ^ (ic >> 31))
}

func base32(buf []byte, value uint, ndigit uint) {
	const base32s = "abcdefghijklmnopqrstuvwxyz234567"

	for i := uint(0); i < ndigit; i++ {
		chval := int(value >> (b32bits * (ndigit - i - 1)) & 0x1f)
		buf[i] = base32s[chval]
	}
}

func nameid(name string) string {
	rv := make([]byte, 0, len(name))
	uc := false

	var iter norm.Iter
	iter.InitString(norm.NFD, name)

	for !iter.Done() {
		v := iter.Next()
		r := v[0]
		var rx byte
		switch {
		case ('a' <= r && r <= 'z') || ('0' <= r && r <= '9'):
			rx = r
		case 'A' <= r && r <= 'Z':
			rx = r + 'a' - 'A'
		case r == '\'':
			// no op
		default:
			uc = true
		}
		if rx != 0 {
			if uc {
				rv, uc = append(rv, '_'), false
			}
			rv = append(rv, rx)
		}
	}

	return string(rv)
}

// SortSystems sorts systems by Name,
// and calls SortStations for all Systems with Stations.
func SortSystems(v []*gmap.System) {
	sort.Sort(sortsys(v))
	for _, sys := range v {
		SortSystem(sys)
	}
}

// SortSystem sorts system stations by Dist and Name.
func SortSystem(sys *gmap.System) {
	SortStations(sys.Stations)

	// clear inferred stations, if there are real ones
	hasinferred, hasreal := false, false
	for _, z := range sys.Stations {
		if z.Inferred {
			hasinferred = true
		} else {
			hasreal = true
		}
	}
	if hasinferred && hasreal {
		j := 0
		for i := range sys.Stations {
			sys.Stations[j] = sys.Stations[i]
			if !sys.Stations[i].Inferred {
				j++
			}
		}
		sys.Stations = sys.Stations[:j]
	}

	// remove detail if it has no relevant data
	if !keepdetail(sys.SystemDetail) {
		sys.SystemDetail = nil
	}

	// make timestamp UTC, truncate to millisecond
	sys.Modified = sys.Modified.UTC().Truncate(time.Millisecond)

	// fix coords to 1/32 Ly grid
	sys.P = sys.P.Vec3i().Vec3()
}

func keepdetail(sd *gmap.SystemDetail) bool {
	if sd == nil {
		return true
	}
	if sd.Pop > 0 || sd.NeedPermit {
		return true
	}
	if a := strings.ToLower(sd.Alleg); a != "" && a != "none" {
		return true
	}
	if g := strings.ToLower(sd.Govt); g != "" && g != "none" && g != "anarchy" {
		return true
	}
	if e := strings.ToLower(sd.Econ); e != "" && e != "none" {
		return true
	}
	return false
}

// SortStations sorts Stations by Dist and Name.
func SortStations(v []gmap.Station) {
	sort.Sort(sortstn(v))
}

type vsysDict []*gmap.System

func (d vsysDict) Len() int            { return len(d) }
func (d vsysDict) String(i int) string { return d[i].Name }

type sortsys []*gmap.System

func (v sortsys) Len() int      { return len(v) }
func (v sortsys) Swap(i, j int) { v[i], v[j] = v[j], v[i] }

func (v sortsys) Less(i, j int) bool {
	return strings.ToLower(v[i].Name) < strings.ToLower(v[j].Name)
}

type sortstn []gmap.Station

func (v sortstn) Len() int      { return len(v) }
func (v sortstn) Swap(i, j int) { v[i], v[j] = v[j], v[i] }

func (v sortstn) Less(i, j int) bool {
	// Sort stations by name only.
	// Distances might change, throwing the order off.
	return strings.ToLower(v[i].Name) < strings.ToLower(v[j].Name)
}

// CleanIds removes database IDs from v to prepare for export.
func CleanIds(v []*gmap.System) {
	for _, sys := range v {
		sys.Id = 0
		for i := range sys.Stations {
			sys.Stations[i].Id = 0
		}
	}
}

func SysEqual(pa, pb *gmap.System) bool {
	a, b := *pa, *pb
	SortSystem(&a)
	SortSystem(&b)

	b.Cmdr = a.Cmdr
	b.Modified = a.Modified
	b.Id = a.Id

	ap, err := json.Marshal(a)
	if err != nil {
		panic(fmt.Sprintf("storage.SysEqual: %v", err))
	}

	bp, err := json.Marshal(b)
	if err != nil {
		panic(fmt.Sprintf("storage.SysEqual: %v", err))
	}

	return bytes.Equal(ap, bp)
}
