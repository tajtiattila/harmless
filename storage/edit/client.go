package edit

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/tajtiattila/harmless/storage"
)

func Do(url, source string, e ...storage.Edit) error {
	req := Request{
		Source: source,
		Data:   e,
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return fmt.Errorf("edit.Do encode post: %v", err)
	}

	r, err := http.Post(url, "application/json", &buf)
	if err != nil {
		return fmt.Errorf("edit.Do post: %v", err)
	}
	defer r.Body.Close()

	var resp Response
	if err = json.NewDecoder(r.Body).Decode(&resp); err != nil {
		return fmt.Errorf("edit.Do decode response: %v", err)
	}

	if resp.Err != "" {
		return fmt.Errorf("%s", resp.Err)
	}

	return nil
}

type Request struct {
	Source string         `json:"source"`
	Data   []storage.Edit `json:"data"`
}
