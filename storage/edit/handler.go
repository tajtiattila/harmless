package edit

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"bitbucket.org/tajtiattila/harmless/gmap"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"bitbucket.org/tajtiattila/harmless/storage"
)

type Handler struct {
	db storage.Db
	w  io.Writer

	cb func(storage.EditStat) error
}

// NewHandler creates a new Handler that operates on db.
func NewHandler(db storage.Db) *Handler {
	return &Handler{db: db, w: ioutil.Discard}
}

// SetLog sets the Writer to store accepted edits.
func (h *Handler) SetLog(w io.Writer) {
	h.w = w
}

// SetCallback sets f to be called whenever a system is edited.
// The func will be called with the old and new system as arguments.
func (h *Handler) SetCallback(f func(storage.EditStat) error) {
	h.cb = f
}

// Load loads saved edits created using SetLog from r.
// Edits accepted from r will be written to the db.
// The log and callback set with SetLog and SetCallback
// will not be called.
func (h *Handler) Load(r io.Reader, errh func(*Entry, error) error) (Stat, error) {
	var s Stat
	j := json.NewDecoder(r)
	for {
		var ent Entry
		err := j.Decode(&ent)
		if err != nil {
			if err == io.EOF {
				return s, nil
			}
			return s, err
		}
		s.Ntotal++
		if _, err := h.db.Edit(ent.Data...); err != nil {
			if errh != nil {
				err = errh(&ent, err)
			}
			if err != nil {
				return s, err
			}
		} else {
			s.Nok++
		}
	}
	panic("not reached")
}

type Stat struct {
	Nok    int // number of successfully loaded entries
	Ntotal int // number of edits in log file
}

// ServeHTTP implements http.Handler.
func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("editapifunc", req).Enter()
	defer errh.Exit()

	var resp Response
	if req.Method == "POST" {
		es, err := h.process(req)
		if err != nil {
			errh.Err(err)
			resp.Err = err.Error()
		}
		resp.Success = es.Processed
	} else {
		var err error
		resp, err = h.getSystem(req)
		if err != nil {
			errh.Err(err)
			resp.Err = err.Error()
		}
	}

	errh.Err(ServeJson(w, req, resp))
}

func (h *Handler) getSystem(req *http.Request) (Response, error) {
	s, err := h.db.Find(req.FormValue("s"))
	if err != nil {
		return Response{}, err
	}
	return Response{Data: s}, nil
}

func (h *Handler) process(req *http.Request) (storage.EditStat, error) {
	var buf bytes.Buffer
	_, err := buf.ReadFrom(req.Body)
	if err != nil {
		return storage.EditStat{}, err
	}

	var ent Entry
	if err = json.Unmarshal(buf.Bytes(), &ent.Request); err != nil {
		var (
			p   []byte
			dst bytes.Buffer
		)
		if xerr := json.Indent(&dst, buf.Bytes(), "    ", "  "); xerr != nil {
			log.Printf("edit.Handler indent JSON: %s", xerr)
			p = buf.Bytes()
		} else {
			p = dst.Bytes()
		}
		log.Printf("edit.Handler decode JSON: %s", p)
		return storage.EditStat{}, err
	}

	t := time.Now().UTC()
	ent.RemoteAddr = RemoteAddr(req)
	ent.Modified = t

	for i := range ent.Data {
		d := &ent.Data[i]
		if d.Data != nil {
			d.Data.Modified = t
		}
	}

	if h.w != nil {
		data, err := json.MarshalIndent(ent, "", "  ")
		if err == nil {
			data = append(data, '\n')
			_, err = h.w.Write(data)
		}
		if err != nil {
			log.Printf("edit.Handler write log: %v", err)
		}
	}

	es, err := h.db.Edit(ent.Data...)
	if h.cb != nil {
		h.cb(es)
	}
	return es, err
}

// Response is the response sent back by Handler.
type Response struct {
	Data    *gmap.System `json:"data,omitempty"`
	Err     string       `json:"error,omitempty"`
	Success int          `json:"success,omitempty"` // number of edits applied
}

type Entry struct {
	Modified   time.Time `json:"time"`
	RemoteAddr string    `json:"remoteAddr"`
	Request
}
