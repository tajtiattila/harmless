package sqlite

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/tajtiattila/harmless/storage"
	sq "bitbucket.org/tajtiattila/harmless/structsql"
)

type DB struct {
	h *sq.DB // structsql db handle

	chack, chrel chan *sq.Tx
}

func NewDB(sdb *sql.DB) *DB {
	chack, chrel := make(chan *sq.Tx), make(chan *sq.Tx)
	go func() {
		var tx *sq.Tx
		for {
			if tx == nil {
				chack <- nil
				tx = <-chrel
			} else {
				select {
				case chack <- tx:
					tx = <-chrel
				case <-time.After(10 * time.Second):
					if err := tx.Commit(); err != nil {
						log.Println("storage/sqlite error releasing tx:v", err)
					}
					tx = nil
				}
			}
		}
	}()
	return &DB{sq.NewDB(sdb, sq.QuestionMark), chack, chrel}
}

func Open(dbfn string) (*DB, error) {
	db, err := openDB(dbfn)
	if err != nil {
		return nil, err
	}

	var n int64
	if err = db.QueryRow("SELECT COUNT(*) FROM System;").Scan(&n); err != nil {
		return nil, fmt.Errorf("storage: Open can't check System count")
	}

	if n == 0 {
		i := strings.LastIndex(dbfn, ".")
		if i == -1 {
			i = len(dbfn)
		}
		impfn := dbfn[:i] + ".json"
		f, err := os.Open(impfn)
		if err != nil {
			return nil, fmt.Errorf("storage: database empty and import file is missing")
		}
		defer f.Close()

		ndb := NewDB(db)
		fmt.Fprintf(os.Stderr, "Importing %q into %q\n", impfn, dbfn)
		if err = storage.Import(f, ndb); err != nil {
			return nil, err
		}
		return ndb, nil
	}

	return NewDB(db), nil
}

func (db *DB) Close() error {
	return db.h.Close()
}

func (db *DB) autotx() (a *autotx, err error) {
	tx := <-db.chack
	if tx == nil {
		tx, err = db.h.Begin()
	}
	return &autotx{db, tx}, err
}

type autotx struct {
	db *DB
	tx *sq.Tx
}

func (a *autotx) Release() {
	a.db.chrel <- a.tx
}

func (a *autotx) Commit() error {
	err := a.tx.Commit()
	a.db.chrel <- nil
	return err
}

func (a *autotx) Rollback() error {
	err := a.tx.Rollback()
	a.db.chrel <- nil
	return err
}

type sqliteDriver struct{}

func (*sqliteDriver) Open(dsn string) (storage.Db, error) {
	return Open(dsn)
}

func init() {
	storage.RegisterDriver("sqlite", &sqliteDriver{})
}
