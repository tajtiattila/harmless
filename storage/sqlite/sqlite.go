package sqlite

import (
	"database/sql"
	"github.com/mattn/go-sqlite3"
	"strings"
	"unicode"
)

func openDB(dbfn string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", dbfn)
	if err != nil {
		return nil, err
	}

	// foreign_keys must be enabled for each connection
	_, err = db.Exec("PRAGMA foreign_keys=on;")
	if err != nil {
		return nil, err
	}

	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Commit()

	rows, err := tx.Query(`SELECT name FROM sqlite_master WHERE type='table';`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tabs := make(map[string]bool)
	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			return nil, err
		}
		tabs[strings.ToLower(name)] = true
	}
	rows.Close()

	// initialize missing tables
	for _, e := range vinitsql {
		if len(tabs) == 0 || (e.name != "" && !tabs[strings.ToLower(e.name)]) {
			_, err = tx.Exec(e.sql)
			if err != nil {
				return nil, err
			}
		}
	}

	return db, nil
}

var vinitsql []sqlinit

type sqlinit struct {
	name string
	sql  string
}

func init() {
	sdata := initsql

	_, ver, _ := sqlite3.Version()
	if ver < 3008002 {
		sdata = strings.Replace(sdata, "WITHOUT ROWID", "", -1)
	}

	stmts := strings.SplitAfter(sdata, ";\n")
	for i := range stmts {
		stmts[i] = strings.TrimSpace(stmts[i])
	}

	for _, stmt := range stmts {
		words := strings.FieldsFunc(stmt, func(r rune) bool {
			return unicode.IsSpace(r) || r == '('
		})
		if len(words) > 3 && words[0] == "CREATE" && words[1] == "TABLE" {
			vinitsql = append(vinitsql, sqlinit{words[2], stmt})
		} else {
			vinitsql = append(vinitsql, sqlinit{"", stmt})
		}
	}
}

var initsql = `

CREATE TABLE Added
 (
   added_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE(name)
 );

CREATE TABLE System
 (
   system_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   pos_x DOUBLE NOT NULL,
   pos_y DOUBLE NOT NULL,
   pos_z DOUBLE NOT NULL,
   added_id INTEGER,

   cmdr VARCHAR(40) COLLATE nocase NOT NULL,
   modified DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,

   valid BOOLEAN,

   UNIQUE(name),
   UNIQUE(pos_x, pos_y, pos_z),

   FOREIGN KEY (added_id) REFERENCES Added(added_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE VIRTUAL TABLE System_index USING rtree(system_id, x0, x1, y0, y1, z0, z1);

CREATE TABLE SystemDetail
 (
   system_id INTEGER NOT NULL,
   allegiance VARCHAR(40) NOT NULL,
   government VARCHAR(40) NOT NULL,
   economy VARCHAR(40) NOT NULL,
   population DOUBLE NOT NULL,
   requires_permit BOOLEAN DEFAULT FALSE NOT NULL,

   UNIQUE (system_id),

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE TABLE Station
 (
   station_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   system_id INTEGER NOT NULL,
   ls_from_star DOUBLE,

   inferred BOOLEAN DEFAULT FALSE NOT NULL,

   type VARCHAR(40) NOT NULL,
   faction VARCHAR(40) NOT NULL,
   government VARCHAR(40) NOT NULL,
   allegiance VARCHAR(40) NOT NULL,
   economies VARCHAR(100) NOT NULL,

   shipyard BOOLEAN,
   outfitting BOOLEAN,
   commodities_market BOOLEAN,
   black_market BOOLEAN,

   UNIQUE (system_id, name),

   FOREIGN KEY (system_id) REFERENCES System(system_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );

CREATE TABLE Category
 (
   category_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,

   UNIQUE (name)
 );

CREATE TABLE Item
 (
   item_id INTEGER PRIMARY KEY,
   name VARCHAR(40) COLLATE nocase,
   category_id INTEGER NOT NULL,

   UNIQUE (category_id, name),

   FOREIGN KEY (category_id) REFERENCES Category(category_id)
   	ON UPDATE CASCADE
   	ON DELETE CASCADE
 );


-- Some items have two versions of their name.
CREATE TABLE AltItemNames
 (
   alt_name VARCHAR(40) NOT NULL COLLATE nocase,
   item_id INTEGER NOT NULL,

   UNIQUE (alt_name),
   PRIMARY KEY (alt_name, item_id)
 );

CREATE TABLE Price
 (
   item_id INTEGER NOT NULL,
   station_id INTEGER NOT NULL,
   ui_order INTEGER NOT NULL DEFAULT 0,
   -- how many credits will the station pay for this item?
   sell_to INTEGER NOT NULL,
   -- how many credits must you pay to buy at this station?
   buy_from INTEGER NOT NULL DEFAULT 0,
   modified DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
   demand INTEGER DEFAULT -1,
   demand_level INTEGER DEFAULT -1,
   stock INTEGER DEFAULT -1,
   stock_level INTEGER DEFAULT -1,

   PRIMARY KEY (item_id, station_id),

   FOREIGN KEY (item_id) REFERENCES Item(item_id)
   	ON UPDATE CASCADE
      ON DELETE CASCADE,
   FOREIGN KEY (station_id) REFERENCES Station(station_id)
   	ON UPDATE CASCADE
      ON DELETE CASCADE
 ) WITHOUT ROWID
;
`
