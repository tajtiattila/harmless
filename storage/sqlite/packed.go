package sqlite

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/storage"
	"fmt"
)

func (db *DB) PackSystems() ([]gmap.PackSys, error) {
	return packSysQuery(db.h)
}

// At returns the systems at the positions, with dumb system pointers
// and the error message as Name upon errors.
func (db *DB) At(positions []gmap.Vec3i) []*gmap.System {
	v, _ := db.AtFunc(positions, func(p gmap.Vec3i, err error) (*gmap.System, error) {
		return &gmap.System{
			P:    p.Vec3(),
			Name: "ERROR " + err.Error(),
		}, nil
	})
	return v
}

// AtFunc returns the systems at the positions,
// calling errh to get a system for each error encountered.
func (db *DB) AtFunc(positions []gmap.Vec3i, errh storage.AtErrh) ([]*gmap.System, error) {
	v := make([]*gmap.System, len(positions))

	if errh == nil {
		errh = func(p gmap.Vec3i, err error) (*gmap.System, error) { return nil, err }
	}

	// TODO: break up query if there are too many positions
	// sqlite has a default limit 999 parameters, so this will work
	// for at most 333 systems.
	var v0 []*gmap.System
	a, err := db.autotx()
	if err == nil {
		defer a.Release()
		v0, err = Query(a.tx, posQuery(positions))
	}
	if err != nil {
		for i, p := range positions {
			var xerr error
			v[i], xerr = errh(p, err)
			if xerr != nil {
				return nil, xerr
			}
		}
		return v, nil
	}
	m := make(map[gmap.Vec3i]*gmap.System)
	for _, sys := range v0 {
		m[sys.P.Vec3i()] = sys
	}
	for i, p := range positions {
		sys := m[p]
		if sys == nil {
			sys, err = errh(p, fmt.Errorf("No system at %v", p))
			if err != nil {
				return nil, err
			}
		}
		v[i] = sys
	}
	return v, nil
}
