package sqlite

import (
	"database/sql"
	"fmt"
	"log"

	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/storage"
	sq "bitbucket.org/tajtiattila/harmless/structsql"
)

// get all systems
func (db *DB) GetSystems() ([]*gmap.System, error) {
	a, err := db.autotx()
	if err != nil {
		return nil, err
	}
	defer a.Release()
	return Query(a.tx, nil)
}

// Search for systems within a sphere
func (db *DB) Sphere(around gmap.Vec3, radius float64) ([]*gmap.System, error) {
	a, err := db.autotx()
	if err != nil {
		return nil, err
	}
	defer a.Release()
	return Query(a.tx, &sphereQuery{around, radius})
}

// Search for system(s) by (part of) name
func (db *DB) Search(name string) ([]*gmap.System, error) {
	a, err := db.autotx()
	if err != nil {
		return nil, err
	}
	defer a.Release()
	one, err := Query(a.tx, &nameQuery{name, true})
	if err != nil {
		return nil, err
	}
	if len(name) <= 3 || len(one) != 0 {
		return one, nil
	}
	v, err := Query(a.tx, &nameQuery{name, false})
	if len(v) == nameQuerySearchLimit {
		// There are too many matches. Don't return anything, because
		// results may be irrelevant if the search is location based.
		v = nil
	}
	return v, err
}

func (db *DB) Namep(name string) (p gmap.Vec3, err error) {
	a, err := db.autotx()
	if err != nil {
		return p, err
	}
	defer a.Release()
	var x, y, z float64
	err = a.tx.Tx.QueryRow(
		`SELECT pos_x,pos_y,pos_z FROM System WHERE name=?`,
		name).Scan(&x, &y, &z)
	if err != nil {
		if err == sql.ErrNoRows {
			err = fmt.Errorf("storage: name %q not found", name)
		}
	} else {
		p = gmap.Vec3{x, y, z}
	}
	return
}

// Find an exact system by (part of) its name.
func (db *DB) Find(name string) (*gmap.System, error) {
	v, err := db.Search(name)
	if err != nil {
		return nil, err
	}
	if len(v) == 0 {
		return nil, fmt.Errorf("storage: name %q not found", name)
	}
	return storage.FindBestMatch(name, v)
}

// Edit (add/modify/remove) systems
func (db *DB) Edit(ve ...storage.Edit) (es storage.EditStat, err error) {
	var a *autotx
	a, err = db.autotx()
	if err != nil {
		return
	}

	var fatal bool
	defer func() {
		var xerr error
		if fatal {
			if xerr = a.Rollback(); xerr != nil {
				log.Printf("storage.Edit Rollback: %v\n", xerr)
			}
		} else {
			if xerr = a.Commit(); xerr != nil {
				log.Printf("storage.Edit Commit: %v\n", xerr)
			}
		}
		// report Commit/Rollback error if no other error occurred
		if err == nil {
			err = xerr
		}
	}()

	for _, e := range ve {
		stat, xerr := edit(a.tx, e)
		if xerr != nil {
			//log.Println(e.Key, xerr)
		}
		if fatal = NeedsRollback(xerr); fatal {
			return storage.EditStat{}, xerr
		}
		if err == nil {
			err = xerr
		}
		if xerr == nil {
			es.Processed++
			es.Flag |= stat.Flag
		}
	}

	return
}

func edit(tx *sq.Tx, e storage.Edit) (stat storage.EditStat, err error) {
	var oldsys *gmap.System
	if e.Op != storage.Add {
		if e.Op == storage.Update {
			v, err := Query(tx, posQuery([]gmap.Vec3i{e.Key.Vec3i()}))
			if err != nil {
				return stat, storeErr(err, "storage.Edit %v position invalid", e.Key)
			}
			if len(v) != 1 {
				return stat, storeErr(nil, "storage.Edit %v internal: got %v rows, not 1", e.Key, len(v))
			}
			oldsys = v[0]
		} else {
			oldsys = &gmap.System{P: e.Key}
			err = tx.Tx.QueryRow(`SELECT System.system_id, System.name
					FROM System
					INNER JOIN System_index USING (System_id)
						WHERE x0=? AND y0=? AND z0=?`,
				e.Key[0], e.Key[1], e.Key[2]).Scan(&oldsys.Id, &oldsys.Name)
			if err != nil {
				return stat, storeErr(err, "storage.Edit position %v invalid", e.Key)
			}
		}
		if oldsys.Id <= 0 {
			return stat, storeErr(nil, "storage.Edit %v internal: sysid invalid", e.Key)
		}
	}

	sys := e.Data
	if e.Op != storage.Delete && sys == nil {
		return stat, storeErr(nil, "storage.Edit %v entry has no data", e.Key)
	}

	switch e.Op {
	case storage.Move:
		err = updateOne(tx.Tx,
			`UPDATE System
				SET pos_x=?,pos_y=?,pos_z=?,cmdr=?,modified=?,valid='true'
				WHERE system_id=?`,
			sys.P[0], sys.P[1], sys.P[2], sys.Cmdr, sys.Modified, oldsys.Id)
		if err != nil {
			err = storeErr(err, "storage.Move %v", e.Key)
		} else {
			err = updatePosition(tx, oldsys.Id)
			if err == nil {
				stat.Flag |= storage.SysMove
			}
		}
	case storage.Rename:
		if sys.Name == "" {
			return stat, storeErr(nil, "storage.Edit %v rename to empty string", e.Key)
		}
		if sys.Name == oldsys.Name {
			return stat, storeErr(nil, "storage.Edit %v rename to same name (%q)", e.Key, sys.Name)
		}
		err = updateOne(tx.Tx,
			`UPDATE System
				SET name=?,cmdr=?,modified=?,valid='true'
				WHERE system_id=?`,
			sys.Name, sys.Cmdr, sys.Modified, oldsys.Id)
		if err != nil {
			return stat, storeErr(err, "storage.Rename %v", e.Key)
		}
		stat.Flag |= storage.SysRename
	case storage.Delete:
		err = updateOne(tx.Tx,
			`DELETE FROM System WHERE system_id=?`,
			oldsys.Id)
		if err != nil {
			return stat, storeErr(err, "storage.Delete %v", e.Key)
		}
		err = updateOne(tx.Tx,
			`DELETE FROM System_index WHERE system_id=?`,
			oldsys.Id)
		if err != nil {
			return stat, consistencyErr(err, "storage.Delete %v from System_index", e.Key)
		}
		stat.Flag |= storage.SysDelete
	case storage.Update:
		sys.Id = oldsys.Id
		if storage.SysEqual(sys, oldsys) {
			return stat, storeErr(err, "storage.Update no changes for %q", oldsys.Name)
		}
		storage.SortSystem(sys)
		if err = putSystem(tx, sys); err != nil {
			return
		}
		if sys.Name != oldsys.Name {
			stat.Flag |= storage.SysRename
		}
		if sys.P != oldsys.P {
			stat.Flag |= storage.SysMove
		}
		if (len(sys.Stations) == 0) != (len(oldsys.Stations) == 0) {
			stat.Flag |= storage.SysHasStationChange
		}
	case storage.Add:
		storage.SortSystem(sys)
		if err = putSystem(tx, sys); err != nil {
			return
		}
		stat.Flag |= storage.SysAdd
	default:
		return stat, storeErr(nil, "storage.Edit: op invalid")
	}
	return
}

func updateOne(tx *sql.Tx, query string, args ...interface{}) error {
	res, err := tx.Exec(query, args...)
	if err != nil {
		return err
	}
	n, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return consistencyErr(nil, "storage.Edit RowsAffacted=%d should be 1", n)
	}
	return nil
}

func (db *DB) PutSystems(v []*gmap.System) (xerr error) {
	a, err := db.autotx()
	if err != nil {
		return err
	}
	defer func() {
		if xerr == nil {
			a.Commit()
		} else {
			a.Rollback()
		}
	}()

	for _, sys := range v {
		if err = putSystem(a.tx, sys); err != nil {
			return err
		}
	}

	return nil
}

type StoreErr struct {
	pfx string // string prefix
	err error  // actual error

	rollback bool // needs rollback
}

func (e *StoreErr) Error() string {
	if e.err == nil {
		return e.pfx
	}
	return e.pfx + ": " + e.err.Error()
}

func newStoreErr(rb bool, err error, msg string, arg ...interface{}) error {
	if err != nil {
		if _, ok := err.(*StoreErr); ok {
			return err
		}
	}
	return &StoreErr{fmt.Sprintf(msg, arg...), err, rb}
}

func storeErr(err error, msg string, arg ...interface{}) error {
	return newStoreErr(false, err, msg, arg...)
}

func consistencyErr(err error, msg string, arg ...interface{}) error {
	return newStoreErr(true, err, msg, arg...)
}

func NeedsRollback(err error) bool {
	if e, ok := err.(*StoreErr); ok {
		return e.rollback
	}
	return false
}

func putSystem(tx *sq.Tx, sys *gmap.System) (err error) {
	var dsys DbSystem
	dsys.SystemId = sys.Id
	dsys.Name = sys.Name
	dsys.PosX = sys.P[0]
	dsys.PosY = sys.P[1]
	dsys.PosZ = sys.P[2]
	dsys.Cmdr = sys.Cmdr
	dsys.Modified = sys.Modified.UTC()
	dsys.Valid = sys.Valid

	if sys.Id != 0 {
		var n int64
		n, err = tx.Update(&dsys).Add("WHERE system_id=?", sys.Id).RunNum()
		if err != nil {
			return storeErr(err, "storage cannot update System %q#%v", sys.Name, sys.Id)
		}
		if n != 1 {
			return storeErr(nil, "storage: RowsAffected=%v in DB.Update", n)
		}
	} else {
		sys.Id, err = tx.Insert(&dsys).RunId()
		if err != nil {
			return storeErr(err, "storage: cannot insert System %q", sys.Name)
		}
	}
	if err = updatePosition(tx, sys.Id); err != nil {
		return err
	}

	if err = tx.Delete("SystemDetail").Add("WHERE system_id=?", sys.Id).Run(); err != nil {
		return consistencyErr(err, "storage: cannot insert SystemDetail for %q#%v", sys.Name, sys.Id)
	}
	if sys.SystemDetail != nil {
		dsd := DbSystemDetail{
			SystemId:       sys.Id,
			Allegiance:     sys.Alleg,
			Government:     sys.Govt,
			Economy:        sys.Econ,
			Population:     sys.Pop,
			RequiresPermit: sys.NeedPermit,
		}
		if err = tx.Insert(&dsd).Run(); err != nil {
			return consistencyErr(err, "storage: cannot insert SystemDetail for %q#%v", sys.Name, sys.Id)
		}
	}

	if err = tx.Delete("Station").Add("WHERE system_id=?", sys.Id).Run(); err != nil {
		return consistencyErr(err, "storage: cannot delete old Stations for %q#%v", sys.Name, sys.Id)
	}

	for i := range sys.Stations {
		stn := sys.Stations[i]
		dstn := DbStation{
			SystemId:   sys.Id,
			Name:       stn.Name,
			LsFromStar: stn.Dist,

			Inferred:   stn.Inferred,
			Type:       stn.Type,
			Faction:    stn.Faction,
			Government: stn.Govt,
			Allegiance: stn.Alleg,
			Economies:  storage.ArrayToString(stn.Econ),

			Shipyard:          stn.Shipyard,
			Outfitting:        stn.Outfit,
			CommoditiesMarket: stn.Cmarket,
			BlackMarket:       stn.Bmarket,
		}
		if stn.Id, err = tx.Insert(&dstn).RunId(); err != nil {
			return consistencyErr(err,
				"storage: cannot insert Station %q for %q#%v",
				stn.Name, sys.Name, sys.Id)
		}
	}
	return nil
}

func (db *DB) Stat() (stat storage.Stat, err error) {
	r := db.h.DB.QueryRow(`SELECT COUNT(*),MAX(Modified) FROM System`)
	err = r.Scan(stat.NumSys, stat.ModTime)
	return
}
