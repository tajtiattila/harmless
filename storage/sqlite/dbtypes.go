package sqlite

import (
	sq "bitbucket.org/tajtiattila/harmless/structsql"
	"time"
)

// System as kept in DB
type DbSystem struct {
	SystemId int64
	Name     string

	PosX, PosY, PosZ float64

	Cmdr     string
	Modified time.Time

	Valid *bool
}

// SystemDetail as kept in DB
type DbSystemDetail struct {
	SystemId   int64
	Allegiance string
	Government string
	Economy    string
	Population float64

	RequiresPermit bool
}

// Station as kept in DB
type DbStation struct {
	StationId  int64
	Name       string
	SystemId   int64
	LsFromStar float64

	Inferred   bool
	Type       string
	Faction    string
	Government string
	Allegiance string
	Economies  string

	Shipyard          *bool
	Outfitting        *bool
	CommoditiesMarket *bool
	BlackMarket       *bool
}

func init() {
	sq.NewTable("System").AutoPriKey("system_id").Struct(&DbSystem{})
	sq.NewTable("SystemDetail").Struct(&DbSystemDetail{})
	sq.NewTable("Station").AutoPriKey("station_id").Struct(&DbStation{})
}
