package sqlite

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	sq "bitbucket.org/tajtiattila/harmless/structsql"
	"database/sql"
	"fmt"
)

type sysRows struct {
	dsys DbSystem
	dsd  DbSystemDetail
	dstn DbStation
}

func Query(i sq.Interface, x xquery) ([]*gmap.System, error) {
	var r sysRows
	q := i.Select(&r.dsys)
	if x != nil {
		x.join(q)
	}
	q.Join("LEFT", &r.dsd, "ON System.system_id = SystemDetail.system_id").
		Join("LEFT", &r.dstn, "ON System.system_id = Station.system_id")
	if x != nil {
		x.where(q)
	}
	var v []*gmap.System
	rows := q.Rows()
	defer rows.Close()
	for rows.Scan() {
		if len(v) == 0 || v[len(v)-1].Id != r.dsys.SystemId {
			sys := new(gmap.System)
			r.dsys.Decode(sys)
			r.dsd.DecodeTo(sys)
			r.dstn.DecodeTo(sys)
			v = append(v, sys)
		} else {
			r.dstn.DecodeTo(v[len(v)-1])
		}
	}
	return v, rows.Err()
}

type xquery interface {
	join(q *sq.Query)
	where(q *sq.Query)
}

type sphereQuery struct {
	c gmap.Vec3
	r float64
}

func (s *sphereQuery) join(q *sq.Query) {
	q.Join("INNER", "System_index", "USING (system_id)")
}

func (s *sphereQuery) where(q *sq.Query) {
	q.Add(`WHERE
		(x0-?)*(x0-?)+(y0-?)*(y0-?)+(z0-?)*(z0-?) <= ?
		AND ?<=x0 AND x1<=?
		AND ?<=y0 AND y1<=?
		AND ?<=z0 AND z1<=?`,
		s.c[0], s.c[0],
		s.c[1], s.c[1],
		s.c[2], s.c[2],
		s.r*s.r,
		s.c[0]-s.r, s.c[0]+s.r,
		s.c[1]-s.r, s.c[1]+s.r,
		s.c[2]-s.r, s.c[2]+s.r,
	)
}

const nameQuerySearchLimit = 20

type nameQuery struct {
	name  string
	exact bool
}

func (*nameQuery) join(q *sq.Query) {}

func (n *nameQuery) where(q *sq.Query) {
	if n.exact {
		q.Add("WHERE System.name=?", n.name)
	} else {
		q.Add("WHERE System.name LIKE ? LIMIT ?", "%"+n.name+"%", nameQuerySearchLimit)
	}
}

func updatePosition(tx *sq.Tx, id int64) error {
	if _, err := tx.Tx.Exec(`INSERT OR REPLACE INTO System_index(system_id,x0,x1,y0,y1,z0,z1)
		SELECT system_id,pos_x,pos_x,pos_y,pos_y,pos_z,pos_z FROM System
		WHERE system_id=?`, id); err != nil {
		return consistencyErr(err, "storage cannot update System_index for %v", id)
	}
	return nil
}

type posQuery []gmap.Vec3i

func (s posQuery) join(q *sq.Query) {
	q.Join("INNER", "System_index", "USING (system_id)")
}

func (s posQuery) where(q *sq.Query) {
	pfx := `WHERE `
	for _, p := range s {
		v := p.Vec3()
		q.Add(pfx+`(x0=? AND y0=? AND z0=?)`, v[0], v[1], v[2])
		pfx = ` OR `
	}
}

func packSysQuery(db sq.Queryer) ([]gmap.PackSys, error) {
	r, err := db.Query(`SELECT
		System.system_id, pos_x, pos_y, pos_z, requires_permit, ls_from_star FROM System
		LEFT JOIN SystemDetail ON SystemDetail.system_id = System.system_id
		LEFT JOIN Station ON Station.system_id = System.system_id;`)
	if err != nil {
		return nil, fmt.Errorf("storage: cannot query packed systems: %v", err)
	}
	defer r.Close()
	var (
		v []gmap.PackSys

		id, lastid int64

		pos     gmap.Vec3
		reqperm sql.NullBool
		stndist sql.NullFloat64
	)
	for r.Next() {
		if err := r.Scan(&id, &pos[0], &pos[1], &pos[2], &reqperm, &stndist); err != nil {
			return nil, fmt.Errorf("storage: error scanning packed system: %v", err)
		}
		if v == nil || id != lastid {
			psys := gmap.PackSys{P: pos.Vec3i()}
			if reqperm.Valid && reqperm.Bool {
				psys.Flag |= gmap.RequiresPermit
			}
			v = append(v, psys)
		}
		if stndist.Valid {
			mag := gmap.StnDistMag(stndist.Float64)
			psys := &v[len(v)-1]
			if psys.StnDistMag > mag {
				psys.StnDistMag = mag
			}
		}
	}
	return v, r.Err()
}
