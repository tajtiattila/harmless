package sqlite

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/storage"
)

func (s *DbSystem) Decode(sys *gmap.System) {
	sys.Id = s.SystemId
	sys.Name = s.Name
	sys.P = gmap.Vec3{s.PosX, s.PosY, s.PosZ}
	sys.SystemDetail = nil
	sys.Stations = sys.Stations[:0]
	sys.Cmdr = s.Cmdr
	sys.Modified = s.Modified
	sys.Valid = s.Valid
}

func (s *DbSystemDetail) DecodeTo(sys *gmap.System) {
	if s.SystemId == 0 {
		sys.SystemDetail = nil
	} else {
		sys.SystemDetail = &gmap.SystemDetail{
			Pop:   s.Population,
			Alleg: s.Allegiance,
			Econ:  s.Economy,
			Govt:  s.Government,

			NeedPermit: s.RequiresPermit,
		}
	}
}

func (s *DbStation) DecodeTo(sys *gmap.System) {
	if s.StationId == 0 {
		return
	}
	sys.Stations = append(sys.Stations, gmap.Station{
		System: sys,
		Id:     s.StationId,
		Name:   s.Name,
		Dist:   s.LsFromStar,

		StationDetail: gmap.StationDetail{
			Inferred: s.Inferred,

			Type: s.Type,

			Faction: s.Faction,
			Govt:    s.Government,
			Alleg:   s.Allegiance,
			Econ:    storage.StringToArray(s.Economies),

			Shipyard: s.Shipyard,
			Outfit:   s.Outfitting,
			Cmarket:  s.CommoditiesMarket,
			Bmarket:  s.BlackMarket,
		},
	})
}
