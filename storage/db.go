package storage

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

type Db interface {
	// Search for systems within a sphere
	Sphere(around gmap.Vec3, radius float64) ([]*gmap.System, error)

	// Namep return the coordinates for a system name.
	Namep(name string) (gmap.Vec3, error)

	// Find an exact system by (part of) its name or coordinates.
	Find(name string) (*gmap.System, error)

	// Search for system(s) by (part of) name
	Search(name string) ([]*gmap.System, error)

	PackSystems() ([]gmap.PackSys, error)

	// At returns the systems at the positions, with dumb system pointers
	// and the error message as Name upon errors.
	At(positions []gmap.Vec3i) []*gmap.System

	// AtFunc returns the systems at the positions,
	// calling errh to get a system for each error encountered.
	// If errh returns an error, it will be returned by AtFunc.
	// If errh is nil, the first error encountered will be returned.
	AtFunc(positions []gmap.Vec3i, errh AtErrh) ([]*gmap.System, error)

	// Edit changes systems.
	Edit(e ...Edit) (EditStat, error)

	// get db statistics
	Stat() (Stat, error)

	Close() error
}

type EditStat struct {
	// Processed reports number of stored edits.
	Processed int

	// Flag reports what operations happened during the edit.
	Flag EditFlag
}

type EditFlag uint

const (
	// a system has been added
	SysAdd EditFlag = 1 << iota

	// a system has been removed
	SysDelete

	// system position has changed
	SysMove

	// system name has changed
	SysRename

	// earlier empty system has station added, or system has all stations removed
	SysHasStationChange
)

// Stat represents Db statistics.
type Stat struct {
	NumSys  int
	ModTime time.Time
}

type AtErrh func(p gmap.Vec3i, err error) (*gmap.System, error)

type Driver interface {
	Open(dsn string) (Db, error)
}

func RegisterDriver(name string, d Driver) {
	drvmtx.Lock()
	defer drvmtx.Unlock()
	if _, ok := drvmap[name]; ok {
		panic(fmt.Errorf("driver %q is already registered", name))
	}
	drvmap[name] = d
}

func Open(driver, dsn string) (Db, error) {
	drvmtx.RLock()
	defer drvmtx.RUnlock()
	d := drvmap[driver]
	if d == nil {
		return nil, fmt.Errorf("driver %q is not available (forgot import?)", driver)
	}
	return d.Open(dsn)
}

var (
	drvmtx sync.RWMutex
	drvmap = make(map[string]Driver)
)
