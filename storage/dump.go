package storage

import (
	"encoding/json"
	"io"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

type DumpIO interface {
	GetSystems() ([]*gmap.System, error)
	PutSystems([]*gmap.System) error
}

type Dump struct {
	Systems []*gmap.System `json:"systems,omitempty"`
}

func Import(r io.Reader, db DumpIO) error {
	var dump Dump
	if err := json.NewDecoder(r).Decode(&dump); err != nil {
		return err
	}
	return db.PutSystems(dump.Systems)
}

func Export(db DumpIO, w io.Writer) error {
	vsys, err := db.GetSystems()
	if err != nil {
		return err
	}
	SortSystems(vsys)
	CleanIds(vsys)
	_, err = w.Write([]byte("{\n \"systems\": [\n  "))
	if err != nil {
		return err
	}
	var sep []byte
	for _, sys := range vsys {
		p, err := json.MarshalIndent(sys, "  ", " ")
		if err != nil {
			return err
		}
		if sep != nil {
			_, err = w.Write(sep)
			if err != nil {
				return err
			}
		} else {
			sep = []byte(",\n  ")
		}
		_, err = w.Write(p)
		if err != nil {
			return err
		}
	}
	_, err = w.Write([]byte("\n ]\n}\n"))
	return err
}
