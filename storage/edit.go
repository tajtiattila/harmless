package storage

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

type Op uint

const (
	Invalid Op = iota
	Update
	Add
	Move
	Rename
	Delete
)

func ParseOp(s string) Op {
	switch s {
	case "update":
		return Update
	case "add":
		return Add
	case "move":
		return Move
	case "rename":
		return Rename
	case "delete":
		return Delete
	}
	return Invalid
}

func (o Op) String() string {
	switch o {
	case Update:
		return "update"
	case Add:
		return "add"
	case Move:
		return "move"
	case Rename:
		return "rename"
	case Delete:
		return "delete"
	}
	return ""
}

func (o Op) MarshalJSON() ([]byte, error) {
	return json.Marshal(o.String())
}

func (o *Op) UnmarshalJSON(p []byte) error {
	var s string
	if err := json.Unmarshal(p, &s); err != nil {
		return err
	}
	*o = ParseOp(s)
	if *o == Invalid {
		return fmt.Errorf("storage.Op %q invalid", s)
	}
	return nil
}

type Edit struct {
	Op Op `json:"op"`

	// Key is the position of the system to be edited.
	Key gmap.Vec3 `json:"key"`

	// Data contains new system data. It may be nil
	// only for Delete. Only fields specific
	// to the Op in question will be used:
	//
	// Add, Update: all fields
	//
	// Move: P, Cmdr, Modified
	//
	// Rename: Name, Cmdr, Modified
	//
	// Delete: no fields
	Data *gmap.System `json:"data"`
}

// Author represents an Edit author for Db.
type Author struct {
	Cmdr string
	Time time.Time
}

func NewUpdater() *Author {
	return &Author{"Jameson", time.Now().UTC()}
}

func (a *Author) Add(s0 *gmap.System) *Edit {
	sys := *s0
	sys.Cmdr = a.Cmdr
	sys.Modified = a.Time
	return &Edit{Add, sys.P, &sys}
}

func (a *Author) Update(s0 *gmap.System) *Edit {
	sys := *s0
	sys.Cmdr = a.Cmdr
	sys.Modified = a.Time
	return &Edit{Update, sys.P, &sys}
}

func (a *Author) Move(p, q gmap.Vec3) *Edit {
	var sys gmap.System
	sys.P = q
	sys.Cmdr = a.Cmdr
	sys.Modified = a.Time
	return &Edit{Move, p, &sys}
}

func (a *Author) Rename(p gmap.Vec3, n string) *Edit {
	var sys gmap.System
	sys.P = p
	sys.Name = n
	sys.Cmdr = a.Cmdr
	sys.Modified = a.Time
	return &Edit{Rename, p, &sys}
}

func (a *Author) Delete(p gmap.Vec3) *Edit {
	return &Edit{Delete, p, nil}
}
