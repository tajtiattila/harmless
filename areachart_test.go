package main

import (
	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/area/gen"
	"bitbucket.org/tajtiattila/harmless/db"
	"os"
	"testing"
)

func perr(err error) {
	if err != nil {
		panic(err)
	}
}

func BenchmarkGenerateChart(b *testing.B) {
	b.StopTimer()
	dbio, err := db.Open("dir", "data")
	perr(err)
	defer dbio.Close()

	var data db.Data
	perr(data.Load(dbio))

	perr(os.Chdir("res"))

	fm, err := gen.LoadDefaultFontMap("font")
	perr(err)

	stor := area.NewNoStore(gen.NewAreaMapper(data.Chart, area.XZPlane, DefaultRadius, fm))

	jumprange := float64(9)
	radius := float64(25)

	clstrc.Map(jumprange) // generate cluster map in advance too

	b.StartTimer()

	for _, s := range []string{"eranin", "sol", "wyrd", "arcturus"} {
		GenerateChart(stor, data.IdSystem(s), radius, jumprange, -1)
	}
}
