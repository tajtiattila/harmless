package gmap

import (
	"time"
)

// System represents a system on the galaxy map
type System struct {
	Id int64 `json:"id,omitempty"`

	Name string `json:"system"`
	P    Vec3   `json:"coords"`

	*SystemDetail `json:"detail,omitempty"`

	Stations []Station `json:"stations,omitempty"`

	Added string `json:"added,omitempty"`

	// last modification
	Cmdr     string    `json:"cmdr,omitempty"`
	Modified time.Time `json:"modified"`

	Valid *bool `json:"valid,omitempty"`
}

// SystemDetail contains details for inhabited systems
type SystemDetail struct {
	Pop   float64 `json:"population"`
	Alleg string  `json:"allegiance,omitempty"`
	Econ  string  `json:"economy,omitempty"`
	Govt  string  `json:"government,omitempty"`

	NeedPermit bool `json:"requires_permit,omitempty"`
}

// Station within a System
type Station struct {
	Id int64 `json:"id,omitempty"`

	Name string  `json:"station"`            // station name
	Dist float64 `json:"distance,omitempty"` // distance from star in light seconds

	StationDetail

	System *System `json:"-"` // system this station is located in
}

type StationDetail struct {
	// This flag means it is not a real station, but the system is known to have one.
	Inferred bool `json:"inferred,omitempty"` // station is inferred based on system info

	Type string `json:"type,omitempty"` // outpost or station type

	Faction string   `json:"faction,omitempty"` // main faction
	Govt    string   `json:"government,omitempty"`
	Alleg   string   `json:"allegiance,omitempty"`
	Econ    []string `json:"economy,omitempty"`

	Shipyard *bool `json:"shipyard,omitempty"`           // has shipyard
	Outfit   *bool `json:"outfitting,omitempty"`         // has outfitting
	Cmarket  *bool `json:"commodities_market,omitempty"` // has commodity market
	Bmarket  *bool `json:"black_market,omitempty"`       // has black market
}
