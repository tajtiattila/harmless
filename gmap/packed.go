package gmap

import (
	"math"
)

type PackFlag uint8

const (
	// main star in this system is scopable
	Scopable PackFlag = 1 << iota

	// System requires a permit to enter
	RequiresPermit
)

// DefaultStationDist is used for stations with invalid Dist value
const DefaultStationDist = 5000

// PackSys represents a system on the galaxy map intended for routing.
type PackSys struct {
	P Vec3i // System position

	// Log10(stn.Dist)*10 where stn is the station closest
	// to system entry point. Zero value means no station in system.
	StnDistMag uint8

	Flag PackFlag
}

func StnDistMag(dist float64) uint8 {
	if dist < 0.1 || 1e25 < dist {
		dist = DefaultStationDist
	}
	return uint8(math.Log10(dist) * 10)
}
