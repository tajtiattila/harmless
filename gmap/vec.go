package gmap

import (
	"math"
)

const Vec3iUnit = 32 // multiplier for integer vectors

// Vec3 is a 3D vector
type Vec3 [3]float64

// X() and Y() are useful for templates
func (u Vec3) X() float64 { return u[0] }
func (u Vec3) Y() float64 { return u[1] }
func (u Vec3) Z() float64 { return u[2] }

func (u Vec3) Add(v Vec3) Vec3    { return Vec3{u[0] + v[0], u[1] + v[1], u[2] + v[2]} }
func (u Vec3) Sub(v Vec3) Vec3    { return Vec3{u[0] - v[0], u[1] - v[1], u[2] - v[2]} }
func (u Vec3) Neg() Vec3          { return Vec3{-u[0], -u[1], -u[2]} }
func (u Vec3) Abs() float64       { return math.Sqrt(u.Abssq()) }
func (u Vec3) Abssq() float64     { return u.Dot(u) }
func (u Vec3) Mul(s float64) Vec3 { return Vec3{s * u[0], s * u[1], s * u[2]} }

func (u Vec3) Dot(v Vec3) float64 { return u[0]*v[0] + u[1]*v[1] + u[2]*v[2] }

func (u Vec3) Cross(v Vec3) Vec3 {
	return Vec3{
		u[1]*v[2] - u[2]*v[1],
		u[2]*v[0] - u[0]*v[2],
		u[0]*v[1] - u[1]*v[0],
	}
}

func (u Vec3) Vec3i() (v Vec3i) {
	for k := 0; k < 3; k++ {
		v[k] = int32(math.Floor(u[k]*Vec3iUnit + 0.5))
	}
	return
}

// Vec2 is a 2D vector
type Vec2 [2]float64

func (u Vec2) Coords() (x, y float64) { return u[0], u[1] }

// X() and Y() are useful for templates
func (u Vec2) X() float64 { return u[0] }
func (u Vec2) Y() float64 { return u[1] }

func (u Vec2) Add(v Vec2) Vec2    { return Vec2{u[0] + v[0], u[1] + v[1]} }
func (u Vec2) Sub(v Vec2) Vec2    { return Vec2{u[0] - v[0], u[1] - v[1]} }
func (u Vec2) Neg() Vec2          { return Vec2{-u[0], -u[1]} }
func (u Vec2) Abs() float64       { return math.Sqrt(u.Abssq()) }
func (u Vec2) Abssq() float64     { return u.Dot(u) }
func (u Vec2) Mul(s float64) Vec2 { return Vec2{s * u[0], s * u[1]} }
func (u Vec2) Dot(v Vec2) float64 { return u[0]*v[0] + u[1]*v[1] }
func (u Vec2) Norm() Vec2         { return Vec2{u[1], -u[0]} }

// Vec3i is a 3D vector made up of fixed point coordinates in 27.5 format (1/32 units)
type Vec3i [3]int32

func MakeVec3i(x, y, z float64) Vec3i {
	return Vec3{x, y, z}.Vec3i()
}

func (u Vec3i) Add(v Vec3i) Vec3i { return Vec3i{u[0] + v[0], u[1] + v[1], u[2] + v[2]} }
func (u Vec3i) Sub(v Vec3i) Vec3i { return Vec3i{u[0] - v[0], u[1] - v[1], u[2] - v[2]} }
func (u Vec3i) Neg() Vec3i        { return Vec3i{-u[0], -u[1], -u[2]} }

func (u Vec3i) Abs() float64   { return math.Sqrt(u.Abssq()) }
func (u Vec3i) Abssq() float64 { return u.Vec3().Abssq() }

func (u Vec3i) Vec3() (v Vec3) {
	for k := 0; k < 3; k++ {
		v[k] = float64(u[k]) / Vec3iUnit
	}
	return v
}
