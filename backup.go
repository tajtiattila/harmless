package main

import (
	"fmt"
	"time"

	"bitbucket.org/tajtiattila/harmless/util"
)

type FileBackup struct {
	pfx, sfx string
	timefmt  string
}

func NewFileBackup(prefix, suffix, timefmt string) *FileBackup {
	h := FileBackup{
		pfx:     prefix,
		sfx:     suffix,
		timefmt: timefmt,
	}
	return &h
}

func (h *FileBackup) Backup(modtime time.Time, data []byte) error {
	ts := h.timestamp(modtime)
	f, err := util.SafeFileWriter(h.pfx + ts + h.sfx)
	if err != nil {
		return fmt.Errorf("backuphandler/SafeFileWriter: %v", err)
	}
	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return fmt.Errorf("backuphandler/io.Write: %v", err)
	}

	return nil
}

func (h *FileBackup) timestamp(ts time.Time) string {
	return ts.UTC().Format(h.timefmt)
}
