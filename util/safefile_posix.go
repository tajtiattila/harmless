// +build !windows

package util

import (
	"io"
	"os"
)

type bfile struct {
	n string
	f *os.File

	err error
}

const tmp_suffix = ".tmp"

func SafeFileWriter(filename string) (io.WriteCloser, error) {
	f, err := os.Create(filename + tmp_suffix)
	if err != nil {
		return nil, err
	}
	return &bfile{filename, f, nil}, nil
}

func (f *bfile) Write(p []byte) (n int, err error) {
	n, err = f.f.Write(p)
	if f.err == nil {
		f.err = err
	}
	return
}

func (f *bfile) Close() (err error) {
	err = f.f.Close()
	if err != nil {
		return
	}
	if f.err != nil {
		os.Remove(f.n + tmp_suffix)
		return f.err
	}
	// we have atomic rename
	return os.Rename(f.n+tmp_suffix, f.n)
}
