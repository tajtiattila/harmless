package httpjson

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"
)

// Cache is a gzip-enabled cache of json data.
type Cache struct {
	mtx       sync.RWMutex
	modtime   time.Time
	rawp, gzp []byte
}

func (c *Cache) ModTime() time.Time {
	c.mtx.RLock()
	defer c.mtx.RUnlock()
	return c.modtime
}

func (c *Cache) Bytes() []byte {
	c.mtx.RLock()
	defer c.mtx.RUnlock()
	return c.rawp
}

func (c *Cache) GzipBytes() []byte {
	c.mtx.RLock()
	defer c.mtx.RUnlock()
	return c.gzp
}

// ServeHTTP implements net/http.Handler.
func (c *Cache) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	c.mtx.RLock()
	var data []byte
	if strings.Contains(req.Header.Get("Accept-Encoding"), "gzip") {
		w.Header().Set("Content-Encoding", "gzip")
		data = c.gzp
	} else {
		data = c.rawp
	}
	mt := c.modtime
	c.mtx.RUnlock()

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	http.ServeContent(w, req, "elite.json", mt, bytes.NewReader(data))
}

// Update updates the cache with json encodable data
// created at modtime. Update() will not update its
// buffers unless modtime is newer than the stored time.
func (c *Cache) Update(data interface{}, modtime time.Time) error {
	return c.update(data, modtime, false)
}

// ForceUpdate is like Update but updates even if modtime is
// older than the stored time.
func (c *Cache) ForceUpdate(data interface{}, modtime time.Time) error {
	return c.update(data, modtime, true)
}

func (c *Cache) update(data interface{}, modtime time.Time, force bool) error {
	if !force {
		c.mtx.RLock()
		done := !modtime.After(c.modtime)
		c.mtx.RUnlock()

		if done {
			return nil
		}
	}

	var buf bytes.Buffer

	if err := json.NewEncoder(&buf).Encode(data); err != nil {
		return fmt.Errorf("Cache/json.Encode: %v\n", err)
	}

	p := buf.Bytes()

	var gzbuf bytes.Buffer
	gz := gzip.NewWriter(&gzbuf)
	_, err := buf.WriteTo(gz)
	gz.Close()
	if err != nil {
		return fmt.Errorf("Cache/gzip.Write: %v\n", err)
	}

	c.mtx.Lock()
	defer c.mtx.Unlock()

	c.modtime = modtime
	c.rawp = p
	c.gzp = gzbuf.Bytes()

	return nil
}
