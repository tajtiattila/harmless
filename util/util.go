package util

import (
	"fmt"
	"os"
	"os/user"
	path "path/filepath"
)

// preparepath replaces tilde ('~') at the front of path
// with the user home directory, replaces environment
// variables in the format of $var or ${var} with their
// values, and creates the directories for the path
// if they don't yet exist.
func PreparePath(fn string) string {
	if len(fn) >= 2 && fn[0] == '~' && os.IsPathSeparator(fn[1]) {
		usr, err := user.Current()
		if err != nil {
			fmt.Println(err)
		} else {
			return usr.HomeDir + fn[1:]
		}
	}
	fn = os.ExpandEnv(fn)
	os.MkdirAll(path.Dir(fn), 0777)
	return fn
}
