package fuzzy

import (
	"testing"
)

type fuzzytest struct {
	a, b string // strings to measure
	d    int    // expected distance
}

var vfuzzy = []fuzzytest{
	{"ca", "abc", 3},
	{"hello", "hello", 0},
	{"he", "ben", 2},       // substitute 'b' with 'h', delete 'n'
	{"this", "tihs", 1},    // transpose 'i' and 's'
	{"toralf", "titan", 4}, // replace 'it' with 'or' (=2), delete 'l', replace 'f' with 'n'
	{"google", "goggle", 1},
	{"kitten", "sitting", 3},  // replace 's' with 'k', replace 'i' with 'e', delete 'g'
	{"Saturday", "Sunday", 3}, // insert 'a', insert 't', replace 'n' with 'r'
	{"salom", "aslom", 1},
	{"elephant", "relevant", 3},
}

func TestDist(t *testing.T) {
	for _, f := range vfuzzy {
		if d := DistOpt(f.a, f.b, DamerauLevenshtein); d != f.d {
			t.Errorf("Dist(%#v, %#v) = %#v, expected %#v", f.a, f.b, d, f.d)
		}
	}
}

type sampledictt []string

func (d sampledictt) Len() int            { return len(d) }
func (d sampledictt) String(i int) string { return d[i] }

var sampledict = sampledictt{
	"26 Draconis", "Acihaut", "Aganippe", "Asellus Primus", "Aulin", "Aulis",
	"BD+47 2112", "BD+55 1519", "Bolg", "Chi Herculis", "CM Draco", "Dahan", "DN Draconis",
	"DP Draconis", "Eranin", "G 239-25", "GD 319", "h Draconis", "Hermitage", "i Bootis", "Ithaca",
	"Keries", "Lalande 29917", "LFT 1361", "LFT 880", "LFT 992", "LHS 2819", "LHS 2884",
	"LHS 2887", "LHS 3006", "LHS 3262", "LHS 417", "LHS 5287", "LHS 6309", "LP 271-25",
	"LP 275-68", "LP 64-194", "LP 98-132", "Magec", "Meliae", "Morgor", "Nang Ta-khian", "Naraka",
	"Opala", "Ovid", "Pi-fang", "Rakapila", "Ross 1015", "Ross 1051", "Ross 1057", "Styx", "Surya",
	"Tilian", "WISE 1647+5632", "Wyrd",
}

type fftest struct {
	s, rs string
	rerr  bool
}

var vff = []fftest{
	{"3262", "LHS 3262", false},
	{"aci", "Acihaut", false},
	{"3008", "LHS 3006", false},
	{"8132", "LP 98-132", false},
	{"bootis", "i Bootis", false},
	{"booti", "i Bootis", false},
	{"wis", "WISE 1647+5632", false},
	{"morg", "Morgor", false},
	{"lhs", "", true},
	{"aha", "", true},
	{"dah", "Dahan", false},
}

func TestFind(t *testing.T) {
	for _, ff := range vff {
		r, err := Find(ff.s, sampledict)
		if (ff.rerr && err == nil) || (!ff.rerr && (err != nil || sampledict[r] != ff.rs)) {
			t.Errorf("Find(%#v) = %#v, not %#v", ff.s, sampledict[r], ff.rs)
		}
	}
}
