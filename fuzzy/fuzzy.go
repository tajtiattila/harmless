package fuzzy

import (
	"errors"
	"strings"
	"unicode"
)

// DistOpts is a set of cost values for the FuzzyFind logic.
// The zero value corresponds to the Levenshtein logic.
type DistOpts struct {
	Unit       int // cost of insert, delete and replace op
	Transpose  int // cost for transpose op (0: disable)
	PrefixBase int // base cost for "prefix" logic
	Prefix     int // cost for each dropped character (0: disable)
	SuffixBase int // base cost for "suffix" logic
	Suffix     int // cost for each dropped character (0: disable)
}

type FindOpts struct {
	DistOpts

	// difference between match distances must be this much better than the second best distance,
	// otherwise a multiple match error will be returned
	Margin int

	// Clean is the function used to clean and search strings
	Clean func(string) string
}

var (
	// Levenshtein calculates the distance using only
	// insert, delete and replace character operations.
	Levenshtein = DistOpts{}

	// DamerauLevenshtein is best used for 1-to-1 comparison
	DamerauLevenshtein = DistOpts{Transpose: 1}

	// DefaultFind favors contiguous prefix, suffix and substring matches
	// (in this order) over general DamerauLevenshtein distance.
	DefaultFind = FindOpts{
		DistOpts{
			Unit:       100,
			Transpose:  100,
			PrefixBase: 10,
			SuffixBase: 50,
			Prefix:     6,
			Suffix:     7,
		},
		100,
		LowercaseAlphaNumeric,
	}
)

// Dict is a dictionary of words for Find.
type Dict interface {
	Len() int
	String(i int) string
}

var (
	ErrSearchEmpty   = errors.New("search string is empty")
	ErrSearchInvalid = errors.New("invalid string (need alphanumeric)")
)

// ErrMultiMatch is the error returned for multiple equal matches in Find
type ErrMultiMatch struct {
	Search  string
	Matches []string
}

func (e *ErrMultiMatch) Error() string {
	s := "Search \"" + e.Search + "\" matches multiple: "
	v, pre, ext := e.Matches, "", ""
	if len(v) > 3 {
		v, ext = v[:3], "..."
	}
	for _, m := range v {
		s, pre = s+pre+"'"+m+"'", ", "
	}
	return s + ext
}

// Find searches dict for the best match for s using DefaultFind.
func Find(s string, dict Dict) (int, error) {
	return FindOpt(s, dict, DefaultFind)
}

// Find searches dict for the best match for s using Opts.
// It is an error if the input string, or the string cleaned
// with CleanFunc is empty. Furthermore, an error is returned
// if multiple strings yield a similar match.
func FindOpt(s string, dict Dict, opts FindOpts) (int, error) {
	if s == "" {
		return -1, ErrSearchEmpty
	}
	clean := opts.Clean
	if clean == nil {
		clean = Exact
	}
	cs := clean(s)
	if s == "" {
		return -1, ErrSearchInvalid
	}
	var ml marginLogic
	if opts.Margin == 0 {
		ml = &margin0logic{}
	} else {
		ml = &marginNlogic{n: opts.Margin}
	}
	slc := strings.ToLower(s)
	for i := 0; i < dict.Len(); i++ {
		if strings.ToLower(dict.String(i)) == slc {
			return i, nil
		}
		e := dict.String(i)
		d := DistOpt(cs, clean(e), opts.DistOpts)
		ml.dist(i, e, d)
	}
	return ml.result(s)
}

// Dist calculates Damerau-Levenshtein distance between
// two strings using the default options.
func Dist(a, b string) int {
	return DistOpt(a, b, DamerauLevenshtein)
}

// DistOpt calculates distance between strings using opts.
func DistOpt(s1, s2 string, opts DistOpts) int {
	sj, si := []rune(s1), []rune(s2)
	if len(si) < len(sj) {
		si, sj = sj, si
	}
	step := opts.Unit
	if step == 0 {
		step = 1
	}
	n := len(sj) + 2
	buf := make([]int, 3*n)
	row2 := buf[:n]      // row i-2
	row1 := buf[n : 2*n] // row i-1
	row0 := buf[2*n:]    // row i
	for j := 1; j <= len(sj); j++ {
		row0[1+j] = j * step
	}
	suffixval := len(si) * step
	sbase, sstep := 0, step
	if opts.Suffix != 0 {
		sbase, sstep = opts.SuffixBase, opts.Suffix // special option for match at end
	}
	for i := range si {
		row2, row1, row0 = row1, row0, row2
		row0[1] = sbase + (i+1)*sstep
		for j := range sj {
			var v int
			if si[i] == sj[j] {
				v = row1[1+j]
			} else {
				v = minimum(
					row1[2+j]+step, // deletion
					row0[1+j]+step, // insertion
					row1[1+j]+step, // substitution
				)
			}
			if i > 0 && j > 0 && si[i] == sj[j-1] && si[i-1] == sj[j] && row2[j]+opts.Transpose < v {
				v = row2[j] + opts.Transpose // transposition
			}
			if j == len(sj)-1 {
				suffixval += opts.Prefix
				if v < suffixval {
					suffixval = v
				}
			}
			row0[2+j] = v
		}
	}
	v := row0[1+len(sj)]
	if opts.Prefix != 0 && suffixval+opts.PrefixBase < v {
		v = suffixval + opts.PrefixBase // special option for match at the beginning
	}
	return v
}

func minimum(a, b, c int) int {
	if a < b {
		if a < c {
			return a
		} else {
			return c
		}
	} else {
		if b < c {
			return b
		} else {
			return c
		}
	}
	return 0 // not reached
}

func LowercaseAlphaNumeric(s string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsLetter(r) || unicode.IsDigit(r) {
			return unicode.ToLower(r)
		}
		return -1
	}, s)
}

func Exact(s string) string {
	return s
}

type marginLogic interface {
	dist(index int, sdict string, dist int)
	result(search string) (int, error)
}

// margin0logic is used when FindOpts.Margin is zero
type margin0logic struct {
	vm     []string
	mi, md int
}

func (l *margin0logic) dist(index int, sdict string, dist int) {
	if len(l.vm) == 0 {
		l.vm = []string{sdict}
		l.mi, l.md = index, dist
	} else {
		if dist == l.md {
			l.vm = append(l.vm, sdict)
		} else {
			l.mi, l.md = index, dist
			l.vm[0] = sdict
			l.vm = l.vm[:1]
		}
	}
}

func (l *margin0logic) result(search string) (int, error) {
	if len(l.vm) == 1 {
		return l.mi, nil
	}
	return -1, &ErrMultiMatch{search, l.vm}
}

// margin0logic is used when FindOpts.Margin is zero
type marginNlogic struct {
	n  int
	vm []match
	md int
}

type match struct {
	dist  int
	index int
	value string
}

func (l *marginNlogic) dist(index int, sdict string, dist int) {
	if len(l.vm) == 0 {
		l.vm = []match{{dist, index, sdict}}
		l.md = dist
	} else {
		switch {
		case dist+l.n <= l.md:
			l.md = dist
			l.vm[0] = match{dist, index, sdict}
			l.vm = l.vm[:1]
		case dist < l.md:
			l.md = dist
			fallthrough
		case l.md+l.n <= dist:
			l.vm = append(l.vm, match{dist, index, sdict})
		}
	}
}

func (l *marginNlogic) result(search string) (int, error) {
	l.clean()
	if len(l.vm) == 1 {
		return l.vm[0].index, nil
	}
	v := make([]string, len(l.vm))
	for i := range l.vm {
		v[i] = l.vm[i].value
	}
	return -1, &ErrMultiMatch{search, v}
}

func (l *marginNlogic) clean() {
	j := 0
	for i := 0; i < len(l.vm); {
		if l.md+l.n <= l.vm[i].dist {
			i++
		} else {
			l.vm[j] = l.vm[i]
			i++
			j++
		}
	}
	l.vm = l.vm[:j]
}
