module bitbucket.org/tajtiattila/harmless

go 1.13

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8 // indirect
	golang.org/x/text v0.3.2
)
