#!/usr/bin/env python

from __future__ import print_function

st = set()
with open('BrookesSystems.txt') as f:
    for line in f:
        st.add(line.strip())

"""
in sqlite3 command line do:

.once SqlSystems.txt
select sys.name from System sys
    inner join Station st
	on sys.system_id = st.system_id
    group by sys.name
    order by sys.name;
    """

with open('SqlSystems.txt') as f:
    for line in f:
        st.remove(line.strip())

v = sorted(list(st))
idx = 0
print("BEGIN TRANSACTION;")
for syst in v:
    print("""INSERT INTO Station(name, system_id, ls_from_star) VALUES('*{0}',
    (SELECT system_id FROM System WHERE name = '{0}'), 0);""".format(syst))
    idx += 1
print("COMMIT;");

"""
select sys.name from System sys
    inner join Station st
	on sys.system_id = st.system_id
    where st.name LIKE 'Unknown%'
    order by sys.name;
    """
