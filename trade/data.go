package trade

type Category struct {
	Name  string      `json:"category"`
	Items []Commodity `json:"items"`
}

type Commodity struct {
	Name     string   `json:"commodity"`
	AltNames []string `json:"altnames,omitempty"`
	Vendors  []string `json:"vendors,omitempty"`
}
