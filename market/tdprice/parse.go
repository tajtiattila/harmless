package tdprice

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

/* format to parse

# <item name> <sell> <buy> <timestamp> demand <demand#>L<level> stock <stock#>L<level>
#  demand#/stock#: the quantity available or -1 for 'unknown'
#  level: 0 = None, 1 = Low, 2 = Medium, 3 = High, -1 = Unknown

@ 14 HERCULIS/Ejeta Station
   + Chemicals
      Explosives                258    278   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Hydrogen Fuels            167      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Mineral Oil               316      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Pesticides                175    190   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
   + Consumer Items
      Clothing                  389      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Consumer Tech            7221      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Dom. Appliances           625      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
   + Drugs
      Beer                      233      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Wine                      376      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
   + Foods
      Animal Meat              1453      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
      Coffee                   1489      0   2014-10-18 00:25:27 demand      -1L-1 stock      -1L-1
*/

// regexps used in TD:
//  noCommentRe = re.compile(r'^\s*(?P<text>(?:[^\\#]|\\.)+?)\s*(#|$)')
//  systemStationRe = re.compile(r'^@\s*(.*)\s*/\s*(.*)')
//  categoryRe = re.compile(r'^\+\s*(.*?)\s*$')
//  itemPriceRe = re.compile(r'^(.*?)\s+(\d+)\s+(\d+)(?:\s+(\d{4}-.*?)(?:\s+demand\s+(-?\d+)L(-?\d+)\s+stock\s+(-?\d+)L(-?\d+))?)?$')

type DemandSupply int

const (
	Unknown DemandSupply = -1
	None    DemandSupply = 0
	Low     DemandSupply = 1
	Medium  DemandSupply = 2
	High    DemandSupply = 3
)

type Parser struct {
	s        *bufio.Scanner
	pl       PriceLine
	lno      int
	badlines []int
}

type PriceLine struct {
	System, Station string

	Category string

	Item string
	Sell int
	Buy  int

	Modified      time.Time
	Stock, Demand LevelData
}

type BadLines []int

func (b BadLines) Error() string {
	return fmt.Sprint("Can't parse lines:", b)
}

type LevelData struct {
	Qty   int
	Level DemandSupply
}

func NewParser(r io.Reader) *Parser {
	return &Parser{s: bufio.NewScanner(r)}
}

func (p *Parser) Scan() bool {
	for p.s.Scan() {
		line := p.s.Text()
		p.lno++
		i := strings.IndexRune(line, '#')
		if i != -1 {
			line = line[:i]
		}
		line = strings.TrimSpace(line)
		if line == "" || p.locline(line) || p.catline(line) {
			continue
		}
		if p.dataline(line) {
			return true
		}
		p.badlines = append(p.badlines, p.lno)
	}
	return false
}

func (p *Parser) Line() *PriceLine {
	return &p.pl
}

func (p *Parser) Err() error {
	err := p.s.Err()
	if err == nil && p.badlines != nil {
		err = BadLines(p.badlines)
	}
	return err
}

func (p *Parser) line(line string) bool {
	i := strings.IndexRune(line, '#')
	if i != -1 {
		line = line[:i]
	}
	line = strings.TrimSpace(line)
	if line == "" {
		return true
	}
	return p.locline(line) || p.catline(line) || p.dataline(line)
}

func (p *Parser) locline(line string) bool {
	if line[0] != '@' {
		return false
	}
	i := strings.IndexRune(line, '/')
	if i == -1 {
		return false
	}
	p.pl.System, p.pl.Station = strings.TrimSpace(line[1:i]), strings.TrimSpace(line[i+1:])
	return true
}

func (p *Parser) catline(line string) bool {
	if line[0] != '+' {
		return false
	}
	p.pl.Category = line[1:]
	return true
}

func (p *Parser) dataline(line string) bool {
	y := time.Now().Year()
	y0i := sidx(line, fmt.Sprintf("%4d-", y))
	y1i := sidx(line, fmt.Sprintf("%4d-", y-1))
	var yi int
	if y0i < y1i {
		yi = y0i
	} else {
		yi = y1i
	}
	if !p.parseNameBuySell(strings.TrimSpace(line[:yi])) {
		return false
	}

	if yi == len(line) {
		return true
	}

	di := sidx(line, "demand")
	si := sidx(line, "stock")
	s := strings.TrimSpace(line[yi:di])
	t, err := time.Parse("2006-01-02 15:04:05Z07:00", s)
	if err != nil {
		t, err = time.Parse(time.RFC3339, s)
	}
	if err == nil {
		p.pl.Modified = t
	} else {
		p.pl.Modified = time.Now().Add(-4 * 168 * time.Hour) // assume 4 weeks old
	}

	p.parseLevel(&p.pl.Demand, line[di:si])
	p.parseLevel(&p.pl.Stock, line[si:])
	return true
}

func (p *Parser) parseNameBuySell(s string) bool {
	i := strings.LastIndexAny(s, " \t")
	if i == -1 {
		return false
	}
	buy, err := strconv.Atoi(s[i+1:])
	if err != nil {
		return false
	}
	s = strings.TrimSpace(s[:i])
	i = strings.LastIndexAny(s, " \t")
	if i == -1 {
		return false
	}
	sell, err := strconv.Atoi(s[i+1:])
	if err != nil {
		return false
	}
	name := strings.TrimSpace(s[:i])
	p.pl.Item = name
	p.pl.Buy = buy
	p.pl.Sell = sell
	return true
}

func (p *Parser) parseLevel(l *LevelData, s string) {
	// s ==
	//    'stock' space* <qty>L<level>
	//    'demand' space* <qty>L<level>
	v := strings.Fields(s)
	if len(v) > 0 {
		s = v[1] // <qty>L<level>
		v = strings.SplitN(s, "L", 2)
		if len(v) >= 2 {
			if n, err := strconv.Atoi(v[1]); err != nil && n >= 0 {
				l.Level = DemandSupply(n)
			} else {
				l.Level = Unknown
			}
		} else {
			l.Level = Unknown
		}
		if len(v) >= 1 {
			var err error
			if l.Qty, err = strconv.Atoi(v[0]); err != nil || l.Qty < 0 {
				l.Qty = 0
			}
		} else {
			l.Qty = 0
		}
	}
}

func sidx(s, sep string) int {
	i := strings.Index(s, sep)
	if i >= 0 {
		return i
	}
	return len(s)
}
