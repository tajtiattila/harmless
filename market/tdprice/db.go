package tdprice

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"io"
	"strings"
)

type UnknownHandler interface {
	MissingItem(string)
	MissingStation(string)
}

type devNullHandler struct{}

func (*devNullHandler) MissingItem(string)    {}
func (*devNullHandler) MissingStation(string) {}

func UpdateDb(dbfn string, r io.Reader) error {
	return UpdateDbParam(dbfn, r, &devNullHandler{})
}

func UpdateDbParam(dbfn string, r io.Reader, h UnknownHandler) error {
	db, err := sql.Open("sqlite3", dbfn)
	if err != nil {
		return err
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Commit()

	mitem := make(map[string]int)
	mstat := make(map[string]int)

	rows, err := tx.Query(`SELECT item_id,name FROM Item;`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id   int
			name string
		)
		if err = rows.Scan(&id, &name); err != nil {
			return err
		}
		mitem[strings.ToLower(name)] = id
	}

	rows, err = tx.Query(`SELECT item_id,alt_name FROM AltItemNames;`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id   int
			name string
		)
		if err = rows.Scan(&id, &name); err != nil {
			return err
		}
		mitem[strings.ToLower(name)] = id
	}

	rows, err = tx.Query(`SELECT st.station_id,sys.name,st.name FROM Station st
							INNER JOIN System sys ON st.system_id=sys.system_id;`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var (
			id   int
			sys  string
			stat string
		)
		if err = rows.Scan(&id, &sys, &stat); err != nil {
			return err
		}
		mstat[strings.ToLower(sys+"/"+stat)] = id
	}

	stmt, err := db.Prepare(`INSERT INTO Price(item_id,station_id,sell_to,buy_from,
					modified,demand,demand_level,stock,stock_level)
					VALUES(?,?,?,?,?,?,?,?,?);`)
	if err != nil {
		return err
	}
	stmt = tx.Stmt(stmt)

	p := NewParser(r)
	for p.Scan() {
		l := p.Line()
		n := strings.ToLower(l.Item)
		itemid, ok := mitem[n]
		if !ok {
			h.MissingItem(n)
			continue
		}
		n = strings.ToLower(l.System + "/" + l.Station)
		statid, ok := mstat[n]
		if !ok {
			h.MissingStation(n)
			continue
		}

		_, err := stmt.Exec(itemid, statid, l.Sell, l.Buy, l.Modified,
			l.Demand.Qty, l.Demand.Level, l.Stock.Qty, l.Stock.Level)
		if err != nil {
			return err
		}
	}
	return p.Err()
}
