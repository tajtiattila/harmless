package market

import (
	"fmt"
)

type DataSource interface {
	Query() (*Data, error)
	Close() error
}

type Driver func(string) (DataSource, error)

var ddrivermap = make(map[string]Driver)

// Register makes a DataSource driver available by the provided name.
func Register(kind string, drv Driver) {
	if _, ok := ddrivermap[kind]; ok || drv == nil {
		panic("RegisterMarketDataSource: kind '" + kind + "' registered multiple times")
	}
	ddrivermap[kind] = drv
}

// Open DataSource kind with the (type-specific) spec as source/url.
func Open(kind string, spec string) (DataSource, error) {
	if d, ok := ddrivermap[kind]; ok {
		return d(spec)
	}
	return nil, fmt.Errorf("Unknown DataSource driver: " + kind)
}
