package elitetrade

import (
	"bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
	"bitbucket.org/tajtiattila/harmless/market/quote"
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"time"
)

func init() {
	market.Register("sqlite", func(fn string) (market.DataSource, error) {
		return OpenMarketDb(fn)
	})
}

const (
	MarketTableName = "marketdata"
)

type MarketDB struct {
	db *sql.DB

	insItem       *sql.Stmt
	insLoc        *sql.Stmt
	insMarketData *sql.Stmt

	selItem       *sql.Stmt
	selLoc        *sql.Stmt
	selMarketData *sql.Stmt

	selItemId *sql.Stmt
	selLocId  *sql.Stmt

	hourly marketStat
	daily  marketStat
	weekly marketStat

	locCache  map[string]int
	itemCache map[string]int
}

func OpenMarketDb(fn string) (mdb *MarketDB, err error) {
	db, err := sql.Open("sqlite3", fn)
	if err != nil {
		return nil, err
	}

	if _, err = db.Exec("PRAGMA journal_mode=WAL;"); err != nil {
		return nil, err
	}

	prep := prepare{db: db}

	mdb = &MarketDB{db: db}

	prep.table("loc", `(
		id      INTEGER NOT NULL PRIMARY KEY,
		system  TEXT,
		station TEXT,
		UNIQUE (system, station)
	)`)
	prep.table("item", `(
		id       INTEGER NOT NULL PRIMARY KEY,
		name     TEXT UNIQUE,
		category TEXT
	)`)
	prep.table("marketdata", `(
		locationid INTEGER NOT NULL REFERENCES loc(id),
		itemid     INTEGER NOT NULL REFERENCES item(id),
		timestamp  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		buy        INTEGER NOT NULL,
		sell       INTEGER NOT NULL,
		stock      INTEGER NOT NULL,
		demand     INTEGER NOT NULL,
		PRIMARY KEY (locationid, itemid)
	)`)

	mdb.insItem = prep.stmt("INSERT OR IGNORE INTO item(name, category) VALUES(?,?);")
	mdb.insLoc = prep.stmt("INSERT OR IGNORE INTO loc(system, station) VALUES(?,?);")
	mdb.insMarketData = prep.stmt(
		`INSERT OR REPLACE INTO marketdata(itemid, locationid, timestamp, buy, sell, stock, demand)
			VALUES (?, ?, ?, ?, ?, ?, ?);`)

	mdb.selItem = prep.stmt("SELECT id, name, category FROM item;")
	mdb.selLoc = prep.stmt("SELECT id, system, station FROM loc;")

	mdb.selItemId = prep.stmt("SELECT id FROM item WHERE name=? AND category=?;")
	mdb.selLocId = prep.stmt("SELECT id FROM loc WHERE system=? AND station=?;")

	mdb.hourly = prep.stat("hourly", 7, 10*time.Minute)
	mdb.daily = prep.stat("daily", 13, 2*time.Hour)
	mdb.weekly = prep.stat("weekly", 8, 24*time.Hour)

	ij := `INNER JOIN (SELECT
					locationid, itemid,
					MIN(minbuy) AS minb,
					SUM(buy)/SUM(buydiv) AS avgb,
					MAX(maxbuy) AS maxb,
					MIN(minsell) AS mins,
					SUM(sell)/SUM(selldiv) AS avgs,
					MAX(maxsell) AS maxs
				FROM %[1]s
				GROUP BY itemid, locationid) %[2]s
				ON %[2]s.locationid = m.locationid AND %[2]s.itemid = m.itemid `

	mdb.selMarketData = prep.stmt(
		`SELECT m.itemid, m.locationid, m.timestamp, m.buy, m.sell, m.stock, m.demand,
				sw.minb, sw.avgb, sw.maxb, sw.mins, sw.avgs, sw.maxs,
				sd.minb, sd.avgb, sd.maxb, sd.mins, sd.avgs, sd.maxs,
				sh.minb, sh.avgb, sh.maxb, sh.mins, sh.avgs, sh.maxs
			FROM marketdata m ` +
			fmt.Sprintf(ij, "hourly", "sh") +
			fmt.Sprintf(ij, "daily", "sd") +
			fmt.Sprintf(ij, "weekly", "sw"))

	if prep.err != nil {
		return nil, prep.err
	}

	mdb.itemCache = make(map[string]int)
	mdb.locCache = make(map[string]int)

	return
}

func (mdb *MarketDB) Close() error {
	return mdb.db.Close()
}

func (mdb *MarketDB) Insert(v []quote.Quote) (err error) {
	if len(v) == 0 {
		return nil
	}
	mdb.insert(v, func(e error) bool { err = e; return false })
	return
}

func (mdb *MarketDB) InsertOne(m *quote.Quote) (err error) {
	mdb.insert([]quote.Quote{*m}, func(e error) bool { err = e; return false })
	return
}

func (mdn *MarketDB) InsertChan(maxcache int, delay time.Duration) chan<- quote.Quote {
	ch := make(chan quote.Quote)
	go func() {
		buf := make([]quote.Quote, maxcache)
		ticker := time.NewTicker(delay)
		defer ticker.Stop()
		p := 0
		for {
			select {
			case m, ok := <-ch:
				if !ok {
					return
				}
				buf[p], p = m, p+1
				if p == maxcache {
					mdn.insert(buf, logerrh)
					p = 0
				}
			case <-ticker.C:
				if p != 0 {
					mdn.insert(buf[:p], logerrh)
					p = 0
				}
			}
		}
	}()
	return ch
}

// Checkpoint all frames in the write ahead log,
// and sync them into the database. Call this method
// after imports.
func (mdb *MarketDB) Checkpoint() (err error) {
	_, err = mdb.db.Exec("PRAGMA wal_checkpoint(FULL);")
	return
}

func (mdb *MarketDB) Query() (d *market.Data, err error) {
	tx, err := mdb.db.Begin()
	if err != nil {
		return
	}
	defer tx.Commit() // no changes

	var comms map[int]*market.CommodityData
	if comms, err = mdb.commodities(tx); err != nil {
		return
	}

	var locs map[int]*market.StationData
	if locs, err = mdb.stations(tx); err != nil {
		return
	}

	var rows *sql.Rows
	rows, err = tx.Stmt(mdb.selMarketData).Query()
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			cid, lid      int
			ts            time.Time
			buy, sell     market.Price
			stock, demand int
			wminb, wavgb, wmaxb, wmins, wavgs, wmaxs,
			dminb, davgb, dmaxb, dmins, davgs, dmaxs,
			hminb, havgb, hmaxb, hmins, havgs, hmaxs market.Price
		)
		err = rows.Scan(&cid, &lid, &ts, &buy, &sell, &stock, &demand,
			&wminb, &wavgb, &wmaxb, &wmins, &wavgs, &wmaxs,
			&dminb, &davgb, &dmaxb, &dmins, &davgs, &dmaxs,
			&hminb, &havgb, &hmaxb, &hmins, &havgs, &hmaxs)
		if err != nil {
			return
		}
		if l, c := locs[lid], comms[cid]; l != nil && c != nil {
			i := &market.PriceData{
				Timestamp: ts.In(time.Local),
				Buy: market.PriceInfo{
					buy,
					stock,
					market.MinMaxAvg{hminb, havgb, hmaxb},
					market.MinMaxAvg{dminb, davgb, dmaxb},
					market.MinMaxAvg{wminb, wavgb, wmaxb},
				},
				Sell: market.PriceInfo{
					sell,
					demand,
					market.MinMaxAvg{hmins, havgs, hmaxs},
					market.MinMaxAvg{dmins, davgs, dmaxs},
					market.MinMaxAvg{wmins, wavgs, wmaxs},
				},
			}
			c.Stations = append(c.Stations, market.CommodityEntry{l, i})
			l.Commodities = append(l.Commodities, market.StationEntry{c, i})
		}
	}
	if err = rows.Err(); err != nil {
		return
	}

	d = &market.Data{make([]*market.CommodityData, 0, len(comms)), make([]*market.StationData, 0, len(locs))}
	for _, c := range comms {
		d.Commodities = append(d.Commodities, c)
	}
	for _, l := range locs {
		d.Stations = append(d.Stations, l)
	}
	return
}

func (mdb *MarketDB) commodities(tx *sql.Tx) (map[int]*market.CommodityData, error) {
	comms := make(map[int]*market.CommodityData)

	rows, err := tx.Stmt(mdb.selItem).Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var name, cat string
		err = rows.Scan(&id, &name, &cat)
		if err != nil {
			return nil, err
		}
		comms[id] = &market.CommodityData{market.Commodity{Name: name, Category: cat}, nil}
	}
	return comms, rows.Err()
}

func (mdb *MarketDB) stations(tx *sql.Tx) (map[int]*market.StationData, error) {
	m := make(map[int]*market.StationData)

	rows, err := tx.Stmt(mdb.selLoc).Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var system, station string
		err = rows.Scan(&id, &system, &station)
		if err != nil {
			return nil, err
		}
		m[id] = &market.StationData{harmless.GetStarStation(system), nil}
	}
	return m, rows.Err()
}

// Insert multiple records into the database. The function
// errorhandler will be called for any error encountered. If it
// returns false, processing after non-fatal errors will continue.
func (mdb *MarketDB) insert(v []quote.Quote, errorhandler func(error) bool) {
	errh := func(e error) bool {
		if e != nil {
			return errorhandler(e)
		}
		return true
	}
	tx, err := mdb.db.Begin()
	if err != nil {
		errh(err)
		return
	}
	defer func() {
		if err == nil {
			errh(tx.Commit())
		} else {
			errh(err)
			tx.Rollback()
		}
	}()

	locins := prepstmt{tx, mdb.insLoc, nil}
	locsel := prepstmt{tx, mdb.selLocId, nil}
	itemins := prepstmt{tx, mdb.insItem, nil}
	itemsel := prepstmt{tx, mdb.selItemId, nil}
	defer locins.Close()
	defer locsel.Close()
	defer itemins.Close()
	defer itemsel.Close()

	insMarketData := tx.Stmt(mdb.insMarketData)
	defer insMarketData.Close()
	insHourly := tx.Stmt(mdb.hourly.ins)
	defer insHourly.Close()
	insDaily := tx.Stmt(mdb.daily.ins)
	defer insDaily.Close()
	insWeekly := tx.Stmt(mdb.weekly.ins)
	defer insWeekly.Close()

	var locid, itemid int
	for _, mq := range v {
		itemid, err = mdb.itemid(&itemins, &itemsel, mq.Item, mq.Category)
		if !errh(err) {
			return
		}

		locid, err = mdb.locid(&locins, &locsel, mq.System, mq.Station)
		if !errh(err) {
			return
		}

		_, err = insMarketData.Exec(
			itemid, locid, mq.Timestamp,
			mq.Buy, mq.Sell, mq.Stock, mq.Demand)
		if !errh(err) {
			return
		}

		if !errh(mdb.hourly.update(insHourly, itemid, locid, mq)) ||
			!errh(mdb.daily.update(insDaily, itemid, locid, mq)) ||
			!errh(mdb.weekly.update(insWeekly, itemid, locid, mq)) {
			return
		}
	}
}

func (mdb *MarketDB) itemid(ins, sel *prepstmt, name, cat string) (i int, err error) {
	var ok bool
	if i, ok = mdb.itemCache[name+"|"+cat]; ok {
		return
	}
	if _, err = ins.Stmt().Exec(name, cat); err != nil {
		return
	}
	if err = sel.Stmt().QueryRow(name, cat).Scan(&i); err == nil {
		mdb.itemCache[name+"|"+cat] = i
	}
	return
}

func (mdb *MarketDB) locid(ins, sel *prepstmt, sys, stat string) (i int, err error) {
	var ok bool
	if i, ok = mdb.locCache[sys+"|"+stat]; ok {
		return
	}
	if _, err = ins.Stmt().Exec(sys, stat); err != nil {
		return
	}
	if err = sel.Stmt().QueryRow(sys, stat).Scan(&i); err == nil {
		mdb.locCache[sys+"|"+stat] = i
	}
	return
}

type prepstmt struct {
	tx         *sql.Tx
	stmt, prep *sql.Stmt
}

func (p *prepstmt) Stmt() *sql.Stmt {
	if p.prep == nil {
		p.prep = p.tx.Stmt(p.stmt)
	}
	return p.prep
}

func (p *prepstmt) Close() error {
	if p.prep != nil {
		return p.prep.Close()
	}
	return nil
}

type prepare struct {
	db  *sql.DB
	err error
}

func (p *prepare) table(name, def string) {
	if p.err != nil {
		return
	}
	_, p.err = p.db.Exec("CREATE TABLE IF NOT EXISTS " + name + " " + def + ";")
}

func (p *prepare) stmt(q string) (s *sql.Stmt) {
	if p.err != nil {
		return nil
	}
	s, p.err = p.db.Prepare(q)
	return
}

func (p *prepare) stat(name string, count int, duration time.Duration) marketStat {
	p.table(name, `(
		locationid INTEGER NOT NULL REFERENCES loc(id),
		itemid     INTEGER NOT NULL REFERENCES item(id),
		timestamp  TIMESTAMP NOT NULL,
		buy        INTEGER NOT NULL,
		buydiv     INTEGER NOT NULL,
		sell       INTEGER NOT NULL,
		selldiv    INTEGER NOT NULL,
		minbuy     INTEGER NOT NULL,
		maxbuy     INTEGER NOT NULL,
		minsell    INTEGER NOT NULL,
		maxsell    INTEGER NOT NULL,
		PRIMARY KEY (locationid, itemid, timestamp)
	)`)
	match := `FROM ` + name + ` WHERE itemid=?1 AND locationid=?2 AND timestamp=?3`
	s := p.stmt(`INSERT OR REPLACE INTO ` + name + `
			(itemid, locationid, timestamp, buy, buydiv, sell, selldiv,
				minbuy, maxbuy, minsell, maxsell)
			VALUES (
				?1, ?2, ?3,
				COALESCE( (SELECT buy     ` + match + `), 0) + ?4,
				COALESCE( (SELECT buydiv  ` + match + `), 0) + 1,
				COALESCE( (SELECT sell    ` + match + `), 0) + ?5,
				COALESCE( (SELECT selldiv ` + match + `), 0) + 1,
				MIN(COALESCE( (SELECT minbuy  ` + match + `), ?4), ?4),
				MAX(COALESCE( (SELECT maxbuy  ` + match + `), ?4), ?4),
				MIN(COALESCE( (SELECT minsell ` + match + `), ?5), ?5),
				MAX(COALESCE( (SELECT maxsell ` + match + `), ?5), ?5));`)
	c := p.stmt(`DELETE FROM ` + name + ` WHERE timestamp < ?;`)
	return marketStat{int64(duration / time.Second), time.Duration(-count) * duration, s, c}
}

func logerrh(err error) bool {
	log.Println(err)
	return true
}

type marketStat struct {
	gransec int64
	delta   time.Duration
	ins     *sql.Stmt
	clean   *sql.Stmt
}

func (s *marketStat) update(ins *sql.Stmt, itemid, locid int, mq quote.Quote) error {
	u := mq.Timestamp.Unix()
	t := time.Unix(u-(u%s.gransec), 0).In(time.UTC)
	if t.Before(time.Now().Add(s.delta)) {
		return nil
	}
	_, err := ins.Exec(itemid, locid, t, mq.Buy, mq.Sell)
	return err
}
