package quote

import (
	"fmt"
	"time"
)

// Quote represents market details for an item at the given location
type Quote struct {
	Timestamp       time.Time
	System, Station string
	Category, Item  string
	Buy, Sell       int
	Demand, Stock   int
}

func (q *Quote) String() string {
	return fmt.Sprintf("%-20.20s %5d+ %5d- %8d", q.System, q.Sell, q.Buy, q.Stock)
}

type Source interface {
	Read(*Quote) error
	Close() error
}

type Driver func(string) (Source, error)

var qdrivermap = make(map[string]Driver)

// RegisterMarketQuoteSource makes a Source driver available by the provided name.
func Register(kind string, drv Driver) {
	if _, ok := qdrivermap[kind]; ok || drv == nil {
		panic("Register: kind '" + kind + "' registered multiple times")
	}
	qdrivermap[kind] = drv
}

// Open source kind with the (type-specific) spec as source/url.
func Open(kind string, spec string) (Source, error) {
	if d, ok := qdrivermap[kind]; ok {
		return d(spec)
	}
	return nil, fmt.Errorf("Unknown source driver: " + kind)
}
