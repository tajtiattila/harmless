package emdn

import (
	"bitbucket.org/tajtiattila/harmless/market/quote"
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type Content interface {
	Type() string
	String() string
}

type Message struct {
	Content   Content
	Sender    string
	Signature string
	Version   string
}

type rawMessage struct {
	Message   map[string]interface{} `json:'message'`
	Sender    string                 `json:'sender'`
	Signature string                 `json:'signature'`
	Type      string                 `json:'type'`
	Version   string                 `json:'version'`
}

func (m *Message) UnmarshalJSON(data []byte) (err error) {
	var r rawMessage
	if err = json.Unmarshal(data, &r); err != nil {
		return err
	}
	m.Sender, m.Signature, m.Version = r.Sender, r.Signature, r.Version
	m.Content, err = decodeContent(r.Version, r.Type, r.Message)
	if err != nil {
		return err
	}
	return nil
}

func decodeContent(ver, typ string, m map[string]interface{}) (c Content, err error) {
	defer func() {
		if r := recover(); r != nil {
			c, err = nil, fmt.Errorf("Error decoding type %s from %s", typ, m)
		}
	}()
	switch typ {
	case "marketquote":
		var t time.Time
		t, err = time.Parse(time.RFC3339, m["timestamp"].(string))
		if err != nil {
			return
		}
		system, station := splitLocation(m["stationName"].(string))

		return &MarketQuoteContent{quote.Quote{
			Timestamp: t,
			System:    system,
			Station:   station,
			Category:  m["categoryName"].(string),
			Item:      m["itemName"].(string),
			Buy:       int(m["buyPrice"].(float64)),
			Sell:      int(m["sellPrice"].(float64)),
			Demand:    int(m["demand"].(float64)),
			Stock:     int(m["stationStock"].(float64)),
		}}, nil
	}
	c = &UnknownContent{typ, m}
	return
}

func splitLocation(loc string) (string, string) {
	s, e := strings.IndexRune(loc, '('), strings.IndexRune(loc, ')')
	if s != -1 && e != -1 {
		return strings.TrimSpace(loc[:s-1]), loc[s+1 : e]
	}
	return loc, ""
}

type MarketQuoteContent struct {
	quote.Quote
}

func (*MarketQuoteContent) Type() string { return "marketquote" }

func (q *MarketQuoteContent) String() string {
	return q.Quote.String()
}

// UnknownContent represents Message content not recognised by this module.
type UnknownContent struct {
	typ  string
	Data map[string]interface{}
}

func (c *UnknownContent) Type() string { return c.typ }

func (c *UnknownContent) String() string { return fmt.Sprint(c.Data) }
