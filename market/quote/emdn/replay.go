package emdn

import (
	"bitbucket.org/tajtiattila/harmless/market/quote"
	"encoding/json"
	"io"
	"os"
)

type ReplayJson struct {
	f       io.Closer
	decoder *json.Decoder
}

func NewReplayJson(fn string) (*ReplayJson, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	return &ReplayJson{f, json.NewDecoder(f)}, nil
}

func (r *ReplayJson) Read(mq *quote.Quote) error {
	for {
		var m Message
		if err := r.decoder.Decode(&m); err != nil {
			return err
		}

		if mqc, ok := m.Content.(*MarketQuoteContent); ok {
			*mq = mqc.Quote
			return nil
		}
	}
	return nil // not reached
}

func (r *ReplayJson) Close() error {
	return r.f.Close()
}

func init() {
	quote.Register("firejson", func(fn string) (quote.Source, error) {
		return NewReplayJson(fn)
	})
}
