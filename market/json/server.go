package marketjson

import (
	"bitbucket.org/tajtiattila/harmless/market"
	"bytes"
	"compress/zlib"
	"encoding/json"
	"net/http"
	"sync"
	"time"
)

type Server struct {
	source market.DataSource

	mtx        sync.RWMutex
	data, work []byte
}

func NewServer(source market.DataSource) (*Server, error) {
	s := &Server{
		source: source,
		data:   make([]byte, 0, 65536),
		work:   make([]byte, 0, 65536),
	}
	if err := s.runquery(); err != nil {
		return nil, err
	}
	go func() {
		for _ = range time.Tick(1 * time.Minute) {
			s.runquery()
		}
	}()
	return s, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/zlib")
	s.mtx.RLock()
	w.Write(s.data)
	s.mtx.RUnlock()
}

func (s *Server) runquery() error {
	q, err := s.source.Query()
	var data *Data
	if err == nil {
		data = NewData(q)
	} else {
		data = &Data{Error: err.Error()}
	}
	buf := bytes.NewBuffer(s.work[:0])
	z := zlib.NewWriter(buf)
	err2 := json.NewEncoder(z).Encode(data)
	z.Close()
	if err2 == nil {
		s.mtx.Lock()
		s.data, s.work = buf.Bytes(), s.data[:0]
		s.mtx.Unlock()
	}
	if err != nil {
		return err
	}
	return err2
}
