package structsql

import (
	"bytes"
	"database/sql"
	"fmt"
	"reflect"
)

type Queryer interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
}

type Query struct {
	qr     Queryer
	fields bytes.Buffer
	query  bytes.Buffer
	args   []interface{}
	res    []interface{}
	qs     string
}

// Add adds further clauses such as WHERE, GROUP BY, ORDER BY, LIMIT
// etc to the Execable. It has no effect after Do, DoId or DoNum
// has been called.
func (q *Query) Add(query string, args ...interface{}) *Query {
	if q.qs != "" {
		return q
	}
	fmt.Fprint(&q.query, " ", query)
	q.args = append(q.args, args...)
	return q
}

func Select(qr Queryer, vi ...interface{}) *Query {
	return SelectCmd(qr, "SELECT", vi...)
}

func SelectCmd(qr Queryer, cmd string, vi ...interface{}) *Query {
	q := &Query{qr: qr}
	fmt.Fprint(&q.fields, cmd)
	fmt.Fprint(&q.query, " FROM ")
	sepp, sepq := " ", ""
	for _, i := range vi {
		v := reflect.ValueOf(i)
		if v.Kind() == reflect.String {
			fmt.Fprintf(&q.query, "%s%s", sepq, v.String())
			sepq = ","
		} else {
			if v.Kind() != reflect.Ptr {
				panic(StructError("structsql: Select needs a pointer receiver"))
			}
			v = v.Elem()
			ts := getStruct(v.Type())
			fmt.Fprintf(&q.query, "%s%s", sepq, ts.t.name)
			sepq = ","
			for _, c := range ts.cf {
				fmt.Fprintf(&q.fields, "%s%s.%s", sepp, ts.t.name, c.name)
				sepp = ", "
				q.res = append(q.res, v.FieldByIndex(c.byIndex).Addr().Interface())
			}
		}
	}
	return q
}

func (q *Query) Join(kind string, i interface{}, cond string) *Query {
	// TODO: queryargs?
	if s, ok := i.(string); ok {
		fmt.Fprintf(&q.query, " %s JOIN %s %s", kind, s, cond)
	} else {
		v := reflect.ValueOf(i)
		if v.Kind() != reflect.Ptr {
			panic(StructError("structsql: Join needs a pointer receiver"))
		}
		v = v.Elem()
		ts := getStruct(v.Type())
		fmt.Fprintf(&q.query, " %s JOIN %s %s", kind, ts.t.name, cond)
		for _, c := range ts.cf {
			fmt.Fprintf(&q.fields, ",%s.%s", ts.t.name, c.name)
			q.res = append(q.res, c.h.right(v.FieldByIndex(c.byIndex).Addr()))
		}
	}
	return q
}

func (q *Query) String() string {
	if q.qs == "" {
		q.query.WriteTo(&q.fields)
		fmt.Fprint(&q.fields, ";")
		q.qs = q.fields.String()
	}
	return q.qs
}

// One returns the a single row filling in the fields
// to structs provided to Select, SelectCmd or Query.Join.
func (q *Query) One() error {
	// use Query instead of QueryRow here to allow caching of SQL
	rows, err := q.qr.Query(q.String(), q.args...)
	if !rows.Next() {
		return sql.ErrNoRows
	}
	err = rows.Scan(q.res...)
	rows.Close()
	return err
}

// Query returns a Rows object which scans into the fields
// to structs provided to Select, SelectCmd or Query.Join.
// It should be used in a loop to iterate through results:
//
//  var d Data
//  rows := Select(&d).Rows()
//  defer rows.Close()
//  for rows.Scan() {
//    // use data in d
//  }
//  err = rows.Err()
//  ...
func (q *Query) Rows() *Rows {
	rows, err := q.qr.Query(q.String(), q.args...)
	if err != nil {
		return &Rows{q, err, nil}
	}
	return &Rows{q, nil, rows}
}

type Rows struct {
	q   *Query
	err error
	r   *sql.Rows
}

// Scan scans the result query values into the structs
// provided to Select, SelectCmd or Query.Join.
//
// Results are overwritten on each call to Scan.
func (r *Rows) Scan() bool {
	if r.err != nil {
		return false
	}
	if r.r.Next() {
		if r.err = r.r.Scan(r.q.res...); r.err != nil {
			return false
		}
		return true
	}
	return false
}

// Err returns the error
// provided to Select, SelectCmd or Query.Join
func (r *Rows) Err() error {
	if r.err != nil {
		return r.err
	}
	return r.r.Err()
}

func (r *Rows) Close() error {
	if r.r != nil {
		return r.r.Close()
	}
	return nil
}
