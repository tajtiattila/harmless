package structsql

import (
	"bytes"
	"database/sql"
	"fmt"
	"reflect"
)

// Execer represents a type to that supports Exec,
// typically an sql.DB or sql.Tx.
type Execer interface {
	// Exec is expected to behave sql.DB.Exec and sql.Tx.Exec
	Exec(query string, args ...interface{}) (sql.Result, error)

	// Placeholder returns the string as ? or $1, $2...
	// used to bind arguments to
	Placeholder(i int) string
}

type Exec struct {
	er    Execer
	query bytes.Buffer
	args  []interface{}
	qs    string
	pk    *int64 // points to the primary key within struct on insert
}

// Run runs the Exec on an Execer it was created for.
func (e *Exec) Run() error {
	_, err := e.er.Exec(e.String(), e.args...)
	return err
}

// RunId works like Run but also returns the last insert id.
//
// If the struct the Exec was created for has
// the primary key colum mapped to a field, it will be written
// back to the structure.
//
// It is intended to be used with UPDATE statements.
func (e *Exec) RunId() (int64, error) {
	r, err := e.er.Exec(e.String(), e.args...)
	if err != nil {
		return 0, err
	}
	id, err := r.LastInsertId()
	if e.pk != nil {
		*e.pk = id
	}
	return id, err
}

// RunNum works like Run but also returns the number of rows affected.
//
// It is intended to be used with UPDATE statements.
func (e *Exec) RunNum() (int64, error) {
	r, err := e.er.Exec(e.String(), e.args...)
	if err != nil {
		return 0, err
	}
	return r.RowsAffected()
}

func (e *Exec) String() string {
	if e.qs == "" {
		e.query.WriteByte(';')
		e.qs = e.query.String()
	}
	return e.qs
}

// Add adds further clauses such as WHERE, GROUP BY, ORDER BY, LIMIT
// etc to the Exec. It has no effect after Run, RunId or RunNum
// has been called.
func (e *Exec) Add(query string, args ...interface{}) *Exec {
	if e.qs != "" {
		return e
	}
	fmt.Fprint(&e.query, " ", query)
	e.args = append(e.args, args...)
	return e
}

// InsertCmd creates an SQL Exec in the format of a typical
// SQL INSERT Statement for i to execute on er (DB or Tx).
//
// Cmd is the actual statement to execute, it can be INSERT,
// INSERT OR REPLACE, INSERT OR IGNORE etc. supported by the
// underlying SQL engine.
//
// It panics if i is not registered.
func InsertCmd(er Execer, cmd string, i interface{}) *Exec {
	v := reflect.ValueOf(i)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	ts := getStruct(v.Type())
	e := &Exec{er: er}
	if cmd == "" {
		cmd = "INSERT"
	}
	fmt.Fprintf(&e.query, "%s INTO %s", cmd, ts.t.name)
	sep := "("
	for _, c := range ts.cf {
		vc := v.FieldByIndex(c.byIndex)
		if !c.autoPriKey || vc.Int() > 0 {
			fmt.Fprintf(&e.query, "%s%s", sep, c.name)
			sep = ","
			e.args = append(e.args, c.h.arg(vc))
		} else {
			if vc.CanAddr() {
				e.pk = vc.Addr().Interface().(*int64)
			}
		}
	}
	sep = ") VALUES("
	for j := range e.args {
		fmt.Fprintf(&e.query, "%s%s", sep, er.Placeholder(j))
		sep = ","
	}
	e.query.WriteString(")")
	return e
}

func Insert(er Execer, i interface{}) *Exec {
	return InsertCmd(er, "INSERT", i)
}

// UpdateCmd creates an SQL Exec in the format of a typical
// SQL UPDATE Statement for i to execute on er (DB or Tx).
//
// Cmd is the actual statement to execute, it can be UPDATE,
// UPDATE OR REPLACE, UPDATE OR IGNORE etc. supported by the
// underlying SQL engine.
//
// It panics if i is not registered.
//
// The returned Exec should typically be extended
// with Add() to add any WHERE clauses necessary before calling Run.
func UpdateCmd(er Execer, cmd string, i interface{}) *Exec {
	v := reflect.ValueOf(i)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	ts := getStruct(v.Type())
	e := &Exec{er: er}
	fmt.Fprintf(&e.query, "%s %s", cmd, ts.t.name)
	sep := " SET "
	for i, c := range ts.cf {
		vc := v.FieldByIndex(c.byIndex)
		if !c.autoPriKey || vc.Int() > 0 {
			fmt.Fprintf(&e.query, "%s%s=%s", sep, c.name, er.Placeholder(i))
			sep = ", "
			e.args = append(e.args, c.h.arg(vc))
		}
	}
	return e
}

func Update(er Execer, i interface{}) *Exec {
	return UpdateCmd(er, "UPDATE", i)
}

// Delete creates an SQL Exec for a DELETE statement to execute on er (DB or Tx).
// There is little benefit of using it over Exec that of sql.DB or sql.Tx
// but is provided for symmetry.
func Delete(er Execer, i interface{}) *Exec {
	v := reflect.ValueOf(i)
	var name string
	switch v.Type().Kind() {
	case reflect.String:
		name = i.(string)
	case reflect.Ptr:
		v = v.Addr()
		fallthrough
	case reflect.Struct:
		ts := getStruct(v.Type())
		name = ts.t.name
	}
	e := &Exec{er: er}
	fmt.Fprintf(&e.query, "DELETE FROM %s", name)
	return e
}
