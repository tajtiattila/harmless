package structsql

import (
	"reflect"
	"sync"
)

var (
	structCacheLock sync.RWMutex
	structCache     = make(map[reflect.Type]*tabStruct)
)

type tabStruct struct {
	t  *Table
	cf []colField
}

func getStruct(t reflect.Type) *tabStruct {
	structCacheLock.RLock()
	ts := structCache[t]
	structCacheLock.RUnlock()
	if ts == nil {
		panic(StructError("Struct " + t.String() + " is not registered"))
	}
	return ts
}

type colField struct {
	name       string
	byIndex    []int
	autoPriKey bool

	h typeHandler
}

func cacheStruct(i interface{}, tab *Table) {
	t := reflect.TypeOf(i)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		panic(StructError("Table.Struct needs a structure"))
	}
	ts := &tabStruct{t: tab}
	ts.add(nil, t)

	dupnam := make(map[string]bool)
	for i := range ts.cf {
		c := &ts.cf[i]
		if dupnam[c.name] {
			panic(StructError("duplicate name '" + c.name + "' in Table.Struct"))
		}
		dupnam[c.name] = true
		if c.name == tab.autoPriKey {
			if t.FieldByIndex(c.byIndex).Type.Kind() != reflect.Int64 {
				panic(StructError("AutoPriKey field '" + c.name + "' must be an int64 in Table.Struct"))
			}
			c.autoPriKey = true
		}
	}

	structCacheLock.Lock()
	structCache[t] = ts
	structCacheLock.Unlock()
}

func (ts *tabStruct) add(bi []int, t reflect.Type) {
	for i := 0; i < t.NumField(); i++ {
		sf := t.Field(i)
		if h := newTypeHandler(sf.Type); h != nil {
			fbi := make([]int, len(bi)+1)
			copy(fbi, bi)
			fbi[len(fbi)-1] = i
			ts.cf = append(ts.cf, colField{name: NameToSql(sf.Name), byIndex: fbi, h: h})
		} else if sf.Anonymous && sf.Type.Kind() == reflect.Struct {
			fbi := make([]int, len(bi)+1)
			copy(fbi, bi)
			fbi[len(fbi)-1] = i
			ts.add(fbi, sf.Type)
		}
	}
}

// StructError represent an error in structure definitions.
// They are never returned as an error, it is used with panic.
type StructError string

func (s StructError) Error() string { return string(s) }

// NameToSql converts CamelCase to snake_case.
// Only ASCII strings are supported.
func NameToSql(s string) string {
	nuc := 0
	for i := 1; i < len(s); i++ {
		if 'A' <= s[i] && s[i] <= 'Z' {
			nuc++
		}
	}
	buf := make([]byte, len(s)+nuc)
	buf[0] = lcch(s[0])
	p := 1
	for i := 1; i < len(s); i++ {
		ch := lcch(s[i])
		if ch != s[i] {
			buf[p] = '_'
			p++
		}
		buf[p] = ch
		p++
	}
	return string(buf)
}

func lcch(ch byte) byte {
	if ch < 'A' || 'Z' < ch {
		return ch
	}
	return ch + 32
}
