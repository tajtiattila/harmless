package structsql

import (
	"database/sql/driver"
	"fmt"
	"reflect"
	"time"
)

// nullRecv creates a receiver to the element pointed to by v
// only the following types are supported:
//  bool
//  string
//  []byte
//  int64
//  float64
//  time.Time
func nullRecv(v reflect.Value) interface{} {
	switch v.Type().Elem().Kind() {
	case reflect.Bool:
		return &nullBool{v.Interface().(*bool)}
	case reflect.String:
		return &nullString{v.Interface().(*string)}
	case reflect.Int64:
		return &nullInt64{v.Interface().(*int64)}
	case reflect.Float64:
		return &nullFloat64{v.Interface().(*float64)}
	}
	if pt, ok := v.Interface().(*time.Time); ok {
		return &nullTime{pt}
	}
	// TODO: this will fail on unsupported types
	return v
}

// nullBool is used as a receiver for string fields on left joins
type nullBool struct {
	p *bool
}

func (r nullBool) Scan(v interface{}) error {
	switch v.(type) {
	case nil:
		return nil
	}
	v, err := driver.Bool.ConvertValue(v)
	if err == nil {
		*r.p = v.(bool)
	}
	return err
}

// nullString is used as a receiver for string fields on left joins
type nullString struct {
	p *string
}

func (r nullString) Scan(v interface{}) error {
	switch i := v.(type) {
	case nil:
		*r.p = ""
	case string:
		*r.p = i
	case []byte:
		*r.p = string(i)
	default:
		*r.p = fmt.Sprintf("%v", v)
	}
	return nil
}

// nullInt64 is used as a receiver for int64 fields on left joins
type nullInt64 struct {
	p *int64
}

func (r nullInt64) Scan(v interface{}) error {
	switch i := v.(type) {
	case nil:
		*r.p = 0
	case int64:
		*r.p = i
	default:
		return fmt.Errorf("structsql: unsupported value %v (type %T) converting to int64", v, v)
	}
	return nil
}

// nullFloat64 is used as a receiver for float64 fields on left joins
type nullFloat64 struct {
	p *float64
}

func (r nullFloat64) Scan(v interface{}) error {
	switch i := v.(type) {
	case nil:
		*r.p = 0
	case float64:
		*r.p = i
	default:
		return fmt.Errorf("structsql: unsupported value %v (type %T) converting to float64", v, v)
	}
	return nil
}

// nullTime is used as a receiver for time.Time fields on left joins
type nullTime struct {
	p *time.Time
}

func (r nullTime) Scan(v interface{}) error {
	switch i := v.(type) {
	case nil:
		*r.p = time.Time{}
	case time.Time:
		*r.p = i
	default:
		return fmt.Errorf("structsql: unsupported value %v (type %T) converting to time.Time", v, v)
	}
	return nil
}
