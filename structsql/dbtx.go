package structsql

import (
	"database/sql"
	"fmt"
	"sync"
)

type Interface interface {
	Select(vi ...interface{}) *Query
	Insert(i interface{}) *Exec
	Update(i interface{}) *Exec
	Delete(i interface{}) *Exec
}

// Db works like sql.Tx but adds Select, Insert, Update and Delete functions.
//
// It also adds caching for Query, QueryRow and Exec calls.
type DB struct {
	*sql.DB

	mtx   sync.RWMutex
	stmts map[string]*sql.Stmt

	PlaceholderFunc func(int) string
}

func Open(driver, dsn string) (*DB, error) {
	db, err := sql.Open(driver, dsn)
	if err != nil {
		return nil, err
	}
	var ph func(int) string
	if driver == "postgres" {
		ph = DollarIndex
	} else {
		ph = QuestionMark
	}
	return NewDB(db, ph), nil
}

func NewDB(db *sql.DB, placholderfn func(int) string) *DB {
	return &DB{
		DB:              db,
		stmts:           make(map[string]*sql.Stmt),
		PlaceholderFunc: placholderfn,
	}
}

func (db *DB) Placeholder(i int) string { return db.PlaceholderFunc(i) }

func (db *DB) Begin() (*Tx, error) {
	tx, err := db.DB.Begin()
	if err != nil {
		return nil, err
	}
	return &Tx{tx, db.PlaceholderFunc}, nil
}

func (db *DB) Close() error {
	for _, stmt := range db.stmts {
		stmt.Close()
	}
	return db.DB.Close()
}

func (db *DB) Select(vi ...interface{}) *Query { return Select(db, vi...) }
func (db *DB) Insert(i interface{}) *Exec      { return Insert(db, i) }
func (db *DB) Update(i interface{}) *Exec      { return Update(db, i) }
func (db *DB) Delete(i interface{}) *Exec      { return Delete(db, i) }

// Exec is like sql.DB.Exec but caches query strings
func (db *DB) Exec(query string, args ...interface{}) (sql.Result, error) {
	stmt, err := db.cache(query)
	if err != nil {
		return nil, err
	}
	return stmt.Exec(args)
}

// Query is like sql.DB.Query but caches query strings
func (db *DB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	stmt, err := db.cache(query)
	if err != nil {
		return nil, err
	}
	return stmt.Query(args...)
}

func (db *DB) cache(query string) (stmt *sql.Stmt, err error) {
	var ok bool
	db.mtx.RLock()
	stmt, ok = db.stmts[query]
	db.mtx.RUnlock()

	if !ok {
		db.mtx.Lock()
		defer db.mtx.Unlock()
		stmt, ok = db.stmts[query] // meanwhile someone may have created it
		if !ok {
			stmt, err = db.DB.Prepare(query)
			if err == nil {
				db.stmts[query] = stmt
			}
		}
	}
	return
}

// Tx works like sql.Tx but adds Select, Insert, Update and Delete functions.
type Tx struct {
	*sql.Tx
	PlaceholderFunc func(int) string
}

func (t *Tx) Placeholder(i int) string { return t.PlaceholderFunc(i) }

func (t *Tx) Select(vi ...interface{}) *Query { return Select(t, vi...) }
func (t *Tx) Insert(i interface{}) *Exec      { return Insert(t, i) }
func (t *Tx) Update(i interface{}) *Exec      { return Update(t, i) }
func (t *Tx) Delete(i interface{}) *Exec      { return Delete(t, i) }

// QuestionMark is a Placeholder function returning "?"
func QuestionMark(i int) string { return "?" }

// DollarIndex is a Placeholder function returning "$1", "$2"...
func DollarIndex(i int) string { return fmt.Sprint("$", i+1) }
