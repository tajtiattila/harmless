package structsql

type Table struct {
	name       string // Table name
	autoPriKey string // integer auto-increment primary, if any
}

// NewTable registers a SQL table to use with this package.
func NewTable(name string) *Table {
	return &Table{name: name}
}

// AutoPriKey sets the autoincrement primary key column name for the Table.
//
// Structs mapped to this Table having the AutoPriKey field
// must be an int64. During an insert a positive value
// used as is, otherwise it will be set automatically.
func (t *Table) AutoPriKey(n string) *Table {
	t.autoPriKey = n
	return t
}

// Struct registers a struct to use with the Table.
//
// Multiple structs may be registered to the same Table.
//
// Exported names of i will be mapped to the columns of the table.
// The Go field name SomeFieldName uses the column some_field_name
// in SQL (CamelCase → snake_case)
//
// Anonymous struct fields within i are processed recursively
// as if they fields if i.
func (t *Table) Struct(i interface{}) *Table {
	cacheStruct(i, t)
	return t
}
