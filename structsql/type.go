package structsql

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"reflect"
	"time"
)

type typeHandler interface {
	// Arg creates an argument for sql.Query or sql.Exec for a field in v
	arg(v reflect.Value) interface{}

	// Recv creates a receiver for sql.Rows.Scan for a field in v,
	// which must be a pointer.
	recv(v reflect.Value) interface{}

	// Right creates a receiver for sql.Rows.Scan for a field in v,
	// which must be a pointer. It used within joins, therefore
	// should always accept NULL values.
	right(v reflect.Value) interface{}
}

// newHandler returns a new handler for t, or nil if it cannot be handled.
func newTypeHandler(t reflect.Type) typeHandler {
	switch t.Kind() {
	case reflect.Bool,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Uintptr,
		reflect.Float32,
		reflect.Float64,
		reflect.String:
		return &selfHandler{}
	case reflect.Slice:
		// []byte, sql.RawBytes
		if t.Elem().Kind() == reflect.Uint8 {
			return &selfHandler{}
		}
	case reflect.Ptr:
		switch t.Elem().Kind() {
		case reflect.Bool:
			return &boolPtrHandler{}
		case reflect.Int,
			reflect.Int8,
			reflect.Int16,
			reflect.Int32,
			reflect.Int64:
			return &intPtrHandler{}
		case reflect.Float32,
			reflect.Float64:
			return &floatPtrHandler{}
		case reflect.String:
			return &stringPtrHandler{}
		}
	}

	p := reflect.New(t)
	if _, ok := p.Elem().Interface().(time.Time); ok {
		// time.Time
		return &selfHandler{}
	}

	_, argok := p.Elem().Interface().(driver.Valuer)
	_, recvok := p.Interface().(sql.Scanner)
	if argok && recvok {
		// a type that can both Scan and Value itself
		return &selfHandler{}
	}

	return nil
}

// selfHandler represents simple types that can be used directly with database/sql
type selfHandler struct{}

func (*selfHandler) arg(v reflect.Value) interface{}   { return v.Interface() }
func (*selfHandler) recv(v reflect.Value) interface{}  { return v.Interface() }
func (*selfHandler) right(v reflect.Value) interface{} { return nullRecv(v) }

// ptrHandler is a handler for pointer types, fields in SQL that may be NULL
type ptrHandler struct{}

func (*ptrHandler) arg(v reflect.Value) interface{} {
	if v.IsNil() {
		return nil
	}
	return v.Elem().Interface()
}

// intPtrHandler is a handler for *int,
// an INTEGER field in SQL that may be NULL
type intPtrHandler struct {
	ptrHandler
}

func (*intPtrHandler) recv(v reflect.Value) interface{}    { return intPtrRecv{v} }
func (h *intPtrHandler) right(v reflect.Value) interface{} { return h.recv(v) }

type intPtrRecv struct {
	v reflect.Value
}

func (r intPtrRecv) Scan(src interface{}) error {
	switch i := src.(type) {
	case int64:
		r.v.Elem().SetInt(i)
	case nil:
		r.v.Set(reflect.Zero(r.v.Type()))
	case float64:
		r.v.Elem().SetInt(int64(i))
	default:
		return fmt.Errorf("Cannot scan %#v into *int", src)
	}
	return nil
}

// floatPtrHandler is a handler for *int,
// an REAL/NUMERIC field in SQL that may be NULL
type floatPtrHandler struct {
	ptrHandler
}

func (*floatPtrHandler) recv(v reflect.Value) interface{}    { return &floatPtrRecv{v} }
func (h *floatPtrHandler) right(v reflect.Value) interface{} { return h.recv(v) }

type floatPtrRecv struct {
	v reflect.Value
}

func (r floatPtrRecv) Scan(src interface{}) error {
	switch i := src.(type) {
	case float64:
		r.v.Elem().SetFloat(i)
	case nil:
		r.v.Set(reflect.Zero(r.v.Type()))
	case int64:
		r.v.Elem().SetFloat(float64(i))
	default:
		return fmt.Errorf("Cannot scan %#v into *float", src)
	}
	return nil
}

// stringPtrHandler is a handler for *string,
// a CHAR/VARCHAR/TEXT field in SQL that may be NULL
type stringPtrHandler struct {
	ptrHandler
}

func (*stringPtrHandler) recv(v reflect.Value) interface{} {
	return stringPtrRecv{v.Interface().(**string)}
}
func (h *stringPtrHandler) right(v reflect.Value) interface{} {
	return h.recv(v)
}

type stringPtrRecv struct {
	p **string
}

func (r stringPtrRecv) Scan(src interface{}) error {
	switch i := src.(type) {
	case string:
		*r.p = &i
	case nil:
		*r.p = nil
	default:
		s := fmt.Sprint(src)
		*r.p = &s
	}
	return nil
}

// boolPtrHandler is a handler for *bool, a BOOL field in SQL that may be NULL
type boolPtrHandler struct {
	ptrHandler
}

func (*boolPtrHandler) recv(v reflect.Value) interface{} {
	return boolPtrRecv{v.Interface().(**bool)}
}
func (h *boolPtrHandler) right(v reflect.Value) interface{} { return h.recv(v) }

type boolPtrRecv struct {
	p **bool
}

func (r boolPtrRecv) Scan(src interface{}) error {
	switch i := src.(type) {
	case bool:
		*r.p = pbool(i)
	case nil:
		*r.p = nil
	case int64:
		*r.p = pbool(i != 0)
	case float64:
		*r.p = pbool(i != 0)
	default:
		return fmt.Errorf("Cannot scan %#v into *bool", src)
	}
	return nil
}

func pbool(i bool) *bool {
	if i {
		return ptrue
	}
	return pfalse
}

var ptrue, pfalse *bool

func init() {
	btrue, bfalse := true, false
	ptrue, pfalse = &btrue, &bfalse
}
