package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/tajtiattila/harmless/edsc"
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/name"
	"bitbucket.org/tajtiattila/harmless/space"
	"bitbucket.org/tajtiattila/harmless/storage"
	"bitbucket.org/tajtiattila/harmless/storage/edit"
)

func main() {
	var (
		srcurl   string
		editurl  string
		showtext bool
		showjson bool
		del      bool
	)
	flag.StringVar(&srcurl, "src", "http://starchart.club/elite.json", "base data source url")
	flag.StringVar(&editurl, "post", "", "post detected changes to this url")
	flag.BoolVar(&showjson, "json", false, "show detected changes in json format")
	flag.BoolVar(&showtext, "print", false, "show detected changes in plain text")
	flag.BoolVar(&del, "del", false, "process delete ops (default: skip)")
	flag.Parse()

	tgc, err := edsc.Open("cache/tgc.gob")
	verify(err)
	defer tgc.Close()

	if len(tgc.Data) == 0 {
		verify(tgc.Import("data/tgcsystems.json"))
	}
	verify(tgc.Fetch())

	scd, err := scdata(srcurl)
	verify(err)

	tgcd := make([]*edsc.System, 0, len(tgc.Data))
	for _, s := range tgc.Data {
		if s != nil && s.P != nil {
			tgcd = append(tgcd, s)
		}
	}

	fmt.Println("TGC:", len(tgcd))
	fmt.Println("SC:", len(scd))

	m := make(i3map)
	for _, s := range tgcd {
		xs := m.at(gmap.Vec3(*s.P))
		xs.esys = append(xs.esys, s)
	}

	for _, s := range scd {
		xs := m.at(s.P)
		xs.gsys = s
	}

	var v vxsys
	for _, xsys := range m {
		xsys.clean()
		v = append(v, *xsys)
	}

	spc := space.New(v)

	editor := &Editor{showtext: showtext, del: del}

	fmt.Println("Differences:")

	const (
		maxmove = 1.0 // Ly
	)

	// Moves must be detected first, process
	// them together with removals now.
	for _, e := range v {
		if m[e.p] == nil {
			continue // already used in move
		}
		if e.gsys != nil && len(e.esys) == 0 {
			var vnear []*xsys
			spc.Sphere(e.p, maxmove, func(d interface{}, dist float64) {
				f := d.(*xsys)
				if f.p == e.p || f.gsys != nil {
					return
				}
				vnear = append(vnear, f)
			})

			p := e.p.Vec3()

			switch len(vnear) {
			case 0:
				editor.Delete(p, e.gsys)
				delete(m, e.p)
			case 1:
				f := vnear[0]
				if f.gsys == nil && len(f.esys) == 1 && fixname(e.gsys.Name) == fixname(f.esys[0].Name) {
					editor.Move(EdscAuthor(f.esys[0]), p, f.p.Vec3(), e.gsys)
					delete(m, e.p)
					delete(m, f.p)
				}
			default:
				// TODO
			}
		}
	}

	// recreate space
	v = v[:0]
	for _, xsys := range m {
		v = append(v, *xsys)
	}

	spc = space.New(v)

	// Process other changes: additions and renames.
	for _, e := range v {
		p := e.p.Vec3()
		ps := fmt.Sprintf("(%v;%v;%v)", p[0], p[1], p[2])
		if e.gsys != nil && len(e.esys) == 1 {
			sn, en := e.gsys.Name, fixname(e.esys[0].Name)
			if sn != en {
				editor.Rename(EdscAuthor(e.esys[0]), p, sn, en)
			}
			continue
		}
		if e.gsys == nil && len(e.esys) == 1 {
			editor.Add(EdscAuthor(e.esys[0]), p, e.esys[0].Name)
			continue
		}
		fmt.Printf("?    %s ", ps)
		if e.gsys == nil {
			fmt.Println("<nil>")
		} else {
			fmt.Printf("%q\n", e.gsys.Name)
		}
		for _, esys := range e.esys {
			fmt.Printf("    %v\n", esys)
		}
		if e.gsys != nil {
			spc.Sphere(e.p, 1.0, func(d interface{}, dist float64) {
				f := d.(*xsys)
				if f.p == e.p || f.gsys != nil {
					return
				}
				for _, esys := range f.esys {
					fmt.Printf("      near %v\n", esys)
				}
			})
		}
	}

	if showjson {
		fmt.Println("JSON:")
		editor.ShowJson()
	}

	if editurl != "" {
		editor.Post(editurl)
	}
}

func fixname(s string) string {
	// remove part with braces at the end
	ob, cb := -1, -1
	for i, r := range s {
		switch r {
		case '(':
			ob = i
		case ')':
			cb = i
		}
	}
	if ob != -1 && cb != -1 && ob < cb && cb == len(s)-1 {
		s = strings.TrimSpace(s[:ob])
	}

	return name.Name(s)
}

type i3map map[gmap.Vec3i]*xsys

func (m i3map) at(p gmap.Vec3) *xsys {
	pi := p.Vec3i()
	e := m[pi]
	if e == nil {
		e = &xsys{p: pi}
		m[pi] = e
	}
	return e
}

type xsys struct {
	p    gmap.Vec3i
	gsys *gmap.System
	esys []*edsc.System
}

func (x *xsys) clean() {
	if len(x.esys) < 2 {
		return
	}
	m := make(map[string]bool)
	for _, e := range x.esys {
		m[fixname(e.Name)] = true
	}
	if len(m) == 1 {
		x.esys = x.esys[:1]
	}
}

func (x xsys) EdscName() string {
	return fixname(x.esys[0].Name)
}

func EdscAuthor(s *edsc.System) Author {
	return Author{
		Cmdr: s.Updater,
		Time: time.Time(s.Updated).UTC(),
	}
}

type vxsys []xsys

func (v vxsys) Len() int             { return len(v) }
func (v vxsys) P(i int) gmap.Vec3i   { return v[i].p }
func (v vxsys) At(i int) interface{} { return &v[i] }

type scd struct {
	Systems []*gmap.System `json:"systems"`
}

func scdata(srcurl string) ([]*gmap.System, error) {
	r, err := http.Get(srcurl)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	var d scd
	if err = json.NewDecoder(r.Body).Decode(&d); err != nil {
		return nil, err
	}
	return d.Systems, nil
}

func verify(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func pstr(p gmap.Vec3) string {
	return fmt.Sprintf("(%v;%v;%v)", p[0], p[1], p[2])
}

type Author struct {
	Cmdr string
	Time time.Time
}

type Editor struct {
	showtext bool
	del      bool

	edits []storage.Edit
}

func (e *Editor) Add(a Author, p gmap.Vec3, name string) {
	name = fixname(name)
	if e.showtext {
		fmt.Printf("ADD  %s %q\n", pstr(p), name)
	}
	e.edit(a, storage.Add, p, &gmap.System{Name: name, Valid: ptrue})
}

func (e *Editor) Rename(a Author, p gmap.Vec3, oldn, newn string) {
	if e.showtext {
		var op string
		if strings.ToLower(oldn) == strings.ToLower(newn) {
			op = "CASE"
		} else {
			op = "REN"
		}
		fmt.Printf("%-4s %s %q → %q\n", op, pstr(p), oldn, newn)
	}
	e.edit(a, storage.Rename, p, &gmap.System{Name: newn, Valid: ptrue})
}

func (e *Editor) Move(a Author, oldp, newp gmap.Vec3, sys *gmap.System) {
	if e.showtext {
		fmt.Printf("MOVE %s %q → %s\n", pstr(oldp), sys.Name, pstr(newp))
	}
	e.edit(a, storage.Move, oldp, &gmap.System{P: newp, Valid: ptrue})
}

func (e *Editor) Delete(p gmap.Vec3, oldsys *gmap.System) {
	if e.showtext {
		fmt.Printf("DEL  %s %q\n", pstr(p), oldsys.Name)
	}
	if e.del {
		e.edit(Author{}, storage.Delete, p, nil)
	}
}

func (e *Editor) edit(a Author, op storage.Op, key gmap.Vec3, sys *gmap.System) {
	if sys != nil {
		sys.Cmdr, sys.Modified = a.Cmdr, a.Time
		if op != storage.Move {
			sys.P = key
		}
	}
	entry := storage.Edit{op, key, sys}

	// check if marshal works
	_, err := json.Marshal(entry)
	verify(err)

	e.edits = append(e.edits, entry)
}

func (e *Editor) ShowJson() {
	p, err := json.MarshalIndent(e.edits, "", "  ")
	verify(err)
	_, err = os.Stdout.Write(p)
	verify(err)
}

func (e *Editor) Post(url string) {
	verify(edit.Do(url, "EDSC", e.edits...))
}

var (
	tru   = true
	ptrue = &tru
)
