package edsc

import (
	"time"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

func (s System) System() *gmap.System {
	if s.P == nil {
		return nil
	}
	return &gmap.System{
		Name:     s.Name,
		P:        gmap.Vec3(*s.P),
		Cmdr:     s.Updater,
		Modified: time.Time(s.Updated),
	}
}
