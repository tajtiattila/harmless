package edsc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

type OutputMode int

const (
	NameOnly OutputMode = 1
	Detailed OutputMode = 2
)

type KnownStatus int

const (
	AllSystems           KnownStatus = 0
	SystemsWithCoords    KnownStatus = 1
	SystemsWithoutCoords KnownStatus = 2
)

const (
	APIBase    = "http://edstarcoordinator.com/api.asmx"
	APIVer     = 2.0
	TimeFormat = "2006-01-02 15:04:05"
)

var StartTime = time.Date(2014, 11, 25, 17, 25, 11, 0, time.UTC)

type Time time.Time

func (t Time) MarshalJSON() ([]byte, error) {
	s := time.Time(t).UTC().Format(TimeFormat)
	return json.Marshal(s)
}

func (t *Time) UnmarshalJSON(p []byte) error {
	var s string
	if err := json.Unmarshal(p, &s); err != nil {
		return err
	}
	t0, err := time.ParseInLocation(TimeFormat, s, time.UTC)
	*t = Time(t0)
	return err
}

type Query struct {
	Ver        float64    `json:"ver"` // api version
	Test       bool       `json:"test,omitempty"`
	OutputMode OutputMode `json:"outputmode,omitempty"`
	Filter     *Filter    `json:"filter,omitempty"`
}

type Filter struct {
	KnownStatus KnownStatus `json:"knownstatus,omitempty"`

	Name   string       `json:"systemname,omitempty"`
	CR     int          `json:"cr,omitempty"`        // confidence rating, default: 5
	After  Time         `json:"date,omitempty"`      // list only additons after date, default: now-24h
	Box    [][2]float64 `json:"coordcube,omitempty"` // [xmin,xmax] [ymin,ymax] [zmin,zmax]
	Sphere *Sphere      `json:"coordsphere,omitempty"`
}

type Sphere struct {
	C [3]float64 `json:"origin"` // centre
	R float64    `json:"radius"` // radius
}

type System struct {
	Id   int    `json:"id"`
	Name string `json:"name"`

	P       *[3]float64 `json:"coord"`
	CR      int         `json:"cr"`
	Creator string      `json:"commandercreate"`
	Created time.Time   `json:"createdate"`
	Updater string      `json:"commanderupdate"`
	Updated time.Time   `json:"updatedate"`
}

func (s *System) UnmarshalJSON(p []byte) error {
	var rs rawsys
	if err := json.Unmarshal(p, &rs); err != nil {
		return err
	}
	s.Id = rs.Id
	s.Name = rs.Name
	if p := rs.P; p[0] != nil && p[1] != nil && p[2] != nil {
		q := [3]float64{*p[0], *p[1], *p[2]}

		s.P = &q
	}
	s.CR = rs.CR

	s.Creator = rs.Creator
	s.Created, _ = time.Parse(TimeFormat, rs.Created)
	s.Updater = rs.Creator
	s.Updated, _ = time.Parse(TimeFormat, rs.Updated)

	return nil
}

func (s System) String() string {
	if s.Id == 0 {
		return s.Name
	}
	var ps string
	if p := s.P; p != nil {
		ps = fmt.Sprintf("%v:%v:%v", p[0], p[1], p[2])
	} else {
		ps = "nil"
	}
	return fmt.Sprintf(
		"%s #%d @%d %s %v",
		s.Name,
		s.Id,
		s.CR,
		ps,
		time.Time(s.Updated).Format(time.RFC3339),
	)
}

type Error struct {
	Code int    `json:"statusnum"`
	Msg  string `json:"msg"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("EDSC(%d): %s", e.Code, e.Msg)
}

const (
	CodeInvalid = -iota
	CodeNoData
	CodeNoSystems
)

var (
	ErrInternal  = &Error{CodeInvalid, "internal error"}
	ErrNoData    = &Error{CodeNoData, "no data object in response"}
	ErrNoSystems = &Error{CodeNoSystems, "no systems in response"}
)

func GetSystems(q *Query) ([]System, error) {
	qd := &sysQuery{Data: q}
	qdata, err := json.Marshal(qd)
	if err != nil {
		return nil, err
	}

	fmt.Println(string(qdata))
	r, err := http.Post(APIBase+"/GetSystems", "application/json; charset=utf-8", bytes.NewBuffer(qdata))
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	var sr sysResp
	if err = json.NewDecoder(r.Body).Decode(&sr); err != nil {
		return nil, err
	}

	sd := sr.Data
	if sd == nil {
		if sr.Message != "" {
			return nil, ErrInternal
		} else {
			return nil, ErrNoData
		}
	}

	return Decode(bytes.NewReader(sd))
}

func Decode(r io.Reader) ([]System, error) {
	var sd sysRespData
	err := json.NewDecoder(r).Decode(&sd)
	if err != nil {
		return nil, err
	}

	var st *Error
	if len(sd.Status.Input) != 0 {
		st = sd.Status.Input[0].Status
	}
	fmt.Println(st)
	switch {
	case st == nil:
		log.Println(`EDSC: invalid status in response`)
		if len(sd.Systems) == 0 {
			return nil, ErrNoSystems
		}
	case st.Code != 0:
		return nil, st
	}

	return sd.Systems, nil
}

type rawsys struct {
	Name string `json:"name"`

	Id      int         `json:"id"`
	P       [3]*float64 `json:"coord"`
	CR      int         `json:"cr"`
	Creator string      `json:"commandercreate"`
	Created string      `json:"createdate"`
	Updater string      `json:"commanderupdate"`
	Updated string      `json:"updatedate"`
}

func GetSystemsSince(since time.Time, om OutputMode) ([]System, error) {
	return GetSystems(&Query{
		Ver:        APIVer,
		OutputMode: om,
		Filter: &Filter{
			KnownStatus: AllSystems,
			After:       Time(since),
			CR:          1,
		},
	})
}

type sysQuery struct {
	Data *Query `json:"data"`
}

type sysResp struct {
	Data json.RawMessage `json:"d"`

	Message       string
	StackTrace    string
	ExceptionType string
}

type sysRespData struct {
	Ver    float64     `json:"ver"`
	Date   Time        `json:"date"`
	Status respStatObj `json:"status"`

	Systems []System `json:"systems"`
}

type respStatObj struct {
	Input []respStatInp `json:"input"`
}

type respStatInp struct {
	Status *Error `json:"status"`
}
