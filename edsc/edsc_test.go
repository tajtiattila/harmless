package edsc

import (
	"bytes"
	"encoding/json"
	"net/http"
	"testing"
	"time"
)

func jsonreq(js string) *http.Request {
	req, err := http.NewRequest(
		"POST",
		"http://edstarcoordinator.com/api.asmx/GetSystems",
		bytes.NewBufferString(js))
	if err != nil {
		panic(err)
	}
	req.Header.Add("Content-Type", "application/json; charset=utf-8")
	return req
}

func TestDummy(t *testing.T) {
	const q = `{"data":{"ver":2,"outputmode":2,"filter":{"date":"2014-12-01"}}}`
	var d map[string]interface{}
	if err := json.Unmarshal([]byte(q), &d); err != nil {
		t.Fatal(err)
	}
	breq := new(bytes.Buffer)
	jsonreq(q).Write(breq)
	t.Log(breq.String())
	client := new(http.Client)
	r, err := client.Do(jsonreq(q))
	if err != nil {
		t.Fatal(err)
	}
	defer r.Body.Close()
	b := new(bytes.Buffer)
	b.ReadFrom(r.Body)
	t.Log(b.String())
}

func TestEdsc(t *testing.T) {
	t0 := StartTime.Add(time.Minute)
	sr, err := GetSystemsSince(t0, Detailed)
	if err != nil {
		t.Error(err)
	}
	for _, es := range sr {
		t.Log(es.String())
	}
}
