package edsc

import (
	"encoding/gob"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

type Cache struct {
	Data    []*System
	ModTime time.Time

	LastFetch time.Time

	fn, tn string
}

func Open(fn string) (*Cache, error) {
	an, err := filepath.Abs(fn)
	if err != nil {
		return nil, err
	}
	c := &Cache{fn: an, tn: an + "~"}
	if err = c.load(); err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Cache) Close() error {
	return nil
}

func (c *Cache) Import(fn string) error {
	f, err := os.Open(fn)
	if err != nil {
		return err
	}
	defer f.Close()
	return c.Read(f)
}

func (c *Cache) Read(r io.Reader) error {
	vsys, err := Decode(r)
	if err != nil {
		return err
	}
	c.update(vsys)
	return nil
}

func (c *Cache) Fetch() error {
	t := time.Now()
	if t.Before(c.LastFetch.Add(15 * time.Minute)) {
		return nil
	}
	fmt.Fprintln(os.Stderr, "Fetching from EDSC")
	vsys, err := GetSystems(&Query{
		Ver:        APIVer,
		OutputMode: Detailed,
		Filter: &Filter{
			KnownStatus: AllSystems,
			After:       Time(c.ModTime),
			CR:          1,
		},
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cache.Fetch: %v\n", err)
		return err
	}
	c.LastFetch = t
	c.update(vsys)
	return nil
}

func (c *Cache) update(vsys []System) {
	idmax := 0
	for _, sys := range vsys {
		if sys.Id >= idmax {
			idmax = sys.Id + 1
		}
	}
	if len(c.Data) < idmax {
		nd := make([]*System, idmax+1024)
		copy(nd, c.Data)
		c.Data = nd
	}
	for i := range vsys {
		sys := &vsys[i]
		c.Data[sys.Id] = sys
		if sys.Updated.After(c.ModTime) {
			c.ModTime = sys.Updated.Add(time.Second)
		}
	}
	c.save()
}

type cacheData struct {
	Data    []*System
	ModTime time.Time

	LastFetch time.Time
}

func (c *Cache) load() error {
	f, err := os.Open(c.fn)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer f.Close()

	var cd cacheData
	if err = gob.NewDecoder(f).Decode(&cd); err != nil {
		return err
	}

	if len(cd.Data) != 0 {
		c.Data = make([]*System, cd.Data[len(cd.Data)-1].Id+1)
		for _, d := range cd.Data {
			c.Data[d.Id] = d
		}
	} else {
		c.Data = nil
	}
	c.ModTime = cd.ModTime
	c.LastFetch = cd.LastFetch
	return nil
}

func (c *Cache) save() error {
	if err := c.savedata(); err != nil {
		fmt.Fprintf(os.Stderr, "Cache.save (write): %v\n", err)
		return err
	}
	if err := os.Remove(c.fn); err != nil && !os.IsNotExist(err) {
		fmt.Fprintf(os.Stderr, "Cache.save (remove): %v\n", err)
		return err
	}
	err := os.Rename(c.tn, c.fn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cache.save (rename): %v\n", err)
	}
	return err
}

func (c *Cache) savedata() error {
	f, err := os.Create(c.tn)
	if err != nil {
		return nil
	}
	defer f.Close()

	var cd cacheData
	cd.Data = make([]*System, 0, len(c.Data))
	for _, d := range c.Data {
		if d != nil {
			cd.Data = append(cd.Data, d)
		}
	}
	cd.ModTime = c.ModTime
	cd.LastFetch = c.LastFetch
	return gob.NewEncoder(f).Encode(cd)
}
