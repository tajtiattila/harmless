package main

import (
	"fmt"
	"html/template"
	"math"
	"os"
	"time"
)

type TmplDir struct {
	path  string
	names []string
	funcs map[string]interface{}

	T *template.Template

	lastModTime time.Time
}

type FuncMap map[string]interface{}

func NewTmplDir(path string, names ...string) *TmplDir {
	return &TmplDir{path: path, names: names}
}

func (td *TmplDir) Load() error {
	fi, err := os.Stat(td.path)
	if err != nil {
		return err
	}
	return td.load(fi)
}

func (td *TmplDir) Funcs(funcmap FuncMap) {
	if td.funcs == nil {
		td.funcs = funcmap
	} else {
		for k, v := range funcmap {
			td.funcs[k] = v
		}
	}
	for k, v := range td.funcs {
		if v == nil {
			delete(td.funcs, k)
		}
	}
}

func (td *TmplDir) Lookup(name string) *template.Template {
	return td.T.Lookup(name)
}

func (td *TmplDir) Update() error {
	fi, err := os.Stat(td.path)
	if err == nil && fi.ModTime().After(td.lastModTime) {
		return td.load(fi)
	}
	return err
}

func (td *TmplDir) load(fi os.FileInfo) error {
	tmpl := template.New(td.path).Funcs(td.funcs)
	if _, err := tmpl.ParseGlob(td.path + "/*"); err != nil {
		return err
	}
	for _, n := range td.names {
		if tmpl.Lookup(n) == nil {
			return fmt.Errorf("required template '%s' is missing", n)
		}
	}
	td.T = tmpl
	td.lastModTime = fi.ModTime()
	return nil
}

var MathFuncs = FuncMap{
	"neg": func(a0 interface{}) (float64, error) {
		var f F64Conv
		a := f.Arg(a0)
		return -a, f.Err
	},
	"mul": func(a0, b0 interface{}) (float64, error) {
		var f F64Conv
		a, b := f.Arg(a0), f.Arg(b0)
		return a * b, f.Err
	},
	"div": func(a0, b0 interface{}) (float64, error) {
		var f F64Conv
		a, b := f.Arg(a0), f.Arg(b0)
		return a / b, f.Err
	},
	"add": func(a0, b0 interface{}) (float64, error) {
		var f F64Conv
		a, b := f.Arg(a0), f.Arg(b0)
		return a + b, f.Err
	},
	"sub": func(a0, b0 interface{}) (float64, error) {
		var f F64Conv
		a, b := f.Arg(a0), f.Arg(b0)
		return a - b, f.Err
	},
	"pow": func(a0, b0 interface{}) (float64, error) {
		var f F64Conv
		a, b := f.Arg(a0), f.Arg(b0)
		return math.Pow(a, b), f.Err
	},
}

type F64Conv struct {
	Err error
}

func (f *F64Conv) Arg(a interface{}) float64 {
	switch a := a.(type) {
	case float64:
		return a
	case float32:
		return float64(a)
	case int:
		return float64(a)
	case int8:
		return float64(a)
	case int16:
		return float64(a)
	case int32:
		return float64(a)
	case int64:
		return float64(a)
	case uint:
		return float64(a)
	case uint8:
		return float64(a)
	case uint16:
		return float64(a)
	case uint32:
		return float64(a)
	case uint64:
		return float64(a)
	case bool:
		if a {
			return 1
		} else {
			return 0
		}
	}
	/*
		v := reflect.ValueOf(a)
		switch v.Kind() {
		case reflect.Bool:
			if v.Bool() {
				return 1
			} else {
				return 0
			}
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			return float64(v.Int())
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
			return float64(v.Uint())
		case reflect.Float32, reflect.Float64:
			return v.Float())
		}
	*/
	f.Err = fmt.Errorf("expected number, got %v", a)
	return 0
}
