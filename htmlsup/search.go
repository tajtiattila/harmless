package htmlsup

import (
	"sort"
	"unicode"
	"unicode/utf8"

	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/storage"
)

type SearchResult struct {
	Label string `json:"label"`
	Value srev   `json:"value"`
}

type srev struct {
	*gmap.System
	Dist float64 `json:"dist"`
}

func SystemSearch(stor storage.Db, query string, distf func(gmap.Vec3) float64) ([]SearchResult, error) {
	vsys, err := stor.Search(query)
	if err != nil {
		return nil, err
	}
	var v []SearchResult
	for _, s := range vsys {
		var (
			d float64
		)
		if distf != nil {
			d = trunc3(distf(s.P))
		}
		v = append(v, SearchResult{
			Label: s.Name,
			Value: srev{
				System: s,
				Dist:   d,
			},
		})
	}
	sort.Sort(distsort(v))
	if len(v) > 20 {
		v = v[:20]
	}
	return v, nil
}

func lowercaseless(a, b string) bool {
	for a != "" && b != "" {
		ar, as := utf8.DecodeRuneInString(a)
		br, bs := utf8.DecodeRuneInString(b)
		al, bl := unicode.ToLower(ar), unicode.ToLower(br)
		if al != bl {
			return al < bl
		}
		a, b = a[as:], b[bs:]
	}
	return len(a) < len(b)
}

type distsort []SearchResult

func (v distsort) Len() int      { return len(v) }
func (v distsort) Swap(i, j int) { v[i], v[j] = v[j], v[i] }

func (v distsort) Less(i, j int) bool {
	f := v[i].Value.Dist - v[j].Value.Dist
	switch {
	case f < 0:
		return true
	case f > 0:
		return false
	}
	return lowercaseless(v[i].Label, v[j].Label)
}

func trunc3(v float64) float64 {
	return float64(int(v*1000+0.5)) / 1000
}
