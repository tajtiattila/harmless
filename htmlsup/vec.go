package htmlsup

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

const CoordSep = ",;" // allowed separators for coordinates within a vector

func ParseVec3(s string) (c gmap.Vec3, err error) {
	var i, j int
	i = strings.IndexAny(s, CoordSep)
	if i != -1 {
		j = strings.IndexAny(s[i+1:], CoordSep)
		if j != -1 {
			j += i + 1
		}
	}
	if i == -1 || j == -1 {
		return gmap.Vec3{}, fmt.Errorf("can't parse coordinates %q", s)
	}
	c[0], err = strconv.ParseFloat(s[:i], 64)
	if err != nil {
		return
	}
	c[1], err = strconv.ParseFloat(s[i+1:j], 64)
	if err != nil {
		return
	}
	c[2], err = strconv.ParseFloat(s[j+1:], 64)
	if err != nil {
		return
	}
	// fix position to 1/32 Ly grid
	return c.Vec3i().Vec3(), nil
}
