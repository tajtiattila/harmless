package main

import (
	"sync"

	"bitbucket.org/tajtiattila/harmless/packed"
	"bitbucket.org/tajtiattila/harmless/packed/clustermap"
)

var (
	SpaceMtx sync.RWMutex
	Space    *packed.Space
	Cluster  clustermap.Cache
)

func LoadSpace() error {
	space, err := packed.NewSpace(Stor)
	if err != nil {
		return err
	}

	SpaceMtx.Lock()
	defer SpaceMtx.Unlock()

	Space = space

	Cluster.SetSpace(space)

	return nil
}

func GetSpace() *packed.Space {
	SpaceMtx.RLock()
	defer SpaceMtx.RUnlock()

	return Space
}
