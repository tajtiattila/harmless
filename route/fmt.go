package route

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	"fmt"
	"io"
	"os"
)

// Format returns route r as string. The jumpmark is used between
// stops, refuelmark is after stops where refueling is necessary. The very
// first and very last points are shown if ends is true, and hidden otherwise.
func Format(r Route, jumpmark, refuelmark string, ends bool) string {
	s, lastb := "", ""
	r.Visit(func(a, b *gmap.System, refuel bool) {
		s += lastb
		if s == "" && ends {
			s = a.Name
		}
		if refuel {
			s += refuelmark
		}
		s += jumpmark
		lastb = b.Name
	})
	if ends {
		s += lastb
	}
	return s
}

const (
	MarkRefuel  = '●' // fuel needed
	MarkStation = '○' // fuel could be taken here
	MarkOther   = '|' // system assumed to have no fuel

	printFmt = "%2d %6.2f  %c %s\n"
)

var RouteLegend = fmt.Sprintf("%c=refuel  %c=station  %c=other", MarkRefuel, MarkStation, MarkOther)

func PrintRoute(r Route) (n int, err error) {
	return FprintRoute(os.Stdout, r)
}

func FprintRoute(w io.Writer, r Route) (n int, err error) {
	v := make([]bool, r.Stat().NumJumps+1)
	i := 0
	var start *gmap.System
	r.Visit(func(a, b *gmap.System, refuel bool) {
		if start == nil {
			start = a
		}
		if refuel {
			v[i] = true
		}
		i++
	})
	var nn int
	nn, err = fmt.Fprintf(w, printFmt, 0, 0.0, MarkRefuel, start.Name)
	i = 1
	n += nn
	r.Visit(func(a, b *gmap.System, refuel bool) {
		dist := a.P.Sub(b.P).Abs()
		var mark rune
		switch {
		case v[i]:
			mark = MarkRefuel
		case canRefuelAtSystem(b):
			mark = MarkStation
		default:
			mark = MarkOther
		}
		nn, err = fmt.Fprintf(w, printFmt, i, dist, mark, b.Name)
		n += nn
		i++
	})
	return
}

func canRefuelAtSystem(sys *gmap.System) bool {
	return len(sys.Stations) != 0
}
