package route

import (
	"bitbucket.org/tajtiattila/harmless/db"
	_ "bitbucket.org/tajtiattila/harmless/db/testdata"
	"fmt"
	"math"
	"math/rand"
	"testing"
)

func init() {
}

func mkstat(nrefuel, njump, fuelleft, fuelused int, tlen float64) *Stat {
	return &Stat{tlen, njump, nrefuel, fuelleft, fuelused}
}

func cmpRlink(t *testing.T, ft int, a, b *Stat, eq bool) {
	t.Log(OptTime == OptFuelUse)
	e := OptTime
	if eq {
		if e.Evaluate(ft, a) != e.Evaluate(ft, b) {
			t.Errorf("Stat %#v and %#v supposed to be equal", a, b)
		}
	} else {
		if e.Evaluate(ft, a) >= e.Evaluate(ft, b) {
			t.Errorf("Stat %#v supposed to be better than %#v", a, b)
		}
	}
}

func TestRlinkStat(t *testing.T) {
	cmpRlink(t, 16, mkstat(0, 1, 1, 8, 8), mkstat(0, 1, 1, 8, 8), true)
	cmpRlink(t, 16, mkstat(0, 1, 1, 8, 8), mkstat(0, 1, 1, 10, 10), false)  // shorter distance
	cmpRlink(t, 16, mkstat(0, 1, 10, 10, 10), mkstat(0, 1, 1, 8, 8), false) // more fuel left
	cmpRlink(t, 16, mkstat(0, 1, 1, 8, 8), mkstat(0, 5, 10, 20, 20), false) // fewer jumps
	cmpRlink(t, 16, mkstat(0, 5, 1, 8, 8), mkstat(1, 2, 10, 20, 20), false) // fewer refuels
}

func TestRoutes(t *testing.T) {
	ch := NewOctreeChart(db.Systems)
	testRoute(t, ch, NewSimpleJumper(2, 0.8, 7.5), "Eranin", "G 239-25",
		"Eranin→i Bootis→Aulin→Rakapila@→LP 271-25→Ross 52→CE Bootis@→OT Serpentis→LHS 411→Miquich@→G 181-6→Tilian→LHS 3262@→DN Draconis→Lalande 29917→G 239-25")
}

func testRoute(t *testing.T, ch db.Chart, jr Jumper, an, bn, expect string) {
	a, err := db.FindSystem(an)
	if err != nil {
		t.Fatal(err)
	}
	b, err := db.FindSystem(bn)
	if err != nil {
		t.Fatal(err)
	}

	var best *rlink
	mem := new(rlink)

	ft := jr.FullTank()
	astar(ch, jr, a, b, func(n *rlink) bool {
		if testing.Verbose() {
			fmt.Printf("%s %d/%d :%d\n", n, n.stat.FuelLeft, n.reach, n.stat.FuelUsed(ft))
		}
		if best == nil || n.cost < best.cost {
			if n.star == b {
				*mem = *n
				best = mem
			} else {
				return true
			}
		}
		return false
	})

	if best != nil {
		best.cleanup()
	}

	if best != nil {
		if testing.Verbose() {
			t.Log(best)
			PrintRoute(best)
		}
		if best.String() != expect {
			t.Errorf("route from %s to %s, should be %s", a.Name, b.Name, expect)
			if !testing.Verbose() {
				t.Log("got", best)
			}
		}
	} else {
		if expect == "" {
			t.Errorf("there should be no route from %s to %s, got %s", a.Name, b.Name, best)
		}
	}
}

func diamondlattice(fn func(x, y, z int) bool) {
	var o int
	pt := func(x, y, z int) (stop bool) {
		if (o+(x|y|z))%2 == 0 || (o+x+y+z)%4 <= 1 {
			return !fn(x, y, z)
		}
		return false
	}
	n := 0
	for {
		o = 4 * n
		for x := -n; x <= n; x++ {
			for y := -n; y <= n; y++ {
				if pt(x, y, -n) || pt(x, y, +n) {
					return
				}
			}
		}
		for x := -n; x <= n; x++ {
			for z := -n + 1; z < n; z++ {
				if pt(x, -n, z) || pt(x, +n, z) {
					return
				}
			}
		}
		for y := -n + 1; y < n; y++ {
			for z := -n + 1; z < n; z++ {
				if pt(-n, y, z) || pt(+n, y, z) {
					return
				}
			}
		}
		n++
	}
}

// create cluster around center with stars startist apart, and specified minimal radius
func diamondlatticecluster(pfx string, center db.Vec3, stardist, radius float64) []*db.System {
	d := stardist / math.Sqrt(3)
	nstop := int(radius/d) + 1
	rr := radius * radius
	rrstop := 2 * rr

	ux := randvec()
	uy := perp(ux)
	uz := ux.Cross(uy)

	ux, uy, uz = ux.Mul(d), uy.Mul(d), uz.Mul(d)

	var v []*db.System
	index := 1
	diamondlattice(func(x, y, z int) bool {
		if x >= nstop {
			return false
		}
		p := ux.Mul(float64(x)).Add(uy.Mul(float64(y))).Add(uz.Mul(float64(z)))
		dd := p.Abssq()
		if dd >= rr {
			rrstop = dd
		}
		if dd <= rrstop {
			v = append(v, xstar(fmt.Sprintf("%s%d", pfx, index), center.Add(p), true))
			index++
		}
		return true
	})
	return v
}

func cubemap(mindist float64) []*db.System {
	ux := randvec()
	uy := perp(ux)
	uz := ux.Cross(uy)

	radius := mindist * 2
	mult, frac := float64(5), 0.25
	d := radius * mult
	ux, uy, uz = ux.Mul(d), uy.Mul(d), uz.Mul(d)

	var v []*db.System
	ch := rune('A')
	for x := 0; x <= 1; x++ {
		for y := 0; y <= 1; y++ {
			for z := 0; z <= 1; z++ {
				c := ux.Mul(float64(x)).Add(uy.Mul(float64(y))).Add(uz.Mul(float64(z)))
				v = append(v, diamondlatticecluster("c"+string(ch), c, mindist, radius)...)
				ch++
			}
		}
	}

	vecch := func(v db.Vec3) string {
		ch = rune('A')
		if v[0] > 0.5 {
			ch += 4
		}
		if v[1] > 0.5 {
			ch += 2
		}
		if v[2] > 0.5 {
			ch += 1
		}
		return string(ch)
	}
	for k := 0; k < 3; k++ {
		var c db.Vec3
		for x := 0; x <= 1; x++ {
			for y := 0; y <= 1; y++ {
				c[(k+1)%3] = float64(x)
				c[(k+2)%3] = float64(y)
				c[k] = frac
				a, ach := ux.Mul(c[0]).Add(uy.Mul(c[1])).Add(uz.Mul(c[2])), vecch(c)
				c[k] = 1 - frac
				b, bch := ux.Mul(c[0]).Add(uy.Mul(c[1])).Add(uz.Mul(c[2])), vecch(c)
				v = append(v, linestars("l"+ach+bch, a, b, mindist)...)
			}
		}
	}
	return v
}

func xstar(n string, p db.Vec3, station bool) *db.System {
	s := &db.System{Name: n, Id: n, P: p}
	if station {
		s.Stations = []db.Station{db.Station{System: s, Name: "*" + n}}
	}
	return s
}

func linestars(pfx string, a, b db.Vec3, mindist float64) []*db.System {
	n := int(math.Ceil(a.Sub(b).Abs()))
	u := b.Sub(a).Mul(1 / float64(n))
	var v []*db.System
	for i := 1; i < n; i++ {
		v = append(v, xstar(fmt.Sprintf("%s%d", pfx, i), a.Add(u.Mul(float64(i))), true))
	}
	return v
}

func randvec() db.Vec3 {
	const mbits = 52
	z := 2*float64(rand.Int63n(1<<mbits))/(1<<mbits-1) - 1
	r := math.Sqrt(1 - z*z)
	sin, cos := math.Sincos(rand.Float64() * 2 * math.Pi)
	return db.Vec3{r * cos, r * sin, z}
}

func perp(v db.Vec3) db.Vec3 {
	u := v.Cross(db.Vec3{v[1], v[2], v[0]})
	return u.Mul(1 / u.Abs())
}

/*
func testRouteShip(t *testing.T, chart *OctreeChart, perm []int, ship *db.Ship) int {
	jr := NewShipJumper(ship)
	nok, nerr := 0, 0
	for i := 1; i < len(perm); i++ {
		a, b := db.Systems[perm[i-1]], db.Systems[perm[i]]
		r := AstarRoute(chart, jr, a, b)
		if r != nil {
			nok++
		} else {
			nerr++
		}
	}
	if testing.Verbose() {
		t.Logf("%s: %d routes found, %d invalid", ship.Name, nok, nerr)
	}
	return nok
}

func TestAstarRouteShips(t *testing.T) {
	perm := rand.Perm(len(db.Systems))
	chart := NewOctreeChart(db.Systems)
	for _, sc := range db.Ships {
		ship := sc.New()
		n0 := testRouteShip(t, chart, perm, ship)
		mt := ship.OutfitSpec.MaxTonnage()
		n1 := testRouteShip(t, chart, perm, ship.Upgrade(mt/2))
		n2 := testRouteShip(t, chart, perm, ship.Upgrade(mt))
		if n1 > n0 || n2 > n1 {
			t.Errorf("heavier load yielded better route")
		}
	}
}
*/

func TestAstarRouteChart(t *testing.T) {
	jr := NewSimpleJumper(2, 0.8, 7)
	sv := cubemap(jr.FullRange())
	chart := NewOctreeChart(sv)
	t.Log("testing with", len(sv), "stars")

	perm := rand.Perm(len(sv))
	nmax := 100
	if testing.Short() {
		nmax = 10
	}
	if len(perm) > nmax {
		perm = perm[:nmax]
	}

	for i := 0; i+1 < len(perm); i++ {
		a, b := sv[perm[i]], sv[perm[i+1]]
		//astarDebug(t, chart, jr, a, b)
		r := AstarRoute(chart, jr, a, b)
		if r == nil {
			t.Error("route invalid")
		} else {
			if testing.Verbose() {
				s := r.Stat()
				nj, nr, fl := s.NumJumps, s.NumRefuels, s.FuelLeft
				t.Log(Format(r, "→", "!", true), nj, nr, fl)
			} else {
				t.Log(a.Name, "→", b.Name, "ok")
			}
		}
	}
}

func routeString(r *rlink) string {
	s := ""
	r.visitjcost(func(a, b *db.System, jcost int) {
		if s == "" {
			s = a.Name
		}
		s += fmt.Sprintf(" —%d@%.f2→ %s", jcost, a.P.Sub(b.P).Abs(), b.Name)
	})
	return s
}

func astarDebug(t *testing.T, chart db.Chart, jr Jumper, a, b *db.System) {
	var best *rlink
	mem := new(rlink)

	astar(chart, jr, a, b, func(n *rlink) bool {
		if best == nil || n.cost < best.cost {
			if n.star == b {
				fmt.Println("best", routeString(n), n.stat)
				*mem = *n
				best = mem
			} else {
				fmt.Println("cont", routeString(n), n.stat)
				return true
			}
		}
		return false
	})

	t.Log("found:", best)
}

/*
func TestAstarRoute(t *testing.T) {
	jr := NewSimpleJumper(2, 0.8, 7)
	//astarDebug(t, shipCircleSystems), jr, db.MustFindSystem("Acihaut"), db.MustFindSystem("Ross 1051"))
	//sv := shipCircleSystems(ship)
	t.Logf("%#v", sv)
	astarDebug(t, NewOctreeChart(sv), jr, sv[1], sv[2])
}
*/
