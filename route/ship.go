package route

import (
	"math"
)

const (
	DefaultFuelPower = 2
	DefaultFuelUnits = 64
)

// SimpleJumper is a Jumper based on data from the UI, and the hidden
// fuel power factor, and the number of "fuel units" the drive/tank/ship has.
//
// Evaluator is set to OptTime by default.
//
// FuelPower is typically higher than 2, and higher for better drives.
// If FuelPower is less than 1, DefaultFuelPower will be used.
//
// FuelUnits is a positive integer for rounding calculated jump fuel costs. The
// smallest amount of fuel usable for any jump is FuelTank/FuelUnits tons. Higher
// values will result in less rounding. The minimum value of 1 means all fuel
// is used up every time on the first jump.
// If FuelUnits is less than 1, DefaultFuelUnits will be used.
type SimpleJumper struct {
	FuelTank    float64 // full tank tonnage
	MaxJumpFuel float64 // drive jump fuel limit
	MaxJumpDist float64 // max jump distance

	FuelPower float64 // exponent for fuel use calculation
	FuelUnits int     // tank has this many units

	ScoopFuel bool // assume to have fuel scoop, don't refuel at stations

	Evaluator Evaluator // Evaluator to use with this jumper
}

func NewSimpleJumper(tanktonnage, maxjumpfuel, maxjumpdist float64) *SimpleJumper {
	return &SimpleJumper{
		FuelTank:    tanktonnage,
		MaxJumpFuel: maxjumpfuel,
		MaxJumpDist: maxjumpdist,
		Evaluator:   OptTime,
	}
}

func (jr *SimpleJumper) JumpCost(fuelu int, jumpdist float64) int {
	ratio := jumpdist / jr.MaxJumpDist
	if 1 < ratio {
		return JumpInvalid
	}
	if jr.ScoopFuel {
		return 0
	}
	fueluse := math.Pow(ratio, jr.fpower()) * jr.MaxJumpFuel
	return int(math.Ceil(float64(jr.funits()) * fueluse / jr.FuelTank))
}

func (jr *SimpleJumper) FullTank() int {
	return jr.funits()
}

func (jr *SimpleJumper) FullRange() float64 {
	return jr.MaxJumpDist
}

func (jr *SimpleJumper) Evaluate(ft int, s *Stat) int64 {
	return jr.Evaluator.Evaluate(ft, s)
}

func (jr *SimpleJumper) fpower() float64 {
	if jr.FuelPower >= 1 {
		return jr.FuelPower
	}
	return DefaultFuelPower
}

func (jr *SimpleJumper) funits() int {
	if jr.FuelUnits >= 1 {
		return jr.FuelUnits
	}
	return DefaultFuelUnits
}
