package route

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/packed"
)

type AstarMap struct {
	jr  Jumper
	spc *packed.Space
	m   map[gmap.Vec3i]map[gmap.Vec3i]Route
}

func NewAstarMap(spc *packed.Space, jr Jumper) *AstarMap {
	return &AstarMap{
		jr:  jr,
		spc: spc,
		m:   make(map[gmap.Vec3i]map[gmap.Vec3i]Route),
	}
}

func (m *AstarMap) Route(a, b gmap.Vec3) Route {
	return m.Routei(a.Vec3i(), b.Vec3i())
}

func (m *AstarMap) Routei(a, b gmap.Vec3i) Route {
	if _, ok := m.m[a]; !ok {
		m.m[a] = make(map[gmap.Vec3i]Route)
	}
	if r, ok := m.m[a][b]; ok {
		return r
	}
	r := AstarRoutei(m.spc, m.jr, a, b)
	m.m[a][b] = r
	return r
}

const astardbg = true

func astar(spc *packed.Space, jr Jumper, a, b gmap.Vec3i, fn func(*rlink) bool) {
	ft, fr := jr.FullTank(), jr.FullRange()

	h := makerlinkheap(&rlink{
		star: gmap.PackSys{P: a},
		stat: Stat{
			FuelLeft: ft,
		},
		reach: ft,
	})

	var buf rlinkbuf

	seen := make(map[gmap.Vec3i]*rvisit)

	for h.Len() != 0 {
		l := h.pop()
		spc.Sphere(l.star.P, fr, func(s gmap.PackSys, leg float64) {
			if leg <= fr && !l.visited(s.P) {
				if jcost := jr.JumpCost(l.stat.FuelLeft, leg); jcost != JumpInvalid {
					n := buf.newrlink(ft, leg, l, jcost, s)
					if n != nil {
						n.dist = s.P.Sub(a).Abssq()
						n.cost = jr.Evaluate(ft, &n.stat)
						rv := seen[s.P]
						if rv == nil {
							rv = new(rvisit)
							seen[s.P] = rv
						}
						if fn(n) && rv.add(n) {
							h.push(n)
						} else {
							buf.recycle(n)
						}
					}
				}
			}
		})
	}
}

func AstarRoute(spc *packed.Space, jr Jumper, a, b gmap.Vec3) Route {
	return AstarRoutei(spc, jr, a.Vec3i(), b.Vec3i())
}

func AstarRoutei(spc *packed.Space, jr Jumper, a, b gmap.Vec3i) Route {
	if a == b {
		return (&rlink{star: gmap.PackSys{P: a}}).finish(spc.Source)
	}

	var best *rlink
	mem := new(rlink)

	astar(spc, jr, a, b, func(n *rlink) bool {
		if best == nil || n.cost < best.cost {
			if n.star.P == b {
				*mem = *n
				best = mem
			} else {
				return true
			}
		}
		return false
	})

	if best != nil {
		return best.finish(spc.Source)
	}
	return nil
}

type rvisit struct {
	bestcost  *rlink
	bestreach *rlink
	rest      []*rlink
}

func (v *rvisit) add(l *rlink) (added bool) {
	if v.bestcost == nil {
		v.bestcost, v.bestreach = l, l
		return true
	}
	one := v.bestcost == v.bestreach
	if l.cost < v.bestcost.cost {
		if !one && v.bestcost.reach > l.reach {
			v.rest = append(v.rest, v.bestcost)
		}
		v.bestcost = l
		added = true
	}
	if l.reach > v.bestcost.reach {
		if v.bestreach != v.bestcost && v.bestreach.cost < l.cost {
			v.rest = append(v.rest, v.bestcost)
		}
		v.bestreach = l
		added = true
	}
	if one {
		return
	}
	for i, r := range v.rest {
		if r.reach >= l.reach && r.cost <= l.cost {
			return false
		}
		if l.reach >= r.reach && l.cost <= r.cost {
			v.rest[i] = l
			return true
		}
	}
	v.rest = append(v.rest, l)
	return true
}

func canRefuelAt(sys gmap.PackSys) bool {
	return sys.StnDistMag != 0
}
