package route

import (
	"container/heap"
	"fmt"

	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/packed"
)

type rlink struct {
	nref  int
	from  *rlink
	jcost int
	star  gmap.PackSys

	dist  float64
	cost  int64
	reach int

	stat Stat
}

type rlinkbuf struct {
	free *rlink
}

func (b *rlinkbuf) newrlink(ft int, leg float64, from *rlink, jcost int, star gmap.PackSys) (l *rlink) {
	reach := from.reach - jcost
	if reach < 0 {
		return nil
	}
	if b.free != nil {
		l, b.free = b.free, b.free.from
		l.from = from
		l.jcost = jcost
		l.star = star
	} else {
		l = &rlink{
			from:  from,
			jcost: jcost,
			star:  star,
		}
	}
	from.nref++
	l.stat.NumJumps = from.stat.NumJumps + 1
	l.stat.TotalLen = from.stat.TotalLen + leg
	if canRefuelAt(star) {
		l.reach = ft
	} else {
		l.reach = from.reach - jcost
	}
	l.stat.FuelLeft = from.stat.FuelLeft - jcost
	l.stat.FuelTaken = from.stat.FuelTaken
	l.stat.NumRefuels = from.stat.NumRefuels
	if l.stat.FuelLeft >= 0 {
		return l
	}
	l.stat.FuelTaken += l.reach - l.stat.FuelLeft
	l.stat.NumRefuels++
	l.stat.FuelLeft = reach
	return l
}

func (b *rlinkbuf) recycle(l *rlink) {
	l.from.nref--
	b.free, l.from = l, b.free
}

// cleanup sets final fuel left values
// so Visit can report valid refuel points
func (l *rlink) cleanup() *rlink {
	v := make([]*rlink, 0, l.stat.NumJumps+1)
	l.visit(func(l *rlink) {
		v = append(v, l)
	})
	fulltank := v[0].stat.FuelLeft
	tank, i, fi := fulltank, 1, -1
	for i < len(v) {
		ntank := tank - v[i].jcost
		v[i].stat.FuelLeft = ntank
		if ntank >= 0 {
			if canRefuelAt(v[i].star) && i >= fi {
				fi = i
			}
			tank, i = ntank, i+1
		} else if fi > 0 {
			tank = fulltank
			v[fi].stat.FuelLeft = tank
			i, fi = fi+1, -1
		}
	}
	return l
}

func (l *rlink) visited(s gmap.Vec3i) bool {
	for {
		if l == nil {
			return false
		}
		if l.star.P == s {
			return true
		}
		l = l.from
	}
	return false // not reached
}

func (l *rlink) visit(fn func(*rlink)) {
	if l.from != nil {
		l.from.visit(fn)
	}
	fn(l)
}

func (l *rlink) visitjcost(fn func(a, b gmap.PackSys, jcost int)) {
	if l.from != nil {
		l.from.visitjcost(fn)
		fn(l.from.star, l.star, l.jcost)
	}
}

func (l *rlink) finish(src packed.Source) Route {
	var (
		tank     int
		refueled bool
	)
	route := simpleRoute{
		stat: l.stat,
		v:    make([]rjump, 0, l.stat.NumJumps+1),
	}
	vp := make([]gmap.Vec3i, 0, l.stat.NumJumps+1)
	l.cleanup().visit(func(l *rlink) {
		vp = append(vp, l.star.P)
		route.v = append(route.v, rjump{refuel: refueled})
		if l.from != nil {
			tank -= l.jcost
			refueled = l.stat.FuelLeft > tank
		}
		tank = l.stat.FuelLeft
	})
	vsys := src.At(vp)
	for i := range vsys {
		route.v[i].sys = vsys[i]
	}
	return &route
}

func (l *rlink) String() string {
	var s string
	l.visit(func(l *rlink) {
		s += fmt.Sprint(" →", l.star.P)
	})
	return s
}

type rlinkheap struct {
	v []*rlink
}

func makerlinkheap(start *rlink) rlinkheap {
	return rlinkheap{[]*rlink{start}}
}

func (h *rlinkheap) Len() int             { return len(h.v) }
func (h *rlinkheap) Swap(i, j int)        { h.v[i], h.v[j] = h.v[j], h.v[i] }
func (h *rlinkheap) Push(x interface{})   { h.v = append(h.v, x.(*rlink)) }
func (h *rlinkheap) Pop() (r interface{}) { r, h.v = h.v[len(h.v)-1], h.v[:len(h.v)-1]; return }

func (h *rlinkheap) Less(i, j int) bool {
	li, lj := h.v[i], h.v[j]
	cmp := li.dist - lj.dist
	switch {
	case cmp < 0:
		return true
	case cmp == 0:
		return li.cost < lj.cost
	}
	return false
}

func (h *rlinkheap) push(r *rlink) { heap.Push(h, r) }
func (h *rlinkheap) pop() *rlink   { return heap.Pop(h).(*rlink) }

type simpleRoute struct {
	stat Stat
	v    []rjump
}

type rjump struct {
	sys    *gmap.System
	refuel bool
}

func (r *simpleRoute) Stat() Stat {
	return r.stat
}

func (r *simpleRoute) Visit(fn VisitRouteFunc) {
	for i := 1; i < len(r.v); i++ {
		fn(r.v[i-1].sys, r.v[i].sys, r.v[i].refuel)
	}
}
