package planner

import (
	et "bitbucket.org/tajtiattila/harmless"
	"time"
)

type Result struct {
	Ship     *et.Ship
	Cash     int
	Err      error
	NumFound int
	Time     time.Time
	Duration time.Duration
	Routes   []RouteResult
}

type RouteResult struct {
	AvgProfit int // profit/jump
	Profit    int
	Hops      []HopResult
}

type HopResult struct {
	From, To      *et.System
	Route         []RouteSystem // intermediate jump points, if any
	FuelRemaining int           // percent
	Items         []TradeItem
}

func (hr *HopResult) Best(atleast int, cutoff float64) []TradeItem {
	if len(hr.Items) == 0 {
		return nil
	}
	treshold := int(float64(hr.Items[0].TotalProfit) * cutoff)
	for i, h := range hr.Items {
		if h.TotalProfit < treshold && i >= atleast {
			return hr.Items[:i]
		}
	}
	return hr.Items
}

type RouteSystem struct {
	*et.System
	Refuel bool // refuel needed here
}

type TradeItem struct {
	T           TradeHopTrade
	Qty         int // buy this qty at start
	Profit      int // sell for this profit
	TotalProfit int
}

func (ti *TradeItem) Buy() int    { return int(ti.T.Buy.Price) }
func (ti *TradeItem) Sell() int   { return int(ti.T.Sell.Price) }
func (ti *TradeItem) Stock() int  { return ti.T.Buy.Qty }
func (ti *TradeItem) Demand() int { return ti.T.Sell.Qty }
