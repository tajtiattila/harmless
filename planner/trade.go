package planner

import (
	. "bitbucket.org/tajtiattila/harmless"
	"bitbucket.org/tajtiattila/harmless/market"
	"container/heap"
	"errors"
	//"fmt"
	"sort"
	"time"
)

type TradeHop struct {
	From, To *System
	Trades   []TradeHopTrade

	Value float64 // value calculated by HopValueFunc
}

type TradeHopTrade struct {
	*market.Commodity
	Buy, Sell   *market.PriceInfo
	BuyT, SellT time.Time
}

type HopValueFunc func(*TradeHop) float64

type tradekey struct {
	Buy, Sell *System
}

func FindHops(mds market.DataSource, valuefn HopValueFunc) ([]*TradeHop, error) {
	md, err := mds.Query()
	if err != nil {
		return nil, err
	}

	m := make(map[tradekey]*TradeHop)
	for _, c := range md.Commodities {
		for _, buyat := range c.Stations {
			for _, sellat := range c.Stations {
				h, ok := m[tradekey{buyat.System, sellat.System}]
				if !ok {
					h = &TradeHop{From: buyat.System, To: sellat.System}
					m[tradekey{buyat.System, sellat.System}] = h
				}
				h.Trades = append(h.Trades, TradeHopTrade{
					&c.Commodity,
					&buyat.PriceData.Buy,
					&sellat.PriceData.Sell,
					buyat.PriceData.Timestamp,
					sellat.PriceData.Timestamp,
				})
				h.Value = valuefn(h)
			}
		}
	}

	v := make([]*TradeHop, 0, len(m))
	for _, h := range m {
		v = append(v, h)
	}
	sort.Sort(sort.Reverse(hopSort(v)))
	i := 0
	for i < len(v) && valuefn(v[i]) > 0 {
		i++
	}
	return v[:i], nil
}

type hopSort []*TradeHop

func (v hopSort) Len() int           { return len(v) }
func (v hopSort) Less(i, j int) bool { return v[i].Value < v[j].Value }
func (v hopSort) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }

type TradeRoute struct {
	Hops  []*TradeHop
	Value float64 // sum(hop.Value for hop in Hops) / len(Hops)
}

func (r *TradeRoute) Head() *System { return r.Hops[0].From }
func (r *TradeRoute) Tail() *System { return r.Hops[len(r.Hops)-1].To }
func (r *TradeRoute) Key() string {
	s := r.Hops[0].From.Name
	for _, h := range r.Hops {
		s += "→" + h.To.Name
	}
	return s
}
func (r *TradeRoute) Len() int { return len(r.Hops) + 1 } // no of Systems
func (r *TradeRoute) System(i int) *System {
	if i != len(r.Hops) {
		return r.Hops[i].From
	}
	return r.Tail()
}

type FindRouteFunc func(r *TradeRoute) (include, keepgoing bool)

var (
	ErrAbort     = errors.New("FindRoutes aborted by FindRouteFunc")
	ErrCantStart = errors.New("No hop accepted at FindRoutes start, can't route")
	Finished     = errors.New("FindRoutes finished")
)

// FindRoutes finds routes for the market, and feeds them to rf.
// The value of a TradeHop is determined using hf. The workingsetsize
// determines the maximum amount of routes cached for further steps. Once this limit is reached,
// potential routes with the worst return are dropped.
func FindRoutes(mds market.DataSource, workingsetsize int, hf HopValueFunc, rf FindRouteFunc) error {
	hops, err := FindHops(mds, hf)
	if err != nil {
		return err
	}
	f := &routeFind{
		seen:   make(map[string]bool),
		work:   routeHeap{limit: workingsetsize},
		accept: rf,
	}
	if err = f.start(hops); err != nil {
		return err
	}
	for {
		if err = f.step(); err != nil {
			break
		}
	}
	if err == Finished {
		return nil
	}
	return err
}

type routeFind struct {
	hops     []*TradeHop
	entering map[*System][]*TradeHop
	leaving  map[*System][]*TradeHop
	seen     map[string]bool
	work     routeHeap
	accept   FindRouteFunc
}

func (f *routeFind) start(hops []*TradeHop) error {
	f.entering = make(map[*System][]*TradeHop)
	f.leaving = make(map[*System][]*TradeHop)
	for _, h := range hops {
		f.entering[h.To] = append(f.entering[h.To], h)
		f.leaving[h.From] = append(f.leaving[h.From], h)
		r := hopRoute(h)
		f.seen[r.Key()] = true
		include, keepgoing := f.accept(r)
		if include {
			f.work.push(r)
		}
		if !keepgoing {
			return ErrAbort
		}
	}
	if f.work.Len() == 0 {
		return ErrCantStart
	}
	return nil
}

func (f *routeFind) step() error {
	r := f.work.pop()
	if r.Head() == r.Tail() {
		return nil
	}
	for _, h := range f.leaving[r.Tail()] {
		if !hasTo(r, h.To) && !f.add(connect(r, hopRoute(h))) {
			return ErrAbort
		}
	}
	for _, h := range f.entering[r.Head()] {
		if !hasFrom(r, h.From) && !f.add(connect(hopRoute(h), r)) {
			return ErrAbort
		}
	}
	if f.work.Len() == 0 {
		return Finished
	}
	return nil
}

func (f *routeFind) add(r *TradeRoute) (keepgoing bool) {
	if r == nil {
		return true
	}
	circle := r.Head() == r.Tail()
	if circle {
		n := 0
		// circles start with smallest alphabetical statoin
		for i := 1; i < len(r.Hops); i++ {
			if r.Hops[i].From.Name < r.Hops[n].From.Name {
				n = i
			}
		}
		// "Programming Pearls 2nd Edition" from Jon Bentley
		nflip := len(r.Hops) - n
		reverseHops(r.Hops, 0, len(r.Hops))
		reverseHops(r.Hops, 0, nflip)
		reverseHops(r.Hops, nflip, len(r.Hops))
	}
	k := r.Key()
	if _, has := f.seen[k]; has {
		return true
	}
	f.seen[k] = true
	include, keepgoing := f.accept(r)
	if include && !circle {
		f.work.push(r)
	}
	return keepgoing
}

func hopRoute(h *TradeHop) *TradeRoute {
	return &TradeRoute{[]*TradeHop{h}, h.Value}
}

func hasFrom(r *TradeRoute, s *System) bool {
	for _, h := range r.Hops {
		if h.From == s {
			return true
		}
	}
	return false
}

func hasTo(r *TradeRoute, s *System) bool {
	for _, h := range r.Hops {
		if h.To == s {
			return true
		}
	}
	return false
}

func connect(a, b *TradeRoute) *TradeRoute {
	/*
		if a.Head() == a.Tail() || b.Head() == b.Tail() {
			return nil
		}
		if hasSameSystem(a.Hops, b.Hops) {
			return nil
		}
		fmt.Println(a.Key(), "+", b.Key())
	*/
	r := &TradeRoute{
		make([]*TradeHop, len(a.Hops)+len(b.Hops)),
		(a.Value + b.Value) / float64(len(a.Hops)+len(b.Hops)),
	}
	copy(r.Hops[:len(a.Hops)], a.Hops)
	copy(r.Hops[len(a.Hops):], b.Hops)
	return r
}

func hasSameSystem(a, b []*TradeHop) bool {
	for _, ah := range a {
		for _, bh := range b {
			if ah.To == bh.To {
				return false
			}
		}
	}
	for _, ah := range a {
		if b[0].From == ah.From {
			return false
		}
	}
	for _, bh := range b[1:] {
		if a[0].From == bh.From {
			return false
		}
	}
	return false
}

func reverseHops(r []*TradeHop, i, j int) {
	j--
	for i < j {
		r[i], r[j] = r[j], r[i]
		i++
		j--
	}
}

type routeHeap struct {
	limit int
	v     []*TradeRoute
}

func (h *routeHeap) Len() int             { return len(h.v) }
func (h *routeHeap) Less(i, j int) bool   { return h.v[i].Value > h.v[j].Value }
func (h *routeHeap) Swap(i, j int)        { h.v[i], h.v[j] = h.v[j], h.v[i] }
func (h *routeHeap) Push(x interface{})   { h.v = append(h.v, x.(*TradeRoute)) }
func (h *routeHeap) Pop() (r interface{}) { r, h.v = h.v[len(h.v)-1], h.v[:len(h.v)-1]; return }

func (h *routeHeap) pop() *TradeRoute {
	return heap.Pop(h).(*TradeRoute)
}

func (h *routeHeap) push(r *TradeRoute) {
	heap.Push(h, r)
	if len(h.v) > h.limit {
		h.v = h.v[:h.limit]
	}
}
