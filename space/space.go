package space

import (
	"math"
	"sort"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

// Interface is the data source for space.Space.
// The order of elements of Interface should not be modified after
// assigning them to a Space.
type Interface interface {
	// Len returns the number of elements.
	// It is called only during space initialization.
	Len() int

	// P returns the position of element i.
	// It is called once only during space initialization
	// with all values 0 <= i < Len().
	P(i int) gmap.Vec3i

	// At returns element i.
	// It is called when Space.At or Space.Sphere is used,
	// with values 0 <= i < Len().
	At(i int) interface{} // element i
}

// Space is an octree based position store,
// of arbitrary elements having 3D positions.
//
// The zero value of Space is ready to use
// and can be initialized with Init.
type Space struct {
	intf Interface
	v    []entry
	root snode
}

// Invalid is returned by Space.Idx if there is no data
// at a position.
const Invalid = int(^uint32(0))

// New creates a new Space from src.
//
// It panics if the absolute value of any coordinate is
// larger than or equal to 1<<31.
func New(i Interface) *Space {
	s := new(Space)
	s.Init(i)
	return s
}

// Init reinitializes s with data from i.
//
// It panics if the absolute value of any coordinate is
// larger than or equal to 1<<31.
func (s *Space) Init(intf Interface) {
	const maxlen = ^uint32(0)
	if int64(intf.Len()) > int64(maxlen) {
		panic("Space: too many elements")
	}
	sz := int64(1)
	v := make([]entry, intf.Len())
	for i := range v {
		v[i] = entry{intf.P(i), uint32(i)}
	}
	for _, n := range v {
		for i := 0; i < 3; i++ {
			for int64(n.p[i]) < -sz {
				sz *= 2
			}
			for sz <= int64(n.p[i]) {
				sz *= 2
			}
		}
	}
	if sz >= 1<<31 {
		panic("Space: nodes don't fit into int32 based tree")
	}
	isz := int32(sz)
	s.intf = intf
	s.v = v
	s.root = snode{
		min: gmap.Vec3i{-isz, -isz, -isz},
		max: gmap.Vec3i{isz, isz, isz},
		s:   0,
		e:   uint32(len(v)),
	}
	subdivide(v, &s.root)
}

// Idx returns the index of the element at pos,
// or Invalid if there is nothing at pos.
func (s *Space) Idx(pos gmap.Vec3i) int {
	return s.root.idx(s.v, pos)
}

// At returns the element at pos,
// or nil if there is nothing at pos.
func (s *Space) At(pos gmap.Vec3i) interface{} {
	if i := s.root.idx(s.v, pos); i != Invalid {
		return s.intf.At(i)
	}
	return nil
}

// Sphere calls f for each element d
// within the sphere defined by centre and radius.
func (s *Space) Sphere(centre gmap.Vec3i, radius float64, f func(d interface{}, dist float64)) {
	s.SphereIdx(centre, radius, func(i int, dist float64) {
		f(s.intf.At(i), dist)
	})
}

// Sphere calls f for each element index i
// within the sphere defined by centre and radius.
func (s *Space) SphereIdx(centre gmap.Vec3i, radius float64, f func(i int, dist float64)) {
	r := int32(math.Floor(radius * gmap.Vec3iUnit))
	s.root.query(&squery{
		v: s.v,
		c: centre,
		r: radius,
		f: f,
		min: gmap.Vec3i{
			centre[0] - r,
			centre[1] - r,
			centre[2] - r,
		},
		max: gmap.Vec3i{
			centre[0] + r + 1,
			centre[1] + r + 1,
			centre[2] + r + 1,
		},
	})
}

type snode struct {
	min, max gmap.Vec3i
	children []snode // 0 to 8 subtrees (empty ones omitted)
	s, e     uint32  // indices into Space.v
}

func (n *snode) idx(v []entry, p gmap.Vec3i) int {
	if p[0] < n.min[0] || p[1] < n.min[1] || p[2] < n.min[2] ||
		n.max[0] < p[0] || n.max[1] < p[1] || n.max[2] < p[2] {
		return Invalid
	}
	if n.children != nil {
		for _, child := range n.children {
			if i := child.idx(v, p); i >= 0 {
				return i
			}
		}
	} else {
		for i := n.s; i < n.e; i++ {
			if v[i].p == p {
				return int(v[i].idx)
			}
		}
	}
	return Invalid
}

func (n *snode) query(q *squery) {
	if q.max[0] < n.min[0] || q.max[1] < n.min[1] || q.max[2] < n.min[2] ||
		n.max[0] < q.min[0] || n.max[1] < q.min[1] || n.max[2] < q.min[2] {
		return
	}
	if n.children != nil {
		for _, child := range n.children {
			child.query(q)
		}
	} else {
		for i := n.s; i < n.e; i++ {
			if dist := q.v[i].p.Sub(q.c).Abs(); dist <= q.r {
				q.f(int(q.v[i].idx), dist)
			}
		}
	}
}

type squery struct {
	v []entry
	c gmap.Vec3i
	r float64
	f func(i int, dist float64)

	min, max gmap.Vec3i
}

func subdivide(v []entry, n *snode) {
	const maxNodeLen = 8

	leaves := v[n.s:n.e]
	if len(leaves) < maxNodeLen || n.min[0]+1 == n.max[0] {
		return // no need to or cannot subdivide
	}

	var c gmap.Vec3i
	for k := 0; k < 3; k++ {
		c[k] = (n.min[k] + n.max[k]) / 2
	}

	// sort leaves so every leaf in the same octant is in a continuous range
	sort.Sort(&nodeSort{c: c, v: leaves})

	i := 0
	for octant := 0; octant < 8; octant++ {
		var child snode
		for k := 0; k < 3; k++ {
			if (octant & (1 << uint(2-k))) == 0 {
				child.min[k] = n.min[k]
				child.max[k] = c[k]
			} else {
				child.min[k] = c[k]
				child.max[k] = n.max[k]
			}
		}
		s := i
	LeavesLoop:
		for ; i < len(leaves); i++ {
			p := leaves[i].p
			for k := 0; k < 3; k++ {
				if p[k] < child.min[k] || child.max[k] <= p[k] {
					break LeavesLoop
				}
			}
		}
		// add child if there it has at least one element
		if s != i {
			child.s, child.e = n.s+uint32(s), n.s+uint32(i)
			subdivide(v, &child)
			n.children = append(n.children, child)
		}
	}

	n.s, n.e = 0, 0

	// if n has only once child, replace
	// it with its child
	if len(n.children) == 1 {
		*n = n.children[0]
	}
}

type entry struct {
	p   gmap.Vec3i // positon of entry from Data
	idx uint32     // index of entry within Data
}

// nodeSort sorts v according to which octant they are with
// relation to center c. The order of coordinates is X, Y, Z:
// Nodes left of c (smaller X) comes first, then Nodes below c
// (smaller Y) within those blocks comes first, finally nodes
// behind c (smaller Z) within comes first.
type nodeSort struct {
	c gmap.Vec3i
	v []entry
}

func (s *nodeSort) Len() int      { return len(s.v) }
func (s *nodeSort) Swap(i, j int) { s.v[i], s.v[j] = s.v[j], s.v[i] }

func (s *nodeSort) Less(i, j int) bool {
	u, v := s.v[i].p, s.v[j].p
	for k := 0; k < 3; k++ {
		if ok := u[k] < s.c[k]; ok != (v[k] < s.c[k]) {
			return ok
		}
	}
	return false
}
