(function( starc, $, undefined ) {

	function inputField(entry, fdecl, changeFn, optarg) {
		var opts = $.extend({
			"class": "",
			itype: "text",
			defvalue: "",
			parse: function(s) { return s; },
			fmt: function(v) { return v.toString(); }
		}, optarg);
		var value = entry[fdecl.field];
		if (value === null || value === undefined) {
			value = opts.defvalue;
		}
		var lbl = $("<label>", {
			"class": optarg["class"],
			"for": fdecl.field
		}).text(fdecl.label);
		if (fdecl.info) {
			lbl.append($("<span>", {"class":"info"}).append($("<i>", {
				"class": "fa fa-info-circle",
				"title": fdecl.info
			})));
		}
		var inp = $("<input>", {
			"class": optarg["class"],
			name: fdecl.field,
			type: opts.itype,
			value: opts.fmt(value),
			placeholder: fdecl.label
		}).blur(function(e) {
			entry[fdecl.field] = opts.parse($(e.currentTarget).val());
			if (changeFn) changeFn(entry, fdecl);
		});
		if (fdecl.options) {
			inp.autocomplete({ minLength: 0, source: fdecl.options });
		}
		return $("<div>").append(lbl, inp);
	}

	function fieldFactory(cls) {
		var t = {
			str: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls
				});
			},
			num: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					itype: "number",
					parse: function(s) { return parseFloat(s); }
				});
			},
			bignum: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					defvalue: 0,
					parse: starc.parsePop,
					fmt: starc.fmtPop
				});
			},
			vstr: function(entry, fdecl, change) {
				return inputField(entry, fdecl, change, {
					"class": cls,
					defvalue: [],
					parse: function(s) {
						var v0 = s.split(",");
						var v = [];
						for (var i = 0; i < v0.length; i++) {
							var e = v0[i].trim();
							if (e != "") {
								v.push(e);
							}
						}
						return v;
					},
					fmt: function(v) { return v.join(", "); }
				});
			},
			bool: function(entry, fdecl, change) {
				return $("<button>", {
					"class": "toggle" + (entry[fdecl.field] == true ? " checked" : ""),
				}).text(fdecl.label).on("click", function(evt) {
					$(evt.currentTarget).toggleClass("checked");
					entry[fdecl.field] = $(evt.currentTarget).hasClass("checked");
					if (change) change(entry, fdecl);
				});
			},
			xbool: function(entry, fdecl, change) {
				var stateClass = " indeterminate";
				if (entry[fdecl.field] == true) {
					stateClass = " checked";
				} else if (entry[fdecl.field] == false) {
					stateClass = "";
				}
				return $("<button>", {
					"class": "toggle" + stateClass,
				}).text(fdecl.label).on("click", function(evt) {
					var b = $(evt.currentTarget);
					if (b.hasClass("checked")) {
						entry[fdecl.field] = false;
						b.removeClass("checked");
						b.removeClass("indeterminate");
					} else if (b.hasClass("indeterminate")) {
						entry[fdecl.field] = true;
						b.addClass("checked");
						b.removeClass("indeterminate");
					} else {
						entry[fdecl.field] = null;
						b.removeClass("checked");
						b.addClass("indeterminate");
					}
					if (change) change(entry, fdecl);
				});
			}
		};
		return function(entry, fdecl, change) {
			return t[fdecl.type](entry, fdecl, change);
		};
	};

	(function() {
		$.ajax({
			url: starc.apiurl("edit", { s: starc.ref.editloc() }),
			dataType: "json",
			success: function(resp) {
				if (resp.error) {
					starc.showError("get system data", resp.error);
				} else if (!resp.data) {
					starc.showError("get system data", "response empty");
				} else {
					systemForm(resp.data);
				}
			},
			error: function(xhr, textstatus, errthrown) {
				starc.ajaxError("get system data", xhr, textstatus, errthrown);
			}
		});
	})();

	function systemForm(s) {
		var c = $("#content");
		// title
		c.append($("<h1>", {"class": "system"})
				.text(s.system));
		c.append($("<span>").text(starc.uicoordstr(s.coords)));

		// valid checkbox
		var ff = fieldFactory("system");
		$("<div>").appendTo(c).append(
			ff(s, {type: "xbool", field: "valid", label: "System exists"}, function() {}),
			$("<span>", {"class": "sysmeta"}).text("Last edit by "),
			$("<span>", {"class": "sysmeta cmdr"}).text(s.cmdr || "unnamed"),
			$("<span>", {"class": "sysmeta"}).text(" " + starc.fmtRelTime(s.modified))
		);

		// commander name
		$("<div>", { "style": "padding-top:1em;" }).appendTo(c).append(
			$("<label>", {
				"class": "field",
				"for": "cmdr"
			}).append(
				$("<span>").text("Commander "),
				$("<i>", {
					"class": "fa fa-info-circle",
					"title": "optional"
				})
			),
			$("<input>", {
				"class": "",
				type: "text",
				name: "cmdr",
				value: starc.prefs.cmdr
			})
			.blur(function(e) {
				starc.prefs.cmdr = $(e.currentTarget).val();
				starc.saveprefs();
			})
		);

		// system details
		c.append($("<h2>", { "class": "system" }).text("System"));
		var frm = $("<div>", { "class": "starc-form" }).appendTo(c);
		if (s.detail === null || s.detail === undefined) {
			s.detail = {};
		}
		var sysfields = [
			{ type: "str",    field: "allegiance", label: "Allegiance" },
			{ type: "str",    field: "economy",    label: "Economy",
				info: "main system economy as shown in the galaxy map" },
			{ type: "str",    field: "government", label: "Government" },
			//{ type: "str",    field: "security",   label: "Security"   },
			{ type: "bignum", field: "population", label: "Population" },
			{ type: "bool", field: "requires_permit", label: "Requires permit" }
		];
		var ff = fieldFactory("systemdetail");
		for (var i = 0; i < sysfields.length; i++) {
			var f = sysfields[i];
			if (starc.defs.hasOwnProperty(f.field)) {
				f.options = starc.defs[f.field];
			}
			frm.append(ff(s.detail, f, function() { touch(s) }));
		}

		// stations
		c.append($("<div>", {"class": "clearfix"}));
		c.append($("<h2>", { "class": "stations" }).text("Stations"));
		c.append($("<div>", {
			"class": "clearfix",
			id: "stations"
		}).sortable());
		var vstn = s.stations;
		if (vstn) {
			for (var i = 0; i < vstn.length; i++) {
				addStation(vstn[i]);
			}
		}

		// buttons
		c.append($("<div>")
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-plus"
				}),
				" Add Station"
			).on("click", function(e) {
				$(".station").each(function(i, stnbox) {
					var stn = $(stnbox).data("stn");
					if (stn && stn.inferred) {
						stnbox.remove();
					}
				})
				addStation({});
			}))
		);

		c.append($("<div>")
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-check"
				}),
				" Save"
			).on("click", function(e) {
				var sx = $.extend({}, s);
				sx.stations = [];
				$(".station").each(function(i, stnbox) {
					var stn = $(stnbox).data("stn");
					if (stn) {
						sx.stations.push(stn);
					}
				});
				if (consistencyCheck(sx)) {
					postSystem(sx);
				}
			}))
			.append($("<button>", { "class": "formcontrol" }).append(
				$("<i>", {
					"class": "fa fa-refresh"
				}),
				" Reload"
			).on("click", function(e) {
				window.location.reload();
			}))
		);
	}

	var stnfields = [
		{ type: "str",  field: "station", label: "Name" },
		{ type: "num",  field: "distance", label: "Distance", info: "from main star with nav beacon (Ls)" },
		{ type: "str",  field: "type", label: "Type", options: starc.defs.station_type },
		{ type: "str",  field: "faction", label: "Faction" },
		{ type: "str",  field: "government", label: "Government", options: starc.defs.government },
		{ type: "str",  field: "allegiance", label: "Allegiance", options: starc.defs.allegiance },
		{ type: "vstr", field: "economy", label: "Economy", info: "comma separated, from system map", options: starc.defs.economy },
		{ type: "xbool", field: "shipyard", label: "Shipyard" },
		{ type: "xbool", field: "outfitting", label: "Outfitting" },
		{ type: "xbool", field: "commodities_market", label: "Commodities" },
		{ type: "xbool", field: "black_market", label: "Black market" }
	];
	function addStation(stn) {
		var stnbox = $("<div>", {
			"class": "station"
		}).appendTo("#stations").data("stn", stn);

		stnbox.append($("<button>", { "class": "close" }).append(
			$("<i>", {
				"title": "delete station",
				"class": "fa fa-times"
			})).on("click", function(e) {
				stnbox.fadeOut("slow", "swing", function() { stnbox.remove(); });
			}));

		if (stn.inferred) {
			stnbox.append($("<span>", {"class": "inferred"}).text("This system is believed to have at least one station. Please add a real station in this system, or delete this entry if no stations exist."))
		} else {
			var frm = stnbox;
			var ff = fieldFactory("stationfield");
			for (var i = 0; i < stnfields.length; i++) {
				frm.append(ff(stn, stnfields[i], function(e) { touch(e) }));
			}
		}
	}

	function consistencyCheck(s) {
		if (!s.detail && s.stations) {
			starc.showError("consistency", "has stations but no allegiance/economy");
			return false;
		}
		if (s.stations) {
			var dupname = {};
			for (var i = 0; i < s.stations.length; i++) {
				var stn = s.stations[i];
				if (stn.station === undefined || stn.station == "") {
					starc.showError("consistency", "station must have a name");
					return false;
				}
				if (dupname[stn.station.toLowerCase()]) {
					starc.showError("consistency", "duplicate station name: " + stn.station);
					return false;
				}
				dupname[stn.station.toLowerCase()] = true;
			}
		}
		return true;
	}

	function touch(sysOrStn) {
		sysOrStn.valid = true;
		var d = new Date();
		sysOrStn.modified = d.toISOString();
	}

	function postSystem(sys) {
		console.log(JSON.stringify(sys));
		sys.cmdr = starc.prefs.cmdr;
		$.ajax({
			type: "POST",
			url: starc.apiurl("edit"),
			dataType: "json",
			data: JSON.stringify({"source":"starchart.club", "data":[{op: "update", key: sys.coords, data: sys}]}),
			success: function(resp) {
				if (resp.error) {
					starc.showError("post system data", resp.error);
				} else if (!resp.success) {
					starc.showError("post system data", "response empty");
				} else {
					starc.reportSuccess("Data posted. Good work, commander! Now reload this page to see if everything is in order.");
				}
			},
			error: function(xhr, textstatus, errthrown) {
				starc.ajaxError("post system data", xhr, textstatus, errthrown);
			}
		});
	}

})(window.starc = window.starc || {}, jQuery);
