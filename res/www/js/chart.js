(function( starc, $, undefined ) {

	var chartparams = {
		dotr: 0.25,
		barscale: 0.2, // scale for vertical bars
		syslabel: {
			// system label settings
			"x": 0.5,
			"y": 0.3,
			"font-size": 0.8
		},
		sysbar: {
			// system bar (vertical line) settings
			"stroke": "#888",
			"stroke-width": 0.05
		},
		sectlabel: {
			// sector label settings
			"x": 0,
			"y": 0.6,
			"fill": "white",
			"opacity": 0.6,
			"font-size": 1.8
		},
	};

	starc.chart = {
		data: {},

		setup: function(p) {
			$.extend(chartparams, p);
			if (chartparams.container) {
				$(chartparams.container).draggable({
					cursor: "move",
					delay: 150,
					start: function(event, ui) {
						dragging = true;
					},
					stop: function(event, ui) {
						setTimeout(function(){ dragging = false; }, 300);
					}
			   	});
			}
		},

		load: function(after) {
			if (chartparams.loading !== undefined) {
				$(chartparams.loading).show();
			}
			var q = {
				c: starc.ref.view(),
				p: starc.ref.loc(),
				r: starc.prefs.view.radius,
				jr: starc.prefs.ship.MaxJumpDist
			};
			var hp = starc.hashparams();
			if (hp.hasOwnProperty("i")) {
				q.i = hp.i;
			}
			$.ajax({
				dataType: "json",
				url: starc.apiurl("chart", q),
				success: function(data) {
					if (chartparams.loading !== undefined) {
						$(chartparams.loading).hide();
					}
					starc.chart.data = data.chart;
					prepareChart();
					markupChart(null, true, false);
					loadWaypoints();
					if (after) {
						after();
					}
				},
				error: function(xhr, textstatus, errthrown) {
					if (chartparams.loading !== undefined) {
						$(chartparams.loading).hide();
					}
					starc.ajaxError("mapload", xhr, textstatus, errthrown);
				}
			});
		},

		lookup: function(id) {
			return idmap[id.toLowerCase()];
		},

		cmpkey: function(a, b) {
			if (a.hasOwnProperty("system")) {
				a = a.system;
			}
			return a.toLowerCase() == b.toLowerCase();
		}
	};

	var known_routes = {};

	var dragging = false;

	var showinfo = function(){};
	var setsearch = function(){};

	var wayptbox, routebox, mainsearchfield;

	var stationicon_template;
	var stnx = $("<div>");
	stnx.load(starc.prefix + "/image/station.svg", function(resp, stat, xhr) {
		stationicon_template = $(stnx).children().first();
	});

	var stationicon = function(type) {
		var layer;
		if (typeof type === "string") {
			var p = type.toLowerCase().split(/ +/);
			if (p.length == 2) {
				if ($.inArray(p[0], ["coriolis", "ocellus", "orbis"]) > -1) {
					layer = "#" + p[0];
				} else if (p[1] == "outpost") {
					layer = "#outpost";
				}
			}
		}
		if (!layer) {
			layer = "#unknown";
		}
		var stnico = stationicon_template.clone();
		$(layer, stnico).css("display", "inline");
		return stnico;
	}

	// setup omnibox
	starc.omnisetup = function(searchbox, info, waypts, itinerary) {
		wayptbox = waypts;
		routebox = itinerary;

		$(wayptbox).sortable({
			handle: ".sorthandle",
			update: function (evt, ui) {
				updateWaypoints();
			}
		});

		// system info
		showinfo = function(sys) {
			setsearch(sys.system);
			var inf = $("<div/>", { "class": "starc omnielem" });
			bigBtn("fa-location-arrow", "Navigate").appendTo(inf).on("click", function(e) {
				addWaypoint(sys.system);
				updateWaypoints();
			});
			$("<button>", {"title": "reload map centered on this system"}).on("click", function(e) {
				console.log(sys);
				console.log(starc.syskey(sys));
				starc.ref.set({view: sys.system});
				starc.chart.load();
			}).append($("<i>", {"class": "fa fa-fw fa-crosshairs"})).appendTo(inf);
			$("<button>", {"title": "edit system details"})
				.append($("<i>", {"class":"fa fa-fw fa-edit"})).on("click", function(e) {
				e.preventDefault(); e.stopPropagation();
				var href = starc.prefix + "/edit/";
				window.location.href = href;
			}).appendTo(inf);
			inf.append($("<span>").text(starc.uicoordstr(sys.coords)));
			if (sys.detail !== undefined) {
				var sysinf = [];
				var sipush = function(i, t) { if (t) sysinf.push({i:i, t:t}); }
				sipush("fa-male", starc.fmtPop(sys.detail.population));
				sipush("fa-flag", sys.detail.allegiance);
				sipush("fa-line-chart", sys.detail.economy);
				sipush("fa-shield", sys.detail.government);
				if (sys.detail.requires_permit) {
					sipush("fa-warning", "Requires permit");
				}
				var sinfel = $("<ul>", {"class": "fa-ul"}).appendTo(inf);
				for (var i = 0; i < sysinf.length; i++) {
					var si = sysinf[i];
					$("<li>", {"class": "systeminfo"}).append(
						$("<i>", {"class": "fa fa-li " + si.i}),
						$("<span>").text(si.t)
					).appendTo(sinfel);
				}
			}
			if (sys.stations) {
				var vstn = sys.stations.slice(0);
				vstn.sort(function(a, b) {
					var ad = typeof a.distance == "number" ? a.distance : 1e9;
					var bd = typeof b.distance == "number" ? b.distance : 1e9;
					if (ad != bd) { return ad-bd; }
					var al = a.station == "string" ? a.station.toLowerCase() : "";
					var bl = b.station == "string" ? b.station.toLowerCase() : "";
					if (al != bl) return al < bl ? -1 : 1;
					return 0;
				});
				var stnlist = $("<div>", {"class":"station-list"}).appendTo(inf);
				for (var i = 0; i < vstn.length; i++) {
					var stn = vstn[i];
					var stnel = $("<div>", {"class":"station-entry"}).appendTo(stnlist);
					$("<div>", {"class":"station-cell station-dist"})
						.appendTo(stnel).append(starc.numFmt(stn.distance));
					$("<div>", {"class":"station-cell station-icon"})
						.appendTo(stnel).append(stationicon(stn.type));
					$("<div>", {"class":"station-cell station-name"})
						.appendTo(stnel).text(stn.station);
				}
			}
			$(info).empty();
			inf.appendTo($(info));
		};

		// search box
		var srch = starSearchField({
			"class": "star search",
			select: function(sys) {
				userSystemSelect(sys, false);
			}
		});
		$(srch).focus(function() {
			$(srch).val("");
		}).appendTo(searchbox);

		mainsearchfield = srch;
		setsearch = function(name) {
			$(srch).val(name);
		};
	};

	function bigBtn(icon, label) {
		return $("<button>", {"class":"big"}).append(
				$("<i>", {"class":"fa fa-2x " + icon}),
				$("<div>").text(label));
	}

	function userSystemSelect(sys, ismapclick) {
		markupChart(sys.system, !ismapclick, true);
		if (starc.ref.waypts().length == 1 && !lceq(starc.ref.waypts()[0], sys.system)) {
			addWaypoint(sys.system);
			updateWaypoints();
		}
		routeboxHighlight();
	}

	function routeboxHighlight() {
		$(".highlight", routebox).removeClass("highlight");
		var loc = starc.ref.loc();
		$(".routeline", routebox).each(function (i, e) {
			if (e.sys !== undefined && starc.syskey(e.sys) == loc.toLowerCase()) {
				$(e).addClass("highlight");
			}
		});
	}

	// search input used in main search and route waypts
	function starSearchField(opts) {
		var cls = "star";
		if (opts.hasOwnProperty("class")) {
			cls = opts["class"];
		}
		var srch = $("<input>", {
			"class": cls,
			"type": "text",
			"placeholder": "System"
		});
		var fromfn;
		if (opts.hasOwnProperty("from")) {
			if (typeof(a) == "string") {
				fromfn = function() { return opts.from; };
			} else {
				fromfn = opts.from;
			}
		} else {
			fromfn = starc.ref.loc;
		}
		$(srch).autocomplete({
			minLength: 2,
			source: function(request, response) {
				$.getJSON(starc.apiurl("search", {
					c: fromfn(),
					term: request.term,
				}), request, response);
			},
			focus: function(event, ui) {
				$(srch).val( ui.item.label );
				return false;
			},
			select: function(event, ui) {
				$(srch).val( ui.item.label ).blur();
				opts.select(ui.item.value);
				return false;
			}
		})
		.autocomplete("instance")._renderItem = function(ul, item) {
			return $("<li>")
				.append(
						$("<span>", { "class": "route star" }).text(item.label + " "),
						$("<span>", { "class": "dist" }).text(rounddist(item.value.dist))
				).appendTo(ul);
		};
		return srch;
	}

	function addWaypoint(name) {
		var elem = $("<div>", { "class": "starc waypt" });
		elem[0].sys = name;
		$("<span>", {
			"class": "sorthandle"
		}).append('<i class="fa fa-ellipsis-v"></i>').appendTo(elem);
		$("<input>", {
			"type": "text",
			"class": "star",
			"value": name
		}).appendTo(elem);
		$("<button>", {
			"class": "sortbtn"
		}).append('<i class="fa fa-times"></i>').on("click", function(e) {
			var n = elem[0];
			n.parentNode.removeChild(n);
			updateWaypoints();
		}).appendTo(elem);
		elem.appendTo(wayptbox);
	}

	function loadWaypoints() {
		wayptbox.empty();
		var waypts = starc.ref.waypts();
		for (var i = 0; i < waypts.length; i++) {
			addWaypoint(waypts[i]);
		}
		updateRoute();
	}

	function updateWaypoints() {
		var wayptelems = $(".waypt", wayptbox);
		var waypts = [];
		for (var i = 0; i < wayptelems.length; i++) {
			waypts.push(wayptelems[i].sys);
		}
		starc.ref.set({waypts: waypts});
		starc.saveprefs();
		updateRoute();
	}

	function updateRoute() {
		var waypts = starc.ref.waypts();
		routebox.empty();
		chartroute.clear();
		if (waypts.length == 0) {
			return;
		}
		if (waypts.length == 1) {
			routebox.append($("<div>").text("Destination:"));
			mainsearchfield.focus();
			return;
		}
		function rmissing(a, b) {
			return [
				$("<span>", {"class": "route badstar"}).text(a),
				" → ",
				$("<span>", {"class": "route badstar"}).text(b)
			];
		};
		function rspan(sys) {
			var cls = "route star";
			if (!sys.stations) cls += " stationless";
			return $("<span>", {"class": cls}).text(sys.system).on("click", function(e) {
				userSystemSelect(sys, false);
			});
		}
		function rdist(a, b) {
			var ac = a.coords;
			var bc = b.coords;
			var d = Math.sqrt(
					Math.pow(ac[0]-bc[0],2) +
					Math.pow(ac[1]-bc[1],2) +
					Math.pow(ac[2]-bc[2],2));
			return $("<span>", {"class":"jump dist"}).text(rounddist(d).toString());
		}
		routebox.append($("<button>", { "class": "close" }).append(
					$("<i>", {
						"title": "clear route",
						"class": "fa fa-times"
					})).on("click", function(e) {
						wayptbox.empty();
						updateWaypoints();
					}));
		var routelist = $("<ul>", {"class": "fa-ul"}).appendTo(routebox);
		var loc = null;
		var showfirst = true;
		for (var i = 1; i < waypts.length; i++) {
			var a = waypts[i-1];
			var b = waypts[i];
			if (a.toLowerCase() == b.toLowerCase())
				continue;
			var key = encodeURIComponent(a.toLowerCase()) + "/" + encodeURIComponent(b.toLowerCase());
			if (known_routes.hasOwnProperty(key)) {
				var r = known_routes[key];
				if (r.unreachable) {
					var elem = $("<li>", {"class": "routeline"}).appendTo(routelist);
					elem.append($("<i>", {
						"class": "fa-li fa fa-warning",
						"title": "no route"
					}));
					elem.append(a, " → ", b);
					//chartroute.line(a, b, "invalid");
					showfirst = true;
					loc = null;
				} else if (!r.route) {
					var elem = $("<li>", {"class": "routeline"}).appendTo(routelist);
					elem.append($("<i>", {
						"class": "fa-li fa fa-warning",
						"title": r.error
					}));
					elem.append(rmissing(a, b));
					//chartroute.line(a, b, "invalid");
					showfirst = true;
					loc = null;
				} else {
					var rr = r.route;
					for (var j = (showfirst ? 0 : 1); j < rr.length; j++) {
						var elem = $("<li>", {"class": "routeline"}).appendTo(routelist);
						var e = rr[j];
						elem[0].sys = e;
						if (j == 0 || j + 1 == rr.length) {
							elem.append($("<i>", {
								"class": "fa-li fa fa-map-marker",
								"title": "waypoint"
							}));
						} else if (e.refuel) {
							elem.append($("<i>", {
								"class": "fa-li fa fa-check-circle",
								"title": "refuel"
							}));
						}
						elem.append(rspan(e), rdist(loc || e, e));
						if (loc != null) {
							chartroute.line(loc, e, "jump");
						}
						loc = e;
						showfirst = false; // don't show duplicate
					}
				}
			} else {
				(function(routekey){
					$.ajax({
						dataType: "json",
						url: starc.apiurl("route", {
							s: a,
							d: b,
							j: jumperSpec()
						}),
						success: function(data) {
							if (data.route || data.unreachable || data.error) {
								known_routes[routekey] = data;
								updateRoute();
							} else {
								known_routes[routekey] = {
									error: "Unknown error while fetching route"
								};
							}
						},
						error: function(jqxhr, textstatus, errthrown) {
							known_routes[routekey] = {
								error: textstatus
							};
						}
					});
				})(key);
				var elem = $("<li>", {"class": "routeline"}).appendTo(routelist);
				elem.append('<i class="fa fa-refresh fa-spin"></i> ', rmissing(a, b));
				showloc = true;
			}
		}
		routeboxHighlight();
	}

	var chartroute = {
		dots: {},
		clear: function() {
			$("#routes", starc.chart.svg).empty();
			chartroute.dots = {};
		},
		dot: function(id, p) {
			if (chartroute.dots.hasOwnProperty(id)) return;
			chartroute.dots[id] = true;

			var cs = starc.chart.data.scale;
			var grp = starc.node("g", tr(cs*p.sx, cs*p.sy), starc.chart.svg.getAttribute("xmlns"));
			grp.child("circle", {
				r: chartparams.dotr*(p.visible ? 2.0 : 1.0),
				fill: "cyan",
			});
			$("#routes", starc.chart.svg).prepend(grp);
		},
		line: function(a, b, style) {
			var g = $("#routes", starc.chart.svg);
			var ap = screenpos(a);
			chartroute.dot(starc.syskey(a), ap);
			var bp = screenpos(b);
			chartroute.dot(starc.syskey(b), bp);

			var linestyle;
			if (!ap.visible || !bp.visible) {
				linestyle = {
					"stroke-dasharray": "0.5, 0.5",
					"stroke-width": 0.1
				};
			} else {
				linestyle = {
					"stroke-width": 0.3
				};
			}

			var color = "cyan";
			if (style == "loading") {
				color = "orange";
			} else if (style == "invalid") {
				color = "red";
			}

			var cs = starc.chart.data.scale;
			var line = starc.node("line", $.extend(linestyle, {
				x1: cs*ap.sx,
				y1: cs*ap.sy,
				x2: cs*bp.sx,
				y2: cs*bp.sy,
				stroke: color,
				"stroke-linecap": "round"
			}), starc.chart.svg.getAttribute("xmlns"));
			g.append(line);
		}
	};

	function jumperSpec() {
		var s = starc.prefs.ship;
		var fields = [
			s.MaxJumpDist,
			s.MaxJumpFuel,
			s.FuelTank,
			s.FuelPower,
			s.FuelUnits,
			s.RoutingMode
		];
		var j = "", sep = "";
		for (var i = 0; i < fields.length; i++) {
			j = j + sep + fields[i].toString()
			sep = "_";
		}
		return j;
	}

	function prefColor(typ, sys) {
		var v = starc.prefs.view.colors;
		if (sys.detail !== undefined) {
			var det = sys.detail;
			for (var i = 0; i < v.length; i++) {
				var c = v[i];
				if (c.Type == typ && det.hasOwnProperty(c.Key) && lceq(det[c.Key], c.Value))
					return c.Color;
			}
		}
		return "white";
	}

	function lceq(a, b) {
		if (typeof(a) == "string" && typeof(b) == "string") {
			return a.toLowerCase() == b.toLowerCase();
		}
		return false;
	}

	function markupChart(loc, center, animate) {
		if (loc) {
			if (starc.chart.lookup(loc) === undefined) {
				starc.ref.set({view: loc, loc: loc});
				starc.chart.load();
				return;
			}
			starc.ref.set({loc: loc});
		}
		// find selected system
		var loc = starc.chart.lookup(starc.ref.loc());
		if (loc === undefined) {
			return;
		}
		showinfo(loc.sys);
		var rc = loc.sys.coords;
		var cluster = loc.clstr;

		var chart = starc.chart.data;
		var cs = chart.scale;
		var markfn, afterfn;
		var l = chart.systems.length;

		var g = $("#jumplines", starc.chart.svg);
		g.empty();
		for (var i = 0; i < l; i++) {
			var e = chart.systems[i];
			var sys = e.sys;
			var opacity = (e.clstr == cluster ? 1 : starc.prefs.view.unreachableOpacity).toString();
			e.gsys.setAttribute("opacity", opacity);
			e.gbar.setAttribute("opacity", opacity);
			if (starc.syskey(sys) == starc.syskey(loc.sys)) continue;
			var sc = sys.coords;
			var d = Math.sqrt(
					Math.pow(rc[0]-sc[0],2) +
					Math.pow(rc[1]-sc[1],2) +
					Math.pow(rc[2]-sc[2],2));
			if (d <= starc.prefs.ship.MaxJumpDist) {
				// connect nearby systems within jump range
				var line = starc.node("line", {
					x1: cs*loc.sx,
					y1: cs*loc.sy,
					x2: cs*e.sx,
					y2: cs*e.sy,
					stroke: "white",
					opacity: 0.8,
					"stroke-width": 0.1
				}, starc.chart.svg.getAttribute("xmlns"));
				g.append(line);

				// mark with a dot
				var grp = starc.node("g", tr(cs*e.sx, cs*e.sy), starc.chart.svg.getAttribute("xmlns"));
				grp.child("circle", {
					r: chartparams.dotr*1.5,
					fill: "white",
					opacity: 0.8
				});
				g.append(grp);
			}
		}

		// pulsing marker for selected system
		var marker = starc.node("g", tr(cs*loc.sx, cs*loc.sy), starc.chart.svg.getAttribute("xmlns"));
		var grp = marker.child("g", {});
		grp.child("animate", {
			attributeType: "xml",
			attributeName: "opacity",
			values: "1;0.5;0.5",
			keyTimes: "0;0.5;1",
			to: "0.0",
			dur: "2s",
			repeatCount: "indefinite"
		});
		var d = chartparams.dotr*1.8;
		grp.child("circle", {
			r: d*1.1,
			fill: "white"
		});
		g.append(marker);

		if (center != false) {
			// center system
			var bcr = grp.getBoundingClientRect();
			var gx = (bcr.left+bcr.right)/2;
			var gy = (bcr.top+bcr.bottom)/2;
			var cx = window.innerWidth/2;
			var cy = window.innerHeight/2;
			var p = chartparams.container.position();
			var px = p.left + (cx-gx);
			var py = p.top + (cy-gy);
			if (animate != false) {
				chartparams.container.animate({left: p.left+cx-gx, top: p.top+cy-gy});
			} else {
				chartparams.container.css({left: p.left+cx-gx, top: p.top+cy-gy});
			}
		}
	}

	// chart specific data, updated in prepareChart
	var idmap = {};
	var chartshift = {sx: 0, sy: 0};

	function prepareChart() {
		var tmpl = chartparams.tmpl[0];

		var chart = starc.chart.data;
		var cs = chart.scale;
		var l = chart.systems.length;
		var dx = 0, dy = 0;
		for (var i = 0; i < l; i++) {
			var e = chart.systems[i];
			if (dx < Math.abs(e.sx)) {
				dx = Math.abs(e.sx);
			}
			if (dy < Math.abs(e.sy)) {
				dy = Math.abs(e.sy);
			}
		}
		var cy = chart.coords[1];
		chartshift = planepos(chart.coords);
		chartshift.sx = -chartshift.sx;
		chartshift.sy = -chartshift.sy;
		dx += 10;
		dx *= starc.prefs.view.scale;
		dy *= starc.prefs.view.scale;
		var gap = 1;
		var boxl = Math.floor(-dx-gap);
		var boxt = Math.floor(-dy-gap);
		var boxw = Math.ceil(dx+gap) - boxl;
		var boxh = Math.ceil(dy+gap) - boxt;
		var vbox = boxl.toString() + "," + boxt.toString() + "," + boxw.toString() + "," + boxh.toString();

		var xidmap = {};
		var svg = starc.node(tmpl, {
			id: "chart",
			preserveAspectRatio: "xMidYMid",
			style: "width:" + boxw.toString() + "em; height:" + boxh.toString() + "em;",
			viewBox: vbox
		});
		var root = svg.child("g", { transform: "scale(" + starc.prefs.view.scale.toString() + ")" });

		var gsect = root.child("g", { id: "sectorlabels" });
		var gbar = root.child("g", { id: "systembars" });
		root.child("g", { id: "routes", opacity: 0.7 });
		root.child("g", { id: "jumplines" });
		var gsysm = root.child("g", { id: "systems" });
		var ghilite = root.child("g", { id: "highlight" });
		var gselect = root.child("g", { id: "select" });

		var l = chart.systems.length;
		for (var i = 0; i < l; i++) {
			var e = chart.systems[i];
			var sys = e.sys;

			xidmap[starc.syskey(sys)] = e;

			var systr = tr(cs*e.sx, cs*e.sy);

			e.gsys = gsysm.child("g", systr);
			// dot marker
			e.gdot = e.gsys.child("circle", {
				"r": chartparams.dotr,
				"fill": prefColor("dot", sys)
			});

			// system label
			e.gtext = e.gsys.child("text", $.extend({}, chartparams.syslabel, {
				"class": sys.stations == null ? "stationless" : "inhabited",
				"fill": prefColor("label", sys)
			}));
			e.gtext.text(e.n);

			// system height bar
			var sy = sys.coords[1];
			e.gbar = gbar.child("g", systr);
			if (sy != cy) {
				e.gbar.child("line", $.extend({}, chartparams.sysbar, {
					x1: 0,
					y1: (sy - cy) * chartparams.barscale,
					x2: 0,
					y2: 0
				}));
			}

			// click box
			var clk = gselect.child("g", tr(cs*e.sx, cs*e.sy)).child("rect", {
				"fill-opacity": 0,
				x: -0.5,
				y: -0.6,
				width: e.w,
				height: 1.0
			});
			clk.system = sys.system;
			$(clk).on("click", function(evt) {
				if (!dragging) {
					var esys = starc.chart.lookup(evt.currentTarget.system);
					if (esys !== undefined) {
						userSystemSelect(esys.sys, true);
					}
				}
			}).on("dblclick", function(evt) {
				if (!dragging) {
					var loc = evt.currentTarget.system;
					starc.ref.set({loc: loc, view: loc});
					starc.chart.load();
				}
			});
		}

		// sector labels
		l = chart.sectors.length;
		for (var i = 0; i < l; i++) {
			var sec = chart.sectors[i];
			gsect.child("g", tr(cs*sec.sx, cs*sec.sy)).child("text", $.extend({}, chartparams.sectlabel, {
				"class": "sector"
			})).text(sec.n);
		}

		// replace map
		if (starc.chart.svg) {
			starc.chart.svg.parentNode.removeChild(starc.chart.svg);
		}

		chartparams.container.append(svg);

		starc.chart.svg = svg;
		idmap = xidmap;
	};

	function screenpos(sys) {
		// get a reasonable position of sys on screen
		var e = starc.chart.lookup(sys.system);
		if (e) {
			return {
				sx: e.sx,
				sy: e.sy,
				visible: true
			};
		}
		var p = planepos(sys.coords);
		p.sx += chartshift.sx;
		p.sy += chartshift.sy;
		p.visible = false;
		return p;
	};

	function planepos(p) {
		// return screen position of system
		// NB: chart shift must be accounted for
		return {
			sx: p[0],
			sy: -p[2]
		};
	}

	function rounddist(d) {
		// round dist to 2 digits
		return Math.round(d*100)/100;
	}

	function tr(x, y) {
		return { transform: "translate(" + x.toString() + " " + y.toString() + ")" };
	};

})(window.starc = window.starc || {}, jQuery);
