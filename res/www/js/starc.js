(function( starc, $, undefined ) {

	var apibase = $("meta[property='harmless:apiurl']").attr('content');
	starc.prefix = $("meta[property='harmless:prefix']").attr('content');

	starc.node = function(t, attrs, xmlns) {
		var n;
		if (typeof t === 'string') {
			if (xmlns == undefined) {
				n = document.createElement(t);
			} else {
				n = document.createElementNS(xmlns, t);
			}
		} else {
			n = t.cloneNode(true);
			xmlns = xmlns || n.getAttribute("xmlns");
		}
		if (attrs !== undefined) {
			for (var prop in attrs) {
				if (attrs.hasOwnProperty(prop)) {
					n.setAttribute(prop, attrs[prop].toString());
				}
			}
		}
		n.text = function(t) {
			n.appendChild(document.createTextNode(t));
		}
		if (xmlns == "") {
			n.child = function(t, attrs) {
				var c = starc.node(t, attrs);
				n.appendChild(c);
				return c;
			}
		} else {
			n.child = function(t, attrs) {
				var c = starc.node(t, attrs, xmlns);
				n.appendChild(c);
				return c;
			}
		}
		return n;
	};

	starc.ajaxError = function(op, xhr, textstatus, errthrown) {
		console.log(op);
		console.log(xhr, textstatus, errthrown);
		starc.showError(op, textstatus + ": " + errthrown);
	};

	starc.showError = function(op, msg) {
		var div = $("div#errormessage");
		if (div.length == 0) {
			div = starc.dlgbox("error", {id:"errormessage"});
		}
		$("<div>").appendTo(div).append(
			$("<i>", { "class": "fa fa-warning" }),
			$("<span>").text(" "+ op + ":"),
			$("<span>").text(" "+ msg)
		);
	};

	starc.reportSuccess = function(msg) {
		var div = starc.dlgbox("success");
		$("<div>").appendTo(div).append(
			$("<i>", { "class": "fa fa-info-circle" }),
			$("<span>").text(" "+ msg)
		);
	};

	starc.dlgbox = function(maincls, params) {
		var box = $("<div>", { "class": "dialogbox "+maincls });
		box.append($("<button>", { "class": "close" }).append(
			$("<i>", {
				"title": "dismiss",
				"class": "fa fa-times"
			})).on("click", function(e) {
				box.remove();
			}));
		box.appendTo(document.body);
		return $("<div>", $.extend({"class":"messages"}, params)).appendTo(box);
	}

	starc.defs = {
		station_type: [
			"Civilian Outpost",
			"Commercial Outpost",
			"Industrial Outpost",
			"Scientific Outpost",
			"Unknown Outpost",
			"Unsanctioned Outpost",
			"Coriolis Starport",
			"Ocellus Starport",
			"Orbis Starport"
		],
		station_population: [
			"tiny",
			"small",
			"medium", //?
			"large",
			"very large",
			"huge"
		],
		economy: [
			"None",
			"Agriculture",
			"Extraction",
			"High Tech",
			"Industrial",
			"Military",
			"Refinery",
			"Service",
			"Terraforming",
			"Tourism"
		],
		allegiance: [
			"None",
			"Alliance",
			"Empire",
			"Federation",
			"Independent"
		],
		government: [
			"None",
			"Anarchy",
			"Colony",
			"Communism",
			"Confederacy",
			"Cooperative",
			"Corporate",
			"Democracy",
			"Dictatorship",
			"Feudal",
			"Patronage",
			"Theocracy"
		],
		security: [ "none", "low", "medium", "high" ]
	};

	starc.defaultPrefs = {
		ref: {
			loc: "Sol",
			view: "Sol",
			waypts: [],
		},
		cmdr: "Jameson",

		ship: {
			MaxJumpDist: 6.97,
			RoutingMode: "x",
			MaxJumpFuel: 0.8,
			FuelTank: 2.0,
			FuelPower: 2.0,
			FuelUnits: 64
		},

		view: {
			radius: 40,
			scale: 1.2,
			unreachableOpacity: 0.4,
			colors: [
				// https://forums.frontier.co.uk/showthread.php?t=74985
				{Type:"dot", Key:"economy", Value:"Military",     Color:"#9482C9"},
				{Type:"dot", Key:"economy", Value:"Extraction",   Color:"#C41F3B"},
				{Type:"dot", Key:"economy", Value:"Refinery",     Color:"#FF7D0A"},
				{Type:"dot", Key:"economy", Value:"Industrial",   Color:"#FFF569"},
				{Type:"dot", Key:"economy", Value:"Agriculture",  Color:"#ABD473"},
				{Type:"dot", Key:"economy", Value:"Terraforming", Color:"#ABD473"},
				{Type:"dot", Key:"economy", Value:"High Tech",    Color:"#69CCF0"},
				{Type:"dot", Key:"economy", Value:"Service",      Color:"#0070DE"},
				{Type:"dot", Key:"economy", Value:"Tourism",      Color:"#F58CBA"},

				{Type:"label", Key:"security",   Value:"low",     Color:"#FF4444"},
				{Type:"label", Key:"government", Value:"Anarchy", Color:"#FF4444"},

				{Type:"label", Key:"allegiance", Value:"Empire",      Color:"#69CCF0"},
				{Type:"label", Key:"allegiance", Value:"Federation",  Color:"#99FF99"},
				{Type:"label", Key:"allegiance", Value:"Alliance",    Color:"#FFCC66"},
				{Type:"label", Key:"allegiance", Value:"Independent", Color:"#AE8253"}
			]
		}
	};
	starc.prefs = $.extend({}, starc.defaultPrefs);

	function encPathComp(s) {
		return encodeURIComponent(s).replace(/%20/g, "+");
	}

	function decPathComp(s) {
		return decodeURIComponent(s.replace(/\+/g, "%20"));
	}

	starc.ref = (function() {
		var mappfx = starc.prefix + "/map/";
		var storkey = "harmless.ref";
		var editstorkey = "harmless.editref";

		var r0 = JSON.parse(window.localStorage.getItem(storkey)) || {};
		var loc = r0.loc || "Sol";
		var editloc = r0.editloc || "Sol";
		var view = r0.view || "Sol";
		var waypts = r0.waypts || [];

		var routepfx = "route/";
		var systempfx = "system/";

		(function() {
			var pn = window.location.pathname;
			if (pn.indexOf(mappfx) == 0) {
				pn = pn.slice(mappfx.length);
				if (pn.indexOf(routepfx) == 0) {
					var r = pn.slice(routepfx.length).split('/');
					if (r.length != 0) {
						var last = r[r.length-1];
						var forceloc;
						if (last.indexOf("@") == 0) {
							forceloc = decPathComp(last.slice(1));
							r = r.slice(0, r.length-1);
						}
						var rdec = [];
						var rloc;
						for (var i = 0; i < r.length; i++) {
							var d = decPathComp(r[i]);
							if (d != "") {
								rloc = d;
								rdec.push(d);
							}
						}
						if (forceloc) {
							loc = forceloc;
						} else if (rloc) {
							loc = rloc;
						}
						waypts = rdec || waypts;
					}
				} else if (pn.indexOf(systempfx) == 0) {
					loc = decPathComp(pn.slice(systempfx.length)) || loc;
				}
				window.localStorage.setItem(editstorkey, loc);
				console.log(pn);
			}
		})();

		function save() {
			var r = {
				loc: loc,
				view: view,
				waypts: waypts
			};
			window.localStorage.setItem(storkey, JSON.stringify(r));
		}

		return {
			editloc: function() {
				return window.localStorage.getItem(editstorkey);
			},
			loc: function() {
				return loc;
			},
			view: function() {
				return view;
			},
			waypts: function() {
				return waypts;
			},
			set: function(o) {
				if (o.loc) {
					window.localStorage.setItem(editstorkey, o.loc);
				}
				loc = o.loc || loc;
				view = o.view || view;
				waypts = o.waypts || waypts;
				var u;
				if (waypts.length != 0) {
					u = mappfx + routepfx;
					for (var i = 0; i < waypts.length; i++) {
						u += encPathComp(waypts[i]) + "/";
					}
					if (waypts[waypts.length-1] != loc) {
						u += "@" + encPathComp(loc);
					}
				} else {
					u = mappfx + systempfx + encPathComp(loc);
				}
				window.history.pushState({}, o.loc, u);
				save();
			}
		};
	})();

	try {
		var pref = JSON.parse(window.localStorage.getItem("harmless.prefs"));
		var oldk = ["locid", "viewid", "locp", "viewp"];
		for (var i = 0; i < oldk.length; i++) {
			try { delete pref[oldk]; } catch (err) {}
		}
		starc.prefs = $.extend({}, starc.defaultPrefs, pref);
	}
	catch (err) {}

	starc.saveprefs = function() {
		window.localStorage.setItem("harmless.prefs", JSON.stringify(starc.prefs));
	};

	starc.hashparams = function () {
		var h = window.location.hash;
		if (h == "") return {};
		return starc.params(h.substr(1));
	};

	starc.params = function (fragment) {
		if (fragment == "") return {};
		var a = fragment.split("&");
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p=a[i].split('=');
			if (p.length != 2) continue;
			b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
		}
		return b;
	};

	starc.apiurl = function(api, p) {
		var q = apibase + "/" + api;
		if (p !== undefined) {
			q += "?" + $.param(p);
		}
		return q;
	};

	starc.apicoordstr = function(sc) {
		return sc[0].toString() +
				";" + sc[1].toString() +
				";" + sc[2].toString();
	};

	starc.syskey = function(sys) {
		return sys.system.toLowerCase();
	};

	starc.uicoordstr = function(sc) {
		return Math.round(sc[0]).toString() +
				" : " + Math.round(sc[1]).toString() +
				" : " + Math.round(sc[2]).toString();
	};

	starc.parsePop = function(s) {
		if (s.toUpperCase() == "N/A")
			return -1;
		var xs = s.replace(/,/g, "");
		if (xs == "") {
			return 0;
		}
		var v = xs.split(/ +/);
		var num = "", mul = 0;
		if (v.length > 0) {
			num = v[0];
		}
		if (v.length == 1) {
			mul = 1;
		} else if (v.length == 2) {
			var x = v[1].toLowerCase();
			if (x == "million" || x == "m") {
				mul = 1e6;
			}
			if (x == "billion" || x == "b") {
				mul = 1e9;
			}
		}
		if (isNaN(num) || mul == 0) {
			starc.showError("invalid number", s);
			return -1;
		}
		return parseFloat(num) * mul;
	};

	starc.numFmt = function(n) {
		if (isNaN(n)) return "N/A";
		var parts = n.toString().split(".");
		var p0 = parts[0];
		var p1 = parts.length > 1 ? "." + parts[1] : "";
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(p0)) {
			p0 = p0.replace(rgx, "$1,$2");
		}
		return p0+p1;
	};

	starc.fmtPop = function(v) {
		if (isNaN(v) || v < 0)
			return "N/A";
		if (v < 1e6) {
			return starc.numFmt(v);
		} else if (v < 1e9) {
			return starc.numFmt(v/1e6) + " Million";
		}
		return starc.numFmt(v/1e9) + " Billion";
	};

	var months = [
		"January", "February", "March",
		"April", "May", "June",
		"July", "August", "September",
		"October", "November", "December"
	];
	starc.fmtRelTime = function(v) {
		var d = new Date(v);
		var diff = (Date.now() - d)/1000;
		if (diff < 120) {
			return "just now";
		}
		diff = Math.floor(diff/60);
		if (diff < 60) {
			if (diff == 1)
				return "one minute ago"
			return diff.toString() + " minutes ago";
		}
		diff = Math.floor(diff/60);
		if (diff < 24) {
			if (diff == 1)
				return "one hour ago"
			return diff.toString() + " hours ago";
		}
		diff = Math.floor(diff/24);
		if (diff < 7) {
			if (diff == 1)
				return "one day ago"
			return diff.toString() + " days ago";
		}
		return "on " + d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
	};
})(window.starc = window.starc || {}, jQuery);
