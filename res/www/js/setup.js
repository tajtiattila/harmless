(function( starc, $, undefined ) {

	var typeTags = [
		"dot",
		"label"
	];
	var keyTags = [
		"allegiance",
		"economy",
		"government",
		"security"
	];

	$("input.starc-setup[type='number'],select.starc-setup").each(function (index) {
		try {
			var x = $(this).attr("name").split('.');
			$(this).val(starc.prefs[x[0]][x[1]]);
		}
		catch(err) { console.log(err); }
	});
	$("input.starc-setup[type='number']").change(function (evt) {
		savepref(this, parseFloat);
	});
	$("select.starc-setup").change(function (index) {
		savepref(this);
	});

	(function() {
		$("#colorsetup").sortable();
		$("#colorsetup").disableSelection().keypress(function(e){
			if ( e.which == 13 ) return false;
		});
		$("#colorsetup").after(
			button("plus", "add line", {}, function(evt){
				var e = addcolorentry({});
				$("#Type", e).focus();
				}),
			button("check", "save", {}, function(evt){
				savecolors();
				}),
			button("undo", "revert", {}, function(evt){
				loadcolors(starc.prefs.view.colors);
				}),
			button("recycle", "defaults", {}, function(evt){
				loadcolors(starc.defaultPrefs.view.colors);
				})
		);
	})();
	function loadcolors(v) {
		$("#colorsetup").empty();
		for (var i = 0; i < v.length; i++) {
			addcolorentry(v[i]);
		}
	};
	loadcolors(starc.prefs.view.colors);

	function savepref(obj, fn) {
		var v = $(obj).val();
		if (fn !== undefined) {
			v = fn(v);
		}
		var x = $(obj).attr("name").split('.');
		starc.prefs[x[0]][x[1]] = v;
		starc.saveprefs();
	}

	function addcolorentry(e) {
		var n = starc.node("div", {
			"class": "colorelem"
		});
		$(n.child("input", {
			"class": "color-type",
			"id": "Type",
			"type": "text"
		})).val(e.Type).autocomplete({ source: typeTags });
		var nk = n.child("input", {
			"class": "color-key",
			"id": "Key",
			"type": "text"
		})
		var nv = n.child("input", {
			"class": "color-value",
			"id": "Value",
			"type": "text"
		})
		$(nk).val(e.Key).autocomplete({ source: keyTags });
		$(nv).val(e.Value).autocomplete({ source: function(req, respf) {
			var k = $(nk).val();
			if (typeof(k) == "string") {
				k = k.toLowerCase();
			}
			if (starc.defs.hasOwnProperty(k)) {
				var v = starc.defs[k];
				var resp = [];
				for (var i = 0; i < v.length; i++) {
					if (v[i].toLowerCase().indexOf(req.term.toLowerCase()) != -1) {
						resp.push(v[i]);
					}
				}
				respf(resp);
			}
		}});
		$(n.child("input", {
			"class": "color-color",
			"id": "Color",
			"type": "text"
		})).val(e.Color);
		$(n).append(button("times", "", {"class": "sortbtn"}, function(evt) {
			n.parentNode.removeChild(n);
		}));
		$("#colorsetup").append(n);
		return n;
	}

	function button(icon, label, attrs, fn) {
		var btn = starc.node("button", attrs);
		if (icon != "") {
			btn.child("i", {"class": "fa fa-" + icon});
		}
		if (label != "") {
			if (icon != "") {
				label = " " + label;
			}
			btn.text(label);
		}
		$(btn).click(function(evt) {
			evt.preventDefault();
			fn(evt);
		});
		return btn;
	}

	function savecolors() {
		var colors = [];
		$("#colorsetup .colorelem").each(function(index) {
			colors.push({
				Type:  $("#Type",  this).val(),
				Key:   $("#Key",   this).val(),
				Value: $("#Value", this).val(),
				Color: $("#Color", this).val(),
			});
		});
		starc.prefs.view.colors = colors;
		starc.saveprefs();
	}

})(window.starc = window.starc || {}, jQuery);
