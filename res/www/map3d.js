var $camera, $scene, $cameraHud, $sceneHud, $renderer, $controls;

var $stars, $dbg;

var $wantUpdate = true;

function Star(data, material) {
	this.data = data;
	this.name = data.sys;
	this.sprite = new THREE.Sprite( material );
	this.sprite.position.set(data.x, data.y, data.z);
	this.hudSprite = makeTextSprite(data.sys, {
		fontface: "Oswald",
		textColor: {r:255, g:255,b:255, a:1.0},
		strokeWidth:4
	});
	// offset for labels
	var hudimg = this.hudSprite.material.map.image;
	this.dx = hudimg.width/2;
	this.dy = hudimg.height/2;
	this.shownHud = false;
}

function fdist(dist) {
	return Math.round(dist * 100) / 100 + "Ly";
}

function Debug(elem) {
	this.domElement = document.createElement("div");
	this.domElement.style.color = "rgb(64,255,64)";
	this.domElement.style.background = "rgba(0,0,0,0.5)";
	this.domElement.style.position = "absolute";
	this.domElement.style.left = 0;
	this.domElement.style.bottom = 0;
	this.domElement.style.paddingLeft = "0.5em";
	this.domElement.style.paddingRight = "0.5em";
	this.clear = function () {
		while (this.domElement.hasChildNodes()) {
			this.domElement.removeChild(this.domElement.lastChild);
		}
	}
	this.log = function () {
		var msg = "";
		for (var i = 0; i < arguments.length; i++) {
			if (i == 0) {
				msg = arguments[0];
			} else {
				msg = msg + " " + arguments[i];
			}
		}
		var p = document.createElement("p");
		p.style.marginTop = "0";
		p.style.marginBottom = "0";
		p.style.opacity = 1.0;
		p.style.font = "Bold 12px Consolas";
		p.appendChild(document.createTextNode(msg));
		this.domElement.appendChild(p);
	}
}

window.onload = function() {
	$("#fontloader").hide();
	$("#loading").hide();
	$.ajax("api/mapdata", {
		"success": function(data) {
			init(data);
			animate();
		}
	})
}

function init(data) {
	$dbg = new Debug();

	var width = window.innerWidth;
	var height = window.innerHeight;

	$camera = new THREE.PerspectiveCamera( 60, width / height, 1, 2100 );
	//$camera.position.z = 1500;
	$cameraHud = new THREE.OrthographicCamera( - width / 2, width / 2, height / 2, - height / 2, 1, 1000 );
	$cameraHud.position.z = 10;

	$scene = new THREE.Scene();
	$sceneHud = new THREE.Scene();

	var starimg = THREE.ImageUtils.loadTexture( "starsprite.png", undefined, function() { $wantUpdate = true; } );
	var starmat = new THREE.SpriteMaterial( { map: starimg, color: 0xffffff, blending: THREE.AdditiveBlending } );
	$stars = [];
	for (var i = 0; i < data.length; i++) {
		var star = new Star(data[i], starmat);
		$scene.add(star.sprite);
		$stars[i] = star;
	}

	// renderer

	$renderer = new THREE.WebGLRenderer();
	$renderer.setSize( window.innerWidth, window.innerHeight );
	$renderer.autoClear = false; // To allow render overlay on top

	// controls
	$controls = new THREE.OrbitControls( $camera, $renderer.domElement );
	THREE.EventDispatcher.prototype.apply(THREE.OrbitControls.prototype);

	$controls.target.set(-22.843750, 36.531250, -1.187500); // Eranin

	updateHud();

	document.body.appendChild( $dbg.domElement );
	document.body.appendChild( $renderer.domElement );

	// events
	THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) });
	window.addEventListener( 'resize', onWindowResize, false );

	$controls.addEventListener('change', function() { $wantUpdate = true });
}

function animate() {
	requestAnimationFrame(animate);
	render();
	update();
}

function render() {
	if ($wantUpdate) {
		$renderer.clear();
		$renderer.render( $scene, $camera );
		updateHud();
		$renderer.clearDepth();
		$renderer.render( $sceneHud, $cameraHud );
		$wantUpdate = false;
	}
}

function update() {
	$controls.update();
}

function updateHud() {
	$dbg.clear();
	$camera.updateProjectionMatrix();
	var matrix = new THREE.Matrix4();
	matrix.getInverse($camera.matrixWorld);
	var whalf = window.innerWidth / 2;
	var hhalf = window.innerHeight / 2;
	var pos = new THREE.Vector3();
	var dpos = new THREE.Vector3();
	var camdist = $camera.position.clone(dpos).sub($controls.target).length();
	var labeldist = camdist * 0.5;
	var vis, posz;
	$dbg.log(fdist(labeldist));
	for (var i = 0; i < $stars.length; i++) {
		var star = $stars[i];
		pos.copy(star.sprite.position);
		pos.applyMatrix4(matrix);
		var hide = 0 < pos.z;
		if (!hide) {
			var dist = $controls.target.clone(dpos).sub(star.sprite.position).length();
			var distf = Math.max(0.0, 2.0-Math.max(1.0, dist/labeldist));
			var signif = star.data.significance * 0.6;
			var signf = Math.min(1.0, Math.max(0.0, 2.0-labeldist/signif));
			vis = signf * distf;
			vis = vis * vis;
			/*if (star.name == "Eranin" || star.name == "Asellus Primus" || star.name == "Jula Oh") {
				$dbg.log(star.name, distf, signf, fdist(dist));
			}*/
			if (vis < 0.2) {
				hide = true;
			}
		}
		if (hide) {
			if (star.shownHud) {
				$sceneHud.remove(star.hudSprite);
				star.shownHud = false;
			}
			continue;
		}
		posz = pos.z;
		pos.applyProjection($camera.projectionMatrix);
		pos.x = (pos.x + 0)* whalf + star.dx;
		pos.y = (pos.y + 0)* hhalf;// + star.dy;
		if (!star.shownHud) {
			$sceneHud.add(star.hudSprite);
			star.shownHud = true;
		}
		star.hudSprite.material.opacity = vis;
		pos.z = posz;
		star.hudSprite.position.copy(pos);
	}
}

function onWindowResize() {

	var width = window.innerWidth;
	var height = window.innerHeight;

	$camera.aspect = width / height;
	$camera.updateProjectionMatrix();

	$cameraHud.left = - width / 2;
	$cameraHud.right = width / 2;
	$cameraHud.top = height / 2;
	$cameraHud.bottom = - height / 2;
	$cameraHud.updateProjectionMatrix();

	$renderer.setSize( window.innerWidth, window.innerHeight );

	$wantUpdate = true;
}

function makeTextSprite( message, parameters )
{
	if ( parameters === undefined ) parameters = {};

	var fontface = parameters.hasOwnProperty("fontface") ?
		parameters["fontface"] : "Arial";
	var fontsize = parameters.hasOwnProperty("fontsize") ?
		parameters["fontsize"] : 14;

	var borderThickness = parameters.hasOwnProperty("borderThickness") ?
		parameters["borderThickness"] : 4;

	var borderColor = parameters.hasOwnProperty("borderColor") ?
		parameters["borderColor"] : { r:0, g:0, b:0, a:1.0 };

	var textColor = parameters.hasOwnProperty("textColor") ?
		parameters["textColor"] : { r:255, g:255, b:255, a:1.0 };

	var strokeColor = parameters.hasOwnProperty("strokeColor") ?
		parameters["strokeColor"] : { r:0, g:0, b:0, a:0.75 };

	var strokeWidth = parameters.hasOwnProperty("strokeWidth") ?
		parameters["strokeWidth"] : 0;

	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	context.font = "Bold " + fontsize + "px " + fontface;

	// get size data (height depends only on font size)
	var w = context.measureText( message ).width;
	var h = fontsize * 1.4;
	w += h;
	canvas.width = w;
	canvas.height = h;

	// get new context for resized canvas
	context = canvas.getContext('2d');
	context.font = "Bold " + fontsize + "px " + fontface;

	if (false) {
		// crosshair at center of canvas
		context.strokeStyle = "rgba(255,0,0,1.0)";
		context.lineWidth = 2;
		var cx = w/2;
		var cy = h/2;
		var r = 10;
		context.beginPath();
		context.moveTo(cx-r, cy);
		context.lineTo(cx+r, cy);
		context.moveTo(cx, cy-r);
		context.lineTo(cx, cy+r);
		context.closePath();
		context.stroke();
		context.strokeStyle = "rgba(0,0,0,0)";
		context.lineWidth = 0;
	}

	// border color
	if (strokeWidth != 0) {
		// background stroke
		context.strokeStyle = strokeColor;
		context.lineWidth = strokeWidth;
		context.strokeText(message, h/2, fontsize);
	}

	// text color
	context.fillStyle = "rgba(" + textColor.r + "," + textColor.g + ","
								  + textColor.b + "," + textColor.a + ")";
	context.fillText(message, h/2, fontsize);

	// canvas contents will be used for a texture
	var texture = new THREE.Texture(canvas)
	texture.needsUpdate = true;
	var material = new THREE.SpriteMaterial( { map: texture } );
	var sprite = new THREE.Sprite( material );
	//sprite.scale.set(200,100,1.0);
	sprite.scale.set(w, h, 1);
	return sprite;
}
