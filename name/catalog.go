package name

import (
	"strings"
)

func CatalogName(s string) (string, bool) {
	if n := len(s); n > 2 && !isalpha(rune(s[n-2])) && isalpha(rune(s[n-1])) {
		// check with alphabetic suffix
		var x, y string
		if s[n-2] == ' ' {
			x, y = s[:n-2], s[n-2:]
		} else {
			x, y = s[:n-1], s[n-1:]
		}
		if f, ok := catalogName(x); ok {
			return f + y, ok
		}
	}
	return catalogName(s)
}

func isalpha(b rune) bool {
	return 'a' <= lc(b) && lc(b) <= 'z'
}

func isdigit(b rune) bool {
	return '0' <= b && b <= '9'
}

func isalnum(b rune) bool {
	return isdigit(b) || isalpha(b)
}

func catalogName(s string) (string, bool) {
	l := strings.ToLower(s)
	for spec, cat := range catSpec {
		if len(l) > len(spec) && l[:len(spec)] == spec {
			return cat.f(cat.name, s[len(spec):])
		}
	}
	for i, r := range s {
		if !isalpha(r) {
			if 0 < i && i < len(s) {
				if cat, ok := catMap[l[:i]]; ok {
					return cat.f(cat.name, s[i:])
				}
			}
			return s, false
		}
	}
	return s, false
}

func catbd(name, s string) (string, bool) {
	r, ok := catdigitgroups(name, s, "+")
	if !ok {
		r, ok = catdigitgroups(name, s, "-")
	}
	return r, ok
}

func catcpc(name, s string) (string, bool) {
	return catdigitgroups(name, s, " ")
}

func catcd(name, s string) (string, bool) {
	return catdigitgroups(name, s, "-")
}

func catwise(name, s string) (string, bool) {
	x := s
	if x[0] == ' ' {
		x = x[1:]
	}
	var (
		spec   rune
		pd, sd bool
	)
	for _, r := range x {
		switch {
		case r == '+' || r == '-':
			if !pd || spec != 0 {
				return s, false
			}
			spec = r
		case '0' <= r && r <= '9':
			if spec != 0 {
				sd = true
			} else {
				pd = true
			}
		case r == 'J' || r == 'j':
			if pd {
				// j must appear first
				return s, false
			}
		default:
			return s, false
		}
	}

	if !pd {
		// no digits
		return s, false
	}
	if spec != 0 && !sd {
		// trailing +/-
		return s, false
	}
	return name + " " + strings.ToUpper(x), true
}

func catdigits(name, s string) (string, bool) {
	x := s
	if x[0] == ' ' {
		x = x[1:]
	}
	for _, r := range x {
		if r < '0' || '9' < r {
			return s, false
		}
	}
	return name + " " + x, true
}

func catdigitsdash(name, s string) (string, bool) {
	return catdigitsonesep(name, s, '-', true)
}

func catfrac(name, s string) (string, bool) {
	return catdigitsonesep(name, s, '.', false)
}

func catdigitsonesep(name, s string, sep rune, mustsep bool) (string, bool) {
	x := s
	if x[0] == ' ' {
		x = x[1:]
	}
	var pd, fs, sd bool
	for _, r := range x {
		switch {
		case '0' <= r && r <= '9':
			if fs {
				sd = true
			} else {
				pd = true
			}
		case r == sep:
			if fs {
				return s, false
			}
			fs = true
		default:
			return s, false
		}
	}
	if !pd || (fs && !sd) || (mustsep && !fs) {
		return s, false
	}
	return name + " " + x, true
}

func catdigitgroups(name, s, sep string) (string, bool) {
	// TODO check if rest is digits
	s = strings.TrimPrefix(s, sep)
	if s[0] == ' ' || ('0' <= s[0] && s[0] <= '9') {
		return name + sep + strings.Join(strings.Fields(strings.ToUpper(s)), " "), true
	}
	return s, false
}

type cat struct {
	name string
	f    func(name, suffix string) (string, bool)
}

var catalogs = []cat{
	{"2MASS", catwise},
	{"ADS", catdigits},
	{"AGKR", catdigits},
	{"BD", catbd},
	{"CD", catcd},
	{"CPC", catcpc},
	{"CPD", catcd},
	{"CPO", catdigits},
	{"DX", catdigits},
	{"G", catdigitsdash},
	{"GD", catdigits},
	{"Gl", catfrac},
	{"Gliese", catfrac},
	{"HD", catdigits},
	{"HDS", catdigits},
	{"HIP", catdigits},
	{"HR", catdigits},
	{"L", catdigitsdash},
	{"Lalande", catdigits},
	{"LAWD", catdigits},
	{"LB", catdigits},
	{"LDS", catdigits},
	{"LFT", catdigits},
	{"LHS", catdigits},
	{"LP", catdigitsdash},
	{"LTT", catdigits},
	{"Luyten", catdigitsdash},
	{"MCC", catdigits},
	{"NLTT", catdigits},
	{"NN", catdigits},
	{"NSV", catdigits},
	{"PLX", catdigits},
	{"PPM", catdigits},
	{"PSPF-LF", catdigits},
	{"RBS", catdigits},
	{"Ross", catdigits},
	{"RST", catdigits},
	{"SAO", catdigits},
	{"SDSS", catwise},
	{"Smethells", catdigits},
	{"SPF-LF", catdigits},
	{"SPOCS", catdigits},
	{"SRS", catdigits},
	{"Stein", catdigits},
	{"STF", catdigits},
	{"StHA", catdigits},
	{"StKM", catdigitsdash},
	{"Struve", catdigits},
	{"TT", catdigits},
	//{"TYC", catdigits},
	{"UBV", catdigits},
	{"UGP", catdigits},
	{"VVO", catdigits},
	{"WD", catdigitsdash},
	{"WISE", catwise},
	{"Wo", catdigits},
	{"Wolf", catdigits},
}

var (
	catMap  = make(map[string]cat)
	catSpec = make(map[string]cat)
)

func init() {
	for _, cat := range catalogs {
		lname := strings.ToLower(cat.name)
		catMap[lname] = cat
		for _, r := range lname {
			if r < 'a' || 'z' < r {
				catSpec[lname] = cat
				break
			}
		}
	}
}
