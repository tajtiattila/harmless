package name

import (
	"strings"
)

type Kind int

const (
	Unknown Kind = iota

	Constellation
	Catalog
	Generated
)

func Name(s string) string {
	x, _ := NameKind(s)
	return x
}

func NameKind(s0 string) (string, Kind) {
	s := strings.Join(strings.Fields(s0), " ")

	if x, ok := ConstName(s); ok {
		return x, Constellation
	}
	if x, ok := CatalogName(s); ok {
		return x, Catalog
	}
	if x, ok := GeneratedName(s); ok {
		return x, Generated
	}

	uc := false
	for _, r := range s {
		if 'A' <= r && r <= 'Z' {
			uc = true
		}
	}

	if !uc {
		return Title(s), Unknown
	}
	return s, Unknown
}

func Title(s string) string {
	prev := ' '
	return strings.Map(
		func(r rune) rune {
			p := prev
			prev = r
			if p == ' ' {
				if 'a' <= r && r <= 'z' {
					r += 'A' - 'a'
				}
			}
			return r
		},
		s)
}
