package name

import (
	"testing"
)

func testNames(t *testing.T, cases []nametest, f func(string) (string, bool)) {
	for _, e := range cases {
		r, ok := f(e.in)
		if ok {
			switch {
			case !e.ok:
				t.Errorf("name %q was erroneously accepted as: %q", e.in, r)
			case e.out != r:
				t.Errorf("name mismatch: %q != %q", r, e.out)
			}
		} else {
			if e.ok {
				t.Errorf("name not accepted: %q", e.in)
			}
		}
	}
}

type nametest struct {
	in, out string
	ok      bool
}

func ok(s string) nametest     { return nametest{s, s, true} }
func bad(s string) nametest    { return nametest{s, s, false} }
func fix(s, r string) nametest { return nametest{s, r, true} }
