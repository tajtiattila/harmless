package name

import (
	"testing"
)

func TestGeneratedName(t *testing.T) {
	testNames(t, gentests, GeneratedName)
}

var gentests = []nametest{
	ok("Aries Dark Region FR-V c2-1"),
	ok("Aries Dark Region GR-V c2-7"),
	ok("Scorpii Sector CL-Y d75"),
	ok("Scorpii Sector DL-Y d125"),
	ok("Scorpii Sector UO-R b4-0"),
	ok("Scorpii Sector ZE-A d140"),
	ok("Scorpii Sector ZE-A d160"),
	ok("Col 285 Sector BG-T b18-4"),
	ok("Tucanae Sector AL-X c1-6"),
	ok("Tucanae Sector CQ-Y d79"),
	ok("Tucanae Sector CQ-Y d94"),

	ok("ICV HR-V b2-0"),
	ok("ICW IW-W c1-13"),
	ok("ICZ AF-Z b3"),
	ok("ICZ AL-X b1-3"),
	ok("ICZ AQ-X b1-4"),
	ok("ICZ CW-V b2-4"),
	fix("Col 285 Sector GI-P B20-4 A", "Col 285 Sector GI-P b20-4 A"),

	fix("Trianguli Sector FM-V B2-7", "Trianguli Sector FM-V b2-7"),
	fix("icz eh-u b3-3", "ICZ EH-U b3-3"),
	fix("ICZ AQ-X B1-4", "ICZ AQ-X b1-4"),
	fix("Cephei Sector Pi-t b3-4", "Cephei Sector PI-T b3-4"),

	// 'O' and 'D' → '0'
	fix("BEI DOU Sector GM-V B2-O", "Bei Dou Sector GM-V b2-0"),
	fix("Cephei Sector KR-W B1-D", "Cephei Sector KR-W b1-0"),
	fix("COL 285 SECTOR MK-P A35-O", "Col 285 Sector MK-P a35-0"),

	bad("BEI DOU Sector GM-V BO-O"),
	bad("ICZ AQ-X B1-4A"),
	bad("CEPHEI SECTOR HW-W"),
}
