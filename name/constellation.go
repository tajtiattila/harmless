package name

import (
	"strings"
)

// ConstName checks wether s seems to be a system
// name within a constellation, and returns the string
// with proper case.
func ConstName(s string) (fixed string, ok bool) {
	if s == "" {
		return s, false
	}

	if n := len(s); n > 2 && s[n-2] == ' ' &&
		'a' <= lcb(s[n-1]) && lcb(s[n-1]) <= 'z' {
		if fixed, ok = constName(s[:n-2]); ok {
			fixed += strings.ToUpper(s[n-2:])
			return
		}
	}

	return constName(s)
}

func constName(s string) (fixed string, ok bool) {
	if s == "" {
		return s, false
	}

	if fixed, ok = constSpec[strings.ToLower(s)]; ok {
		return
	}

	// check 'V' + digits prefix
	if fixed, ok = checkvnum(s); ok {
		return
	}

	// check bayer prefix
	if fixed, ok = checkbayer(s); ok {
		return
	}

	// check double alphabetic prefix
	if fixed, ok = check2latin(s); ok {
		return
	}

	// collects digits and space in front, if any
	digits, rest := "", s
	for i, r := range s {
		if '0' <= r && r <= '9' {
			continue
		}
		if r == ' ' {
			i++
			digits, rest = s[:i], s[i:]
			break
		}
	}

	if digits == "" {
		return s, false
	}

	// digits prefix
	if fixed, ok = finishconst(digits, rest); ok {
		return
	}
	// digits alpha '.' prefix
	if len(rest) > 3 &&
		'a' <= lcb(rest[0]) && lcb(rest[0]) <= 'z' &&
		rest[1] == '.' && rest[2] == ' ' {
		return finishconst(digits+strings.ToUpper(rest[:3]), rest[3:])
	}

	// check bayer after digits
	if fixed, ok = checkbayer(rest); ok {
		fixed = digits + fixed
		return
	}

	return "", false
}

// check format: 'V' digits space constellation
func checkvnum(s string) (string, bool) {
	if s[0] != 'v' && s[0] != 'V' {
		return s, false
	}
	var digit bool
	for i, r := range s[1:] {
		switch {
		case '0' <= r && r <= '9':
			digit = true
		case r == ' ':
			if !digit {
				return s, false
			}
			i += 2
			return finishconst("V"+s[1:i], s[i:])
		default:
			return s, false
		}
	}
	return s, false
}

// check format: uppercase*2 space constellation
// NB: checkbayergreek must be called first
func check2latin(s string) (string, bool) {
	if len(s) < 3 || s[2] != ' ' {
		return s, false
	}
	if 'a' <= s[0] && s[0] <= 'z' && 'a' <= s[1] && s[1] <= 'z' {
		return finishconst(strings.ToUpper(s[:3]), s[3:])
	}
	return s, false
}

func checkbayer(s string) (fixed string, ok bool) {
	if fixed, ok = checkbayergreek(s); ok {
		return
	}
	if fixed, ok = checkbayerlatin(s); ok {
		return
	}
	return s, false
}

// check format: lowercase digit* space constellation
func checkbayerlatin(s string) (string, bool) {
	if r := lcb(s[0]); r < 'a' || 'z' < r || r == 'j' || r == 'v' {
		return s, false
	}
	for i, r := range s[1:] {
		switch {
		case '0' <= r && r < '9':
			// pass
		case r == ' ':
			i += 2
			pfx, sfx := s[:i], s[i:]
			r := pfx[0]
			switch {
			case r == 'a':
				pfx = "A" + pfx[1:]
			case 'B' <= r && r <= 'Z':
				pfx = string(r-'A'+'a') + pfx[1:]
			}
			return finishconst(pfx, sfx)
		default:
			return s, false
		}
	}
	return s, false
}

// check format: greek [ '-' digit+ ] space constellation
func checkbayergreek(s string) (string, bool) {
	dash := -1
	for i, r := range s {
		switch {
		case r == '-':
			dash = i
		case 'a' <= lc(r) && lc(r) <= 'z':
			if dash != -1 {
				return s, false
			}
		case '0' <= r && r <= '9':
			if dash == -1 {
				return s, false
			}
		case r == ' ':
			e := i + 1
			var d int
			if dash != -1 {
				d = dash
			} else {
				d = i
			}
			if l, ok := greekMap[strings.ToLower(s[:d])]; ok {
				return finishconst(l+s[d:e], s[e:])
			}
			fallthrough
		default:
			return s, false
		}
	}
	return s, false
}

func lcb(r byte) rune {
	return lc(rune(r))
}

func lc(r rune) rune {
	if r < 'A' || 'Z' < r {
		return r
	}
	return r - 'A' + 'a'
}

func finishconst(pfx, sfx string) (string, bool) {
	if fixed, ok := constMap[strings.ToLower(sfx)]; ok {
		return pfx + fixed, true
	}
	return "", false
}

// Const represents a constellation
type constInit struct {
	name string
	gen  string // genitive name
	pfx  string // non-Bayer prefixes, comma separaded
}

var greekletters = []string{
	"alpha", "beta", "gamma", "delta", "epsilon", "delta", "zeta", "eta",
	"theta", "iota", "kappa", "lambda", "mu", "nu", "xi", "omicron", "pi",
	"rho", "sigma", "tau", "upsilon", "phi", "chi", "psi", "omega",
	// specials
	"kaus",
}

var vconst = []constInit{
	{"Andromeda", "Andromedae", ""},
	{"Antlia", "Antlia", ""},
	{"Apos", "Apodis", ""},
	{"Aquila", "Aquilae", ""},
	{"Aquarii", "Aquarii", ""},
	{"Arae", "Arae", ""},
	{"Arietis", "Arietis", ""},
	{"Auriga", "Aurigae", ""},
	{"Bootes", "Bootis", ""},
	{"Caelum", "Caeli", ""},
	{"Canes Venatici", "Canum Venaticorum", ""},
	{"Camelopardalis", "Camelopardalis", "K"},
	{"Coma Berenices", "Comae Berenices", ""},
	{"Cancer", "Cancri", ""},
	{"Canis Major", "Canis Majoris", ""},
	{"Canis Minor", "Canis Minoris", ""},
	{"Capricorn", "Capricorni", ""},
	{"Carina", "Carinae", "B C I"},
	{"Cassiopeia", "Cassiopeiae", ""},
	{"Catonis", "Catonis", ""},
	{"Centaurus", "Centauri", ""},
	{"Cephus", "Cephei", ""},
	{"Ceti", "Ceti", ""},
	{"Circinus", "Circini", ""},
	{"Coronae Borealis", "Coronae Borealis", ""},
	{"Corvi", "Corvi", ""},
	{"Columba", "Columbae", ""},
	{"Crateris", "Crateris", ""},
	{"Crux", "Crucis", ""},
	{"Cygnus", "Cygni", ""},
	{"Draco", "Draconis", ""},
	{"Dracox", "Draco", ""},
	{"Delphin", "Delphini", ""},
	{"Doradus", "Doradus", ""},
	{"Gemini", "Geminorum", ""},
	{"Gemini", "Gruis", ""},
	{"Eridanus", "Eridani", ""},
	{"Equulei", "Equulei", ""},
	{"Fornae", "Fornacis", ""},
	{"Hercules", "Herculis", ""},
	{"Horologii", "Horologii", ""},
	{"Hydra", "Hydrae", "C D E F G P"},
	{"Hydrus", "Hydri", ""},
	{"Indus", "Indi", ""},
	{"Lacertae", "Lacertae", ""},
	{"Libra", "Librae", ""},
	{"Lyncis", "Lyncis", ""},
	{"Lyra", "Lyrae", ""},
	{"Leo", "Leonis", ""},
	{"Leo Minor", "Leonis Minoris", ""},
	{"Leporis", "Leporis", ""},
	{"Lupus", "Lupi", ""},
	{"Mensa", "Mensae", ""},
	{"Monoceros", "Monocerotis", ""},
	{"Normae", "Normae", ""},
	{"Octant", "Octantis", ""},
	{"Ophiuchus", "Ophiuchii", ""},
	{"Orion", "Orionis", ""},
	{"Pavo", "Pavonis", ""},
	{"Pegasus", "Pegasi", ""},
	{"Perseus", "Persei", ""},
	{"Pictor", "Pictoris", ""},
	{"Piscis Austrinus", "Piscis Austrini", ""},
	{"Pisces", "Piscium", ""},
	{"Puppis", "Puppis", "H I P V"},
	{"Reticulum", "Reticuli", ""},
	{"Orion", "Orionis", ""},
	{"Ophiuchi", "Ophiuchi", ""},
	{"Scorpus", "Scorpii", "G Q"},
	{"Sculptor", "Sculptoris", ""},
	{"Serpent", "Serpentis", ""},
	{"Sextant", "Sextantis", ""},
	{"Sagitta", "Sagittae", ""},
	{"Sagittarius", "Sagittarii", ""},
	{"Ursa Major", "Ursae Majoris", ""},
	{"Ursa Minor", "Ursae Minoris", ""},
	{"Taurus", "Tauri", ""},
	{"Triangulum", "Trianguli", ""},
	{"Triangulum Australe", "Trianguli Australis", ""},
	{"Tucana", "Tucanae", ""},
	{"Vela", "Velorum", "Y"},
	{"Virgo", "Virginis", ""},
	{"Volans", "Volantis", ""},
	{"Vulpecula", "Vulpeculae", ""},
}

var (
	constMap  = make(map[string]string)
	constSpec = make(map[string]string)
	greekMap  = make(map[string]string)
)

func init() {
	for _, c := range vconst {
		constMap[strings.ToLower(c.gen)] = c.gen
		for _, spec := range strings.Fields(c.pfx) {
			n := spec + " " + c.gen
			constSpec[strings.ToLower(n)] = n
		}
	}
	for _, l := range greekletters {
		greekMap[l] = Title(l)
	}
}
