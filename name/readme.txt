System name rules
=================

system:
	constellation-system
	catalog-system
	generated-system
	named-system

constellation-system:
	constellation-exception
	constellation-prefix space constellation-genitive

constellation-prefix:
	bayer
	digit+
	digit+ space bayer
	digit+ space uc '.'
	uc*2                // except mu, nu, xi, pi
	'V' digit+

bayer:
	greek [ '-' digit+ ]
	bayerletter [ digit+ ]

greek: 'Alpha' - 'Omega'

lc: 'a'-'z'

uc: 'A'-'Z'

space: ' '

bayerletter:
	'A'
	'b' - 'z' except 'j' and 'v'

constellation-exception: // incomplete
	K Camelopardalis
	B Carinae
	C Carinae
	I Carinae
	C Hydrae
	G Hydrae
	H Puppis
	I Puppis
	Y Velorum

catalog-system:
	catalog-name catalog-specific

generated-system:
	generated-prefix uc*2 '-' uc space lc digit+ [ '-' digit+ ] [ space uc+ ]

generated-prefix:
	«title case string»
