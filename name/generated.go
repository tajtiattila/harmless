package name

import (
	"strings"
)

func GeneratedName(s string) (string, bool) {
	if len(s) < 2 {
		return s, false
	}
	if n := len(s); s[n-2] == ' ' && isalpha(rune(s[n-1])) {
		if g, ok := generatedName(s[:n-2]); ok {
			return g + strings.ToUpper(s[n-2:]), true
		}
		return s, false
	}
	return generatedName(s)
}

func generatedName(s string) (string, bool) {
	var p1, p2 int
	// find last 2 spaces
	for i, r := range s {
		if r == ' ' {
			p1, p2 = p2, i+1
		}
	}
	if p1 == 0 || p1+5 != p2 {
		return s, false
	}

	// check first block chars and second block prefix
	if !(isalpha(rune(s[p1])) &&
		isalpha(rune(s[p1+1])) &&
		s[p1+2] == '-' &&
		isalpha(rune(s[p1+3])) &&
		isalpha(rune(s[p2]))) {
		return s, false
	}

	sec := fixsec(s[p2:])
	var digit, dash bool
	for _, r := range sec[1:] {
		switch {
		case r == '-':
			if dash || !digit {
				// second dash or dash without digit
				return s, false
			}
			dash, digit = true, false
		case '0' <= r && r <= '9':
			digit = true
		default:
			// invalid char
			return s, false
		}
	}

	if !digit {
		// no digits or trailing dash
		return s, false
	}

	nam := s[:p1]
	if len(nam) < 5 {
		// short prefixes seem to have all caps
		nam = strings.ToUpper(nam)
	} else {
		// normal prefixes use title case
		nam = Title(strings.ToLower(nam))
	}

	// first part is uppercase
	pri := strings.ToUpper(s[p1:p2])

	return nam + pri + sec, true
}

func fixsec(s string) string {
	var fix bool
	if 'A' <= s[0] && s[0] <= 'Z' {
		fix = true
	}
	var nerr int
	for _, r := range s[1:] {
		if r == 'O' || r == 'D' {
			nerr++
			fix = true
		}
	}
	// fix at most one misspelled digit
	if nerr > 1 || !fix {
		return s
	}
	p := []byte(s)
	// second part starts with lowercase
	p[0] = byte(lcb(p[0]))
	for i := 1; i < len(p); i++ {
		if p[i] == 'O' || p[i] == 'D' {
			p[i] = '0'
		}
	}
	return string(p)
}
