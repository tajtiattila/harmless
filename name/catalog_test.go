package name

import (
	"testing"
)

func TestCatalogName(t *testing.T) {
	testNames(t, cattest, CatalogName)
}

var cattest = []nametest{
	ok("2MASS 1503+2525"),
	ok("2MASS J07464256+2000321 A"),
	ok("ADS 16553"),
	ok("ADS 844"),

	ok("AGKR 1017"),
	ok("AGKR 199"),

	ok("BD+00 197"),
	ok("BD+01 299"),
	ok("BD+01 3574"),
	ok("BD+03 3531a"),
	ok("BD+87 118"),
	ok("BD-00 3426"),
	ok("BD-22 5522"),

	ok("CD-22 12617"),
	ok("CD-22 9062"),
	ok("CD-77 45"),
	ok("CD-86 4"),
	ok("CD-86 84"),

	ok("CPC 20 6743"),
	ok("CPC 22 212"),
	ok("CPD-23 501"),
	ok("CPD-28 332"),
	ok("CPD-51 3323"),
	ok("CPD-59 314"),
	ok("CPD-60 604"),
	ok("CPD-64 4178"),
	ok("CPD-70 2439"),
	ok("CPO 24"),

	ok("DX 1217"),
	ok("DX 799"),

	ok("G 107-65"),
	ok("G 121-8"),
	ok("G 14-6"),
	ok("G 181-6"),
	ok("G 5-32"),
	fix("g 65-9", "G 65-9"),
	fix("G146-60", "G 146-60"),
	bad("G Lupi"),

	ok("GD 1192"),
	ok("GD 69"),

	ok("Gl 452.2"),
	ok("Gl 606.1 B"),

	ok("Gliese 105.2"),
	ok("Gliese 110"),
	ok("Gliese 113.1"),
	ok("Gliese 44"),
	ok("Gliese 9378 B"),

	ok("HD 137388"),
	ok("HDS 123"),

	ok("HIP 100035"),
	ok("HIP 110"),
	ok("HIP 99994"),

	ok("HR 1013"),
	ok("HR 12"),
	ok("HR 8809"),
	ok("HR 943"),

	ok("L 119-33"),
	ok("L 12-19"),
	ok("L 206-187"),
	ok("L 214-72"),
	ok("L 26-27"),

	ok("Lalande 10797"),
	ok("Lalande 1799"),
	ok("Lalande 2682"),
	ok("Lalande 9828"),

	ok("LAWD 13"),
	ok("LAWD 8"),
	fix("Lawd 86", "LAWD 86"),
	ok("LAWD 96"),

	ok("LB 2449"),
	ok("LB 3303"),
	ok("LDS 1503"),
	ok("LDS 1503A"),
	ok("LDS 883"),

	ok("LFT 1"),
	ok("LFT 1046"),
	ok("LFT 78"),

	ok("LHS 1004"),
	ok("LHS 103"),
	ok("LHS 2764a"),
	ok("LHS 71"),

	ok("LP 1-52"),
	ok("LP 100-56"),
	ok("LP 101-377"),
	ok("LP 104-2"),
	ok("LP 98-132"),
	ok("LP 98-62"),

	ok("LTT 1015"),
	ok("LTT 10410"),
	ok("LTT 18"),
	ok("LTT 982"),
	ok("LTT 9821"),

	ok("Luyten 143-23"),
	ok("Luyten 145-141"),
	ok("Luyten 205-128"),
	ok("Luyten 347-14"),
	ok("Luyten 674-15"),

	ok("MCC 105"),
	ok("MCC 296"),
	ok("MCC 872"),

	ok("NLTT 10055"),
	ok("NLTT 1139"),
	ok("NLTT 12619"),

	ok("NN 3062"),
	ok("NN 3365"),

	ok("NSV 3315"),
	ok("NSV 4864"),

	ok("PLX 695"),

	ok("PPM 41187"),
	ok("PPM 5287"),
	ok("PPM 62114"),

	ok("PSPF-LF 2"),

	ok("RBS 1843"),
	ok("RBS 742"),

	ok("Ross 1028b"),
	ok("Ross 5"),
	fix("ross 49", "Ross 49"),
	ok("Ross 528"),

	ok("RST 1154"),

	ok("SAO 196806"),
	ok("SAO 82442"),

	ok("SDSS J1416+1348"),

	ok("Smethells 1"),
	ok("Smethells 173"),
	ok("Smethells 56"),

	ok("SPF-LF 1"),

	ok("SPOCS 103"),
	ok("SPOCS 895"),
	ok("SPOCS 900"),

	ok("SRS 2543"),

	ok("Stein 2051"),
	ok("STF 1774"),
	ok("STF 2841"),
	ok("StHA 181"),
	ok("StKM 1-1118"),
	ok("StKM 1-1341"),

	ok("Struve 1321"),
	ok("Struve 2398"),

	ok("TT 58343"),

	ok("UBV 15076"),
	ok("UBV 8670"),

	ok("UGP 120"),
	ok("UGP 64"),

	ok("VVO 19"),

	ok("WD 1207-032"),

	ok("WISE 0313+7807"),
	ok("WISE 0855-0714"),
	ok("WISE 1405+5534"),
	ok("WISE J000517"),

	ok("Wo 9067 B"),
	ok("Wo 9203"),

	ok("Wolf 10"),
	ok("Wolf 1003"),
	ok("Wolf 485a"),
	ok("Wolf 499"),
}
