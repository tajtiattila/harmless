package name

import (
	"strings"
	"testing"
)

func TestConstName(t *testing.T) {
	for _, n := range constnames {
		if n == "" {
			continue
		}
		r, ok := ConstName(n)
		if !ok {
			t.Errorf("constellation name not accepted: %q", n)
		}
		if ok && r != n {
			t.Errorf("constellation mismatch: %q != %q", n, r)
		}
	}
}

var constnames = strings.Split(`
1 G. Caeli
1 Geminorum
1 Hydrae
1 i Centauri
1 Kappa Cygni
10 Arietis
10 Canum Venaticorum
10 Delta Coronae Borealis
10 G. Canis Majoris
10 Hydrae
10 Iota Cygni
10 Kappa Pegasi
10 Rho-3 Eridani
10 Serpentis
10 Tauri
10 Theta Piscium
10 Ursae Majoris
101 Tauri
108 Herculis
109 Herculis
109 Piscium
109 Virginis
11 Aquilae
11 Bootis
11 Cephei
11 Leonis Minoris
11 Mu Aurigae
11 Virginis
110 Herculis
111 Tauri
114 G. Aquilae
119 G. Aquarii
12 Andromedae
12 Aquarii
12 d Bootis
12 G. Canis Minoris
12 Gamma-1 Delphini
12 Kappa Arietis
12 Lambda Coronae Borealis
12 Persei
12 Sextantis
128 Carinae
13 Lambda Crateris
13 Orionis
13 Trianguli
132 G. Aquarii
14 Eridani
14 Herculis
14 i Orionis
14 Ophiuchi
14 Psi Cancri
14 Tau Ursae Majoris
14 Vulpeculae
15 Delphini
15 f Leonis
15 Geminorum
15 Lambda Aurigae
15 Pegasi
15 Psi Scorpii
15 Ursae Majoris
157 G. Aquarii
16 c Ursae Majoris
16 Cephei
16 Cygni
16 Eta Sagittae
16 Librae
16 Lyrae
16 Persei
16 Piscium
16 Tau Coronae Borealis
169 G. Canis Majoris
17 Beta Piscis Austrini
17 Crateris
17 Cygni
17 Kappa-1 Bootis
17 Lyrae
17 Theta Sagittae
17 Virginis
171 G. Aquarii
18 Aquarii
18 Camelopardalis
18 Ceti
18 Comae Berenices
18 e Ursae Majoris
18 Lambda Piscium
18 Leonis Minoris
18 Puppis
18 Scorpii
19 Aquilae
19 Leonis Minoris
19 Phi-2 Ceti
19 Xi Coronae Borealis
2 Lambda Equulei
2 Lyncis
2 Omega Leonis
20 Aquarii
20 Arietis
20 Cephei
20 Leonis Minoris
20 Ophiuchi
21 Draco
21 Eridani
21 Eta Cygni
21 Eta Ursae Minoris
21 Leonis Minoris
22 g Leonis
22 Gamma Piscis Austrini
22 Lyncis
23 Andromedae
23 Arietis
23 Delta Piscis Austrini
23 h Ursae Majoris
23 H. Camelopardalis
23 Librae
233 G. Aquarii
234 G. Carinae
239 G. Aquarii
24 Aquarii
24 Canum Venaticorum
24 G. Carinae
24 Iota Crateris
241 G. Aquarii
25 Arietis
25 d2 Cancri
25 G. Canis Minoris
25 Nu-2 Draconis
26 Alpha Monocerotis
26 Arietis
26 Draconis
26 Ophiuchi
268 G. Carinae
27 b1 Cygni
27 G. Caeli
27 Kappa Persei
28 Beta Serpentis
28 Cephei
28 Doradus
28 Omega Piscium
29 Arietis
29 Ceti
3 Beta Lacertae
3 Corvi
3 Upsilon Ophiuchi
30 Ceti
30 Lyncis
30 Serpentis
31 Aquilae
31 Arietis
31 Beta Leonis Minoris
31 Cephei
31 d1 Virginis
31 Epsilon Librae
31 Vulpeculae
32 c Piscium
32 G. Capricorni
32 l Persei
32 Lyncis
32 Mu Serpentis
33 Bootis
33 G. Antlia
33 G. Canis Majoris
33 Pi Cephei
33 Sextantis
33 Theta Cassiopeiae
34 Lyncis
34 Omicron Cephei
34 Pegasi
35 Ceti
35 Draconis
35 Leonis
35 Leonis Minoris
36 Andromedae
36 Doradus
36 Draconis
36 Ophiuchi
36 Persei
36 Ursae Majoris
37 Geminorum
37 Librae
37 Lyncis
37 Pegasi
37 Ursae Majoris
37 Xi Bootis
38 Arietis
38 Leonis Minoris
38 Lyncis
38 Virginis
39 Aquarii
39 Arietis
39 b Draconis
39 Herculis
39 Leonis
39 Serpentis
39 Tauri
4 A1 Virginis
4 Aquarii
4 Beta Trianguli
4 Camelopardalis
4 Cephei
4 Epsilon-1 Lyrae
4 Eridani
4 Sextantis
40 Ceti
40 Draconis
40 Leonis
40 Leonis Minoris
41 Gamma Serpentis
41 Lambda Hydrae
41 Upsilon-4 Eridani
41 Virginis
42 Aquilae
42 Aurigae
42 Capricorni
44 b Ophiuchi
44 Eta Librae
44 G. Aquilae
44 Pi Serpentis
44 Rho-1 Sagittarii
45 c Bootis
45 d Ophiuchi
45 Tauri
45 Theta Ceti
46 Theta Librae
47 Andromedae
47 Arietis
47 Cassiopeiae
47 Ceti
47 Ursae Majoris
48 Tauri
49 Arietis
49 Ceti
49 d Orionis
49 G. Antlia
49 Librae
49 Serpentis
5 Andromedae
5 Epsilon-2 Lyrae
5 G. Apodis
5 G. Capricorni
5 Gamma Equulei
5 Piscis Austrini
5 Puppis
50 Omega Tauri
50 Upsilon-1 Eridani
51 Aquilae
51 Arietis
51 Eridani
51 Leonis Minoris
52 Leonis Minoris
52 Psi Ursae Majoris
53 Aquarii
53 Tauri
53 Virginis
54 Cassiopeiae
54 Ceti
54 G. Antlia
54 Piscium
55 Virginis
55 Xi Serpentis
56 Cygni
57 Ursae Majoris
57 Virginis
57 Zeta Serpentis
58 Andromedae
58 Epsilon Herculis
58 Eridani
58 Geminorum
58 Ursae Majoris
59 Draconis
59 Eridani
59 Ursae Majoris
59 Virginis
6 Andromedae
6 Ceti
6 Comae Berenices
6 Lyncis
60 Aurigae
60 b Leonis
60 Herculis
60 Tau Draconis
60 Tauri
61 Cygni
61 Mu Orionis
61 Piscium
61 Ursae Majoris
61 Virginis
62 Gamma Ophiuchi
62 Omicron-1 Cancri
62 Ursae Majoris
63 Eridani
63 G. Capricorni
63 Geminorum
63 Omicron-2 Cancri
64 Arietis
64 Ceti
64 G. Capricorni
64 Piscium
65 Kappa Tauri
66 Ceti
66 Draconis
67 Tauri
68 Draconis
68 Eridani
69 e Herculis
69 G. Carinae
69 Tau Ophiuchi
69 Upsilon Tauri
7 Alpha Lacertae
7 Andromedae
7 Canum Venaticorum
7 G. Aquilae
7 Kappa Delphini
7 Pi-1 Orionis
7 Zeta-2 Lyrae
70 Ophiuchi
70 Pegasi
70 Tauri
70 Virginis
71 Epsilon Piscium
71 Omicron Geminorum
72 G. Aquarii
72 Herculis
72 Ophiuchi
73 Rho Cygni
73 Xi-2 Ceti
74 G. Aquarii
75 Pegasi
76 Sigma Ceti
77 Aquarii
78 Ursae Majoris
79 Ceti
79 Herculis
8 Gamma Coronae Borealis
8 Kappa Piscium
8 Lyncis
8 Piscis Austrini
8 Serpentis
80 e Piscium
80 Leonis
80 Tauri
81 Leonis
82 Eridani
82 Pegasi
83 Cancri
83 Tau Piscium
84 Ceti
85 Pegasi
86 Aquarii
86 Mu Herculis
87 Mu Ceti
88 d Tauri
89 Centauri
89 Leonis
9 Aurigae
9 Ceti
9 G. Carinae
9 Iota Piscis Austrini
9 Omega Ophiuchi
9 Omicron Virginis
9 Puppis
9 Ursae Minoris
90 G. Canis Majoris
91 Sigma-1 Tauri
93 Rho Piscium
95 G. Aquarii
97 i Tauri
98 Aquarii

Alpha Caeli
Alpha Centauri
Alpha Cygni
Alpha Fornacis
Alpha Octantis
Alpha Reticuli
Alpha Tucanae
Alpha Volantis
b Capricorni
B Carinae
b Herculis

Pi-1 Ursae Minoris
Pi-2 Columbae

g Eridani
G Hydrae
G Scorpii

I Carinae
I Puppis

ZZ Piscium

CE Bootis
CN Bootis
DE Bootis
GY Bootis
HM Bootis
HN Bootis
HO Bootis
i Bootis
IZ Bootis
Sigma Bootis
Tau Bootis

Iota Aquarii
Iota Capricorni
Iota Centauri
Iota Cephei
Iota Crucis
Iota Eridani
Iota Horologii
Iota Hydri
Iota Pavonis
Iota Persei
Iota Pictoris A
Iota Pictoris B
Iota Piscium
Iota Trianguli Australis
Iota Tucanae
Iota-1 Normae
Iota-2 Fornacis

K Camelopardalis
k Velorum A

Omicron Capricorni B
Omicron Columbae
Omicron Gruis
Omicron Ursae Majoris
Omicron-2 Eridani

Tau Centauri
Tau Ceti
Tau Cygni
Tau Ophiuchii
Tau Puppis
Tau Sagittarii
Tau Sculptoris
Tau-1 Eridani
Tau-1 Gruis
Tau-1 Hydrae
Tau-2 Gruis A
Tau-3 Eridani
Tau-3 Gruis

V0972 Herculis
V1084 Tauri
V1090 Herculis
V1198 Orionis
V1285 Aquilae
V1581 Cygni
V1688 Aquilae
V1703 Aquilae
V2151 Cygni
V2160 Cygni
V2213 Ophiuchii
V2431 Cygni
V2578 Ophiuchii
V2689 Orionis
V350 Canis Majoris
V352 Canis Majoris
V371 Normae
V372 Puppis
V374 Andromedae
V374 Pegasi
V379 Serpentis
V388 Cassiopeiae
V390 Pavonis
V397 Hydrae
V401 Hydrae
V402 Pegasi
V405 Hydrae
V406 Pegasi
V417 Hydrae
V418 Hydrae
V419 Hydrae
V443 Lacertae
V463 Carinae
V484 Tauri
V491 Persei
V492 Lyrae
V538 Aurigae
V565 Persei
V590 Lyrae
V595 Lyrae
V711 Tauri
V740 Cassiopeiae
V749 Herculis
V774 Herculis
V774 Tauri
V780 Tauri
V816 Herculis
V841 Arae
V848 Monocerotis
V857 Centauri
V886 Centauri
V902 Centauri
V989 Cassiopeiae
`, "\n")

var badnames = `
133 G. Canis Major
8 Alpha-1 Libra
V1082 Herculi
V775 Herculi
V898 Herculi
Alpha Chamaelontis
V640 Cassiopeia

25 chi Capricorni
44 chi Draconis
48 chi Bootis
53 chi Ceti
63 chi Leonis
`
