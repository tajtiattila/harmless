package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/tajtiattila/harmless/gmap"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
	"bitbucket.org/tajtiattila/harmless/route"
)

func routeapifunc(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("routeapifunc", req).Enter()
	defer errh.Exit()

	xr, err := makeroute(req)
	errh.Err(err)
	if err != nil {
		xr = &XRoute{Error: err.Error()}
	}

	errh.Err(ServeJson(w, req, xr))
}

func makeroute(req *http.Request) (*XRoute, error) {
	sn, dn := req.FormValue("s"), req.FormValue("d")
	if sn == "" || dn == "" {
		return nil, fmt.Errorf("invalid route query: s=%q, d=%q", sn, dn)
	}
	s, err := ParseSysLoc(sn)
	if err != nil {
		return nil, err
	}
	d, err := ParseSysLoc(dn)
	if err != nil {
		return nil, err
	}
	var xr XRoute
	_, jr := reqjumper(req)

	rr := route.NewAstarMap(GetSpace(), jr).Route(s, d)
	if rr == nil {
		xr.Unreachable = true
	} else {
		rr.Visit(func(a, b *gmap.System, fulltank bool) {
			if len(xr.Route) == 0 {
				xr.Route = append(xr.Route, XJump{System: a})
			}
			if fulltank {
				xr.Route[len(xr.Route)-1].Refuel = true
			}
			xr.Route = append(xr.Route, XJump{System: b})
		})
	}
	return &xr, nil
}

type XRoute struct {
	Route       []XJump `json:"route"`
	Unreachable bool    `json:"unreachable"`
	Error       string  `json:"error,omitempty"`
}

type XJump struct {
	Refuel bool `json:"refuel"`
	*gmap.System
}
