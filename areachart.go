package main

import (
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/area/ngen"
	"bitbucket.org/tajtiattila/harmless/gmap"
	. "bitbucket.org/tajtiattila/harmless/htmlsup"
)

type ChartData struct {
	XChart `json:"chart"`
	Error  string `json:"error,omitempty"`
}

func ServeChart(w http.ResponseWriter, req *http.Request) {
	errh := Hlog("chart", req).Enter()
	defer errh.Exit()

	centre, err := ParseSysLoc(req.FormValue("c"))
	errh.Err(err)

	radius := DefaultRadius
	if rs := req.FormValue("r"); rs != "" {
		if rx, err := strconv.ParseFloat(rs, 64); err == nil {
			radius = rx
		}
	}

	if ps := req.FormValue("p"); ps != "" {
		p, err := ParseSysLoc(ps)
		errh.Err(err)
		if err == nil && p.Sub(centre).Abs() >= radius {
			centre = p
		}
	}

	jumprange, _ := strconv.ParseFloat(req.FormValue("jr"), 64)

	chart, err := GenerateChart(centre, radius, jumprange)
	if err != nil {
		errh.Err(ServeJson(w, req, ChartData{Error: err.Error()}))
	}
	errh.Err(ServeJson(w, req, chart))
}

func GenerateChart(p gmap.Vec3, radius, jumprange float64) (*ChartData, error) {
	vsys, err := Stor.Sphere(p, radius)
	if err != nil {
		return nil, err
	}

	ngm := ngen.Mapper{
		Fonts:  FontMap,
		Proj:   area.XZPlane,
		Scaler: area.FixSphereScale(DefaultRadius),
	}
	am := ngm.Map(vsys)

	chart := XChart{
		P:      p,
		Scale:  am.Scale,
		Elems:  make([]XSys, 0, len(am.Elems)),
		Labels: make([]area.Label, 0, len(am.Labels)),
	}

	fclstr := func(*gmap.System) int { return 0 }
	if jumprange != 0 {
		cm := Cluster.Map(jumprange)
		fclstr = func(sys *gmap.System) int {
			return int(cm.At(sys.P.Vec3i()))
		}
	}
	labels := make(map[string]bool)
	for _, e := range am.Elems {
		if sect, _ := ngen.SplitLabel(e.Name); sect != "" {
			labels[strings.ToLower(sect)] = true
		}
		chart.Elems = append(chart.Elems, XSys{
			Sys:     e.System,
			Cluster: fclstr(e.System),
			X:       e.X,
			Y:       -e.Y, // swap sign for proper screen alignment
			Width:   e.Width,
			Label:   e.Label,
		})
	}
	for _, sect := range am.Labels {
		if labels[strings.ToLower(sect.Label)] {
			sect.Y *= -1 // swap sign for proper screen alignment
			chart.Labels = append(chart.Labels, sect)
		}
	}

	return &ChartData{XChart: chart}, nil
}

type XChart struct {
	P      gmap.Vec3    `json:"coords"` // chart generated for these coordinates
	Scale  float64      `json:"scale"`
	Elems  []XSys       `json:"systems"`
	Labels []area.Label `json:"sectors"`
}

type XSys struct {
	Sys *gmap.System `json:"sys"`

	Cluster int     `json:"clstr,omitempty"`
	X       float64 `json:"sx"`
	Y       float64 `json:"sy"`
	Width   float64 `json:"w"` // width for click area, assuming font size in FontMap
	Label   string  `json:"n"` // system name without sector part
}

func (s *XSys) S() gmap.Vec2 { return gmap.Vec2{s.X, s.Y} }
