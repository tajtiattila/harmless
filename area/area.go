package area

import (
	"bitbucket.org/tajtiattila/harmless/gmap"
)

type Map struct {
	Scale  float64 `json:"scale"` // scaling to show map on screen
	Hash   string  `json:"-"`     // hash changes when map changes
	Elems  []Sys   `json:"systems"`
	Labels []Label `json:"sectors"`
}

type Sys struct {
	*gmap.System
	X     float64 `json:"x"`
	Y     float64 `json:"y"`
	Width float64 `json:"w"` // width for click area, assuming font size in FontMap
	Label string  `json:"n"` // system name without sector part
}

func (s *Sys) S() gmap.Vec2 { return gmap.Vec2{s.X, s.Y} }

type Label struct {
	X     float64 `json:"sx"`
	Y     float64 `json:"sy"`
	Label string  `json:"n"`
}
