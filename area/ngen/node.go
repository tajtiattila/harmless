package ngen

import (
	"bitbucket.org/tajtiattila/harmless/area"
)

const factor = 0.25

type systemNode struct {
	area.Sys
}

func (n *systemNode) Overlap() bool { return false }
func (n *systemNode) Fixed() bool   { return false }

func (n *systemNode) Rect() (x0, x1, y0, y1 float64) {
	x0 = n.X - sysHeight
	x1 = n.X + n.Width
	y0 = n.Y - sysHeight/2
	y1 = n.Y + sysHeight/2
	x0 *= factor
	x1 *= factor
	return
}

func (n *systemNode) SetRect(x0, x1, y0, y1 float64) {
	x0 /= factor
	x1 /= factor
	n.X = x0 + sysHeight
	n.Y = (y0 + y1) / 2
}

type sectorNode struct {
	area.Label
	w float64
}

func (n *sectorNode) Overlap() bool { return false }
func (n *sectorNode) Fixed() bool   { return false }

func (n *sectorNode) Rect() (x0, x1, y0, y1 float64) {
	x0 = n.X - sectHeight
	x1 = n.X + n.w
	y0 = n.Y - sectHeight/2
	y1 = n.Y + sectHeight/2
	x0 *= factor
	x1 *= factor
	return
}

func (n *sectorNode) SetRect(x0, x1, y0, y1 float64) {
	x0 /= factor
	x1 /= factor
	n.X = x0 + sectHeight
	n.Y = (y0 + y1) / 2
}
