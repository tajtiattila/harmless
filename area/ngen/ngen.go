package ngen

import (
	"strings"

	"bitbucket.org/tajtiattila/harmless/area"
	"bitbucket.org/tajtiattila/harmless/area/font"
	"bitbucket.org/tajtiattila/harmless/gmap"
	"bitbucket.org/tajtiattila/harmless/util/vpsc"
)

type Mapper struct {
	Fonts  *font.FontMap
	Proj   area.Projection
	Scaler area.Scaler
}

const (
	sysHeight     = 1.0
	sectHeight    = 2.0
	nameFontScale = 0.8
)

func (mr *Mapper) Map(v []*gmap.System) *area.Map {
	if len(v) == 0 {
		return nil
	}

	scale := mr.Scaler.Scale(v)

	var centre gmap.Vec3
	for _, sys := range v {
		centre = centre.Add(sys.P)
	}
	centre = centre.Mul(1 / float64(len(v)))

	var sysNodes []systemNode
	sectm := make(map[string]sector)
	for _, sys := range v {
		sectn, l := SplitLabel(sys.Name)
		p := mr.Proj.P(sys.P.Sub(centre))
		sysNodes = append(sysNodes, systemNode{area.Sys{
			System: sys,
			X:      p[0],
			Y:      p[1],
			Width:  sysfont(mr.Fonts, sys).TextWidth(l, sysHeight*nameFontScale) + sysHeight,
			Label:  l,
		}})

		if sectn != "" {
			slo := strings.ToLower(sectn)
			z := sectm[slo]
			z.name = sectn
			z.p = z.p.Add(sys.P)
			z.count++
			sectm[slo] = z
		}
	}

	var sectNodes []sectorNode
	for _, sect := range sectm {
		p := mr.Proj.P(sect.avg().Sub(centre))
		sectNodes = append(sectNodes, sectorNode{area.Label{
			X:     p[0],
			Y:     p[1],
			Label: sect.name,
		}, mr.Fonts.Sector.TextWidth(sect.name, sectHeight*nameFontScale) + sectHeight})
	}

	nodes := make([]vpsc.Node, 0, len(sysNodes)+len(sectNodes))
	for i := range sysNodes {
		p := &sysNodes[i]
		nodes = append(nodes, p)
	}
	for i := range sectNodes {
		p := &sectNodes[i]
		nodes = append(nodes, p)
	}

	vpsc.RemoveOverlaps(nodes)

	StalkShift(centre[1], sysNodes, sectNodes)

	m := area.Map{
		Scale:  scale,
		Elems:  make([]area.Sys, len(sysNodes)),
		Labels: make([]area.Label, len(sectNodes)),
	}
	for i := range sectNodes {
		m.Labels[i] = sectNodes[i].Label
	}
	for i := range sysNodes {
		m.Elems[i] = sysNodes[i].Sys
	}
	return &m
}

type sector struct {
	name  string
	p     gmap.Vec3
	count int
}

func (z *sector) avg() gmap.Vec3 {
	return z.p.Mul(1 / float64(z.count))
}

func sysfont(fm *font.FontMap, sys *gmap.System) *font.Font {
	if len(sys.Stations) > 0 {
		return fm.Inhabited
	}
	return fm.Empty
}

func SplitLabel(name string) (sect, label string) {
	ll := strings.ToLower(name)
	if i := strings.Index(ll, " sector "); i != -1 {
		i += 7
		sect, label = name[:i], name[i+1:]
	} else {
		label = name
	}
	return
}
