package ngen

import (
	"sort"
)

const (
	// stalkscale specifies how long the stalk is relative to the y coordinate
	// must match chartparams.barscale in js
	stalkscale = 0.2

	// stalkmin is the minimal length to consider
	// smaller lengths are typically hidden behind a star
	stalkmin = 0.3

	// minimal separation between stalks
	stalkdist = 0.15
)

func StalkShift(y0 float64, vs []systemNode, vz []sectorNode) {
	v := make([]stalker, 0, len(vs)+len(vz))
	for _, s := range vs {
		dy := (s.P[1] - y0) * stalkscale
		y0, y1 := s.Y, s.Y-dy
		if y1 < y0 {
			y0, y1 = y1, y0
		}
		visible := y1-y0 > stalkmin
		if visible {
			// avoid touching stalks by extending them vertically for this test
			y0 -= stalkmin / 2
			y1 += stalkmin / 2
		}
		v = append(v, stalker{len(v), s.X, visible, y0, y1})
	}
	for _, z := range vz {
		v = append(v, stalker{len(v), z.X, false, z.Y, z.Y})
	}

	sort.Sort(horzsort(v))
	start := 0
	totalshift := 0.0
	for i := range v {
		v[i].x += totalshift
		if !v[i].stalk {
			continue
		}

		shift := 0.0
		for j := start; j < i; j++ {

			// check if igored or passed behind, skip in future
			if !v[j].stalk || v[j].x+stalkdist <= v[i].x {
				start++
				continue
			}

			// check overlap
			if (v[j].y0 < v[i].y0 && v[i].y0 < v[j].y1) ||
				(v[i].y0 < v[j].y0 && v[j].y0 < v[i].y1) {
				if minshift := stalkdist - (v[i].x - v[j].x); shift < minshift {
					shift = minshift
				}
			}
		}
		totalshift += shift
		v[i].x += shift
	}
	adjust := totalshift / 2
	for i := range v {
		x := v[i].x - adjust
		ii := v[i].idx
		if ii < len(vs) {
			vs[ii].X = x
		} else {
			vz[ii-len(vs)].X = x
		}
	}
}

type stalker struct {
	idx    int
	x      float64
	stalk  bool
	y0, y1 float64
}

type horzsort []stalker

func (s horzsort) Len() int           { return len(s) }
func (s horzsort) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s horzsort) Less(i, j int) bool { return s[i].x < s[j].x }
