package area

import (
	"fmt"
	"math"

	"bitbucket.org/tajtiattila/harmless/gmap"
)

const (
	CubeLyPerStar = 70 // Ly³ there is on the average one star in this volume
	SpaceForStar  = 4  // average on screen area needed for a single star
)

type Scaler interface {
	String() string
	Scale(v []*gmap.System) float64
}

// FixBoxScale is a fixed scale based on the
// average density of systems within a sphere
// defined by radius.
func FixSphereScale(radius float64) Scaler {
	return &fixSphereScale{radius}
}

// FixBoxScale is a fixed scale based on the
// average density of systems in a box having
// a square base of xz length and a height of y.
func FixBoxScale(xz, y float64) Scaler {
	return &fixBoxScale{xz, y}
}

// AutoScale is an automatic scaler based on the
// number of systems provided to Scale().
var AutoScale Scaler = &autoScale{}

type fixSphereScale struct {
	Radius float64
}

func (s *fixSphereScale) String() string {
	return fmt.Sprintf("fixSphereScale{%v}", s.Radius)
}

func (s *fixSphereScale) Scale([]*gmap.System) float64 {
	vol := 4 / 3 * math.Pi * (s.Radius * s.Radius * s.Radius)
	nfit := vol / CubeLyPerStar
	scale := math.Sqrt(nfit*SpaceForStar) / math.Pi / s.Radius
	return scale
}

type fixBoxScale struct {
	XZ, Y float64
}

func (s *fixBoxScale) String() string {
	return fmt.Sprintf("fixBoxScale{%v,%v}", s.XZ, s.Y)
}

func (s *fixBoxScale) Scale([]*gmap.System) float64 {
	vol := s.XZ * s.XZ * s.Y
	nfit := vol / CubeLyPerStar
	scale := math.Sqrt(nfit*SpaceForStar) / s.XZ / 2.5
	return scale
}

type autoScale struct{}

func (s *autoScale) String() string { return "autoScale" }

func (s *autoScale) Scale(v []*gmap.System) float64 {
	if len(v) == 0 {
		return 1
	}
	var c gmap.Vec3
	for _, sys := range v {
		c = c.Add(sys.P)
	}
	c = c.Mul(1 / float64(len(v)))
	var maxr2 float64
	for _, sys := range v {
		if r2 := sys.P.Sub(c).Abssq(); maxr2 < r2 {
			maxr2 = r2
		}
	}
	r := math.Sqrt(maxr2)
	scale := math.Sqrt(float64(len(v)*SpaceForStar)) / math.Pi / r
	return scale
}
