package font

import (
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"image"
	"image/color"
	"io"
	"io/ioutil"
)

type Font struct {
	Data *truetype.Font
	ctx  *freetype.Context
}

func LoadFont(fn string) (*Font, error) {
	data, err := ioutil.ReadFile(fn)
	if err != nil {
		return nil, err
	}
	return parseFont(data)
}

func ReadFont(r io.Reader) (*Font, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}
	return parseFont(data)
}

func parseFont(data []byte) (*Font, error) {
	f, err := truetype.Parse(data)
	if err != nil {
		return nil, err
	}
	ctx := freetype.NewContext()
	ctx.SetDPI(72)
	ctx.SetSrc(devnullim)
	ctx.SetDst(devnullim)
	ctx.SetFont(f)
	return &Font{f, ctx}, nil
}

func (f *Font) TextWidth(s string, siz float64) float64 {
	const mult = 10
	f.ctx.SetFontSize(siz * mult)
	pt, _ := f.ctx.DrawString(s, freetype.Pt(0, 0))
	return float64(pt.X) / (mult * 0x100)
}

var devnullim = new(devNullImage)

type devNullImage struct{}

func (d *devNullImage) ColorModel() color.Model     { return color.RGBAModel }
func (d *devNullImage) Bounds() image.Rectangle     { return image.Rect(-1e9, -1e9, 1e9, 1e9) }
func (d *devNullImage) At(x, y int) color.Color     { return image.Black }
func (d *devNullImage) Set(x, y int, c color.Color) {}
