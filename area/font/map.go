package font

import (
	"os"
)

type FontMap struct {
	Inhabited, Empty, Sector *Font
}

func LoadDefaultFontMap(dir string) (*FontMap, error) {
	if dir != "" && !os.IsPathSeparator(dir[len(dir)-1]) {
		dir += string(os.PathSeparator)
	}
	fontinhabited, err := LoadFont(dir + "Oswald-Regular.ttf")
	if err != nil {
		return nil, err
	}
	fontempty, err := LoadFont(dir + "Oswald-Light.ttf")
	if err != nil {
		return nil, err
	}
	fontsector, err := LoadFont(dir + "CrimsonText-Italic.ttf")
	if err != nil {
		return nil, err
	}
	return &FontMap{fontinhabited, fontempty, fontsector}, nil
}
